#!/bin/bash 
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Thu 09 Aug 2007 09:10:25 AM CEST

# This will create documentation for python modules
# in the TDAQ release specified.

if [ $# = 0 ]; then
  echo "usage: $0 [module1] [module2] ... [moduleN]"
  exit 1
fi

function get_tdaq_release() {
  local name=`dirname $TDAQ_INST_PATH`;
  echo `basename $name`;
}

RELEASE=`get_tdaq_release`;

# These are epydoc (as of 3.0b1) options
TYPE="--html"
URL="--url=https://atddoc.cern.ch/cmt/releases/index.html"
NAME="--name=$RC"
GRAPH="--graph=all"
NOPARSING="--exclude-parse=Py.+"
VERBOSE="--verbose --simple-term"

# Removing the old directory before we can proceed
OUTDIR="/afs/cern.ch/user/r/rabello/public/html/epydoc/$RELEASE"
[ -e $OUTDIR ] && rm -rf $OUTDIR;
OUTPUT="--output=$OUTDIR";

if [ -z ${TDAQ_INST_PATH} ]; then
  export CMTCONFIG=i686-slc5-gcc43-opt
  source /afs/cern.ch/atlas/project/tdaq/cmt/$RELEASE/installed/setup.sh
fi

# Guarantees we use LCG's python instead of the machine's default
export PATH=${TDAQ_PYTHON_HOME}/bin:${PATH}
export PYTHONPATH=$PYTHONPATH:/usr/lib/python2.4/site-packages

/usr/bin/epydoc $TYPE $URL $NAME $GRAPH $NOPARSING $VERBOSE $OUTPUT $*

exit $?
