# PartitionMaker 

The underlying implementation of the `pm_farm.py` tool has
been changed from ParallelSSH to [fabric](https://www.fabfile.org).
The reason is that the former seems to be mostly unmaintained
in the last couple of years.

## tdaq-11-02-00

Build DALs from oks files in release installation area to avoid side effects when use oksgit and rdb

## tdaq-09-03-01

The underlying implementation of the pm_farm.py tool has
been changed from paramiko to ParallelSSH. Both key
based and kerberos authentication are supported.

