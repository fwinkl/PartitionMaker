from __future__ import print_function
from builtins import range
import sys
import re
import pm.common
import logging
import itertools
from collections import defaultdict
from pm.dal import dal, DFdal, DFTriggerIndal
from pm.common import localhost
from pm.multinode.GLOBALS import global_counter

# ====================
# begin of ROS section
# ====================

def ros_farm_subdet(rob_list, computer, subdetectors=None, farm_prefix=None, default_host=None):
  """Splits ROB identifiers among different ROSs producing a farm.

  This method will generate a python dictionary that represents a ROS farm just
  in the way gen_rose_segment() and gen_ros_segment() expects.

  Keyword parameters:

  default_host -- This is the default host that will be used for every
  individual subfarm controller. If you want to change, after the return,
  browse the list and re-set the controllers.

  rob_list -- This is a list which contains unique identifiers for the ROBs
  that will be spread through the system.

  computer -- This is a list of dal.Computer or dal.ComputerSet objects that
  will be used as ROSs for subdetectors in use.

  subdetectors -- This is a list of subdetectors that should be used by
  the ROSs for subdetectors in use. A

  farm_prefix -- If this variable is set, we generate the ROS-Segment name
  structures taking the value of this string as a prefix.
  """
  import pm.ros
  import pm.utils
  available_ross = pm.utils.computer_list(computer) #this will order the list
  sublist = pm.ros.subdetector_rob_ros_allocation(rob_list, len(available_ross), subdetectors)

  retval = {}
  if default_host: retval['default_host'] = default_host
  if farm_prefix: retval['name'] = farm_prefix
  for s in range(0, len(sublist)):
    sd = pm.ros.calculate_proper_subdetector(sublist[s])

    if sd in retval: retval[sd]['ros'].append((available_ross[s], sublist[s]))
    else:
      retval[sd] = {'ros': [(available_ross[s], sublist[s])]}
      if default_host: retval[sd]['default_host'] = default_host
      if farm_prefix:
        import eformat
        sdname = eformat.helper.SourceIdentifier(sd, 0).human_detector()
        retval[sd]['name'] = '%s-%s' % (farm_prefix, sdname)

  return retval

def ros_farm_random(rob_list, computer, default_host=None, farm_prefix=None):
  """Splits ROB identifiers among different ROSs producing a farm.

  This method will generate a python dictionary that represents a ROS farm just
  in the way gen_rose_segment() and gen_ros_segment() expects.

  Keyword parameters:

  default_host -- This is the default host that will be used for every
  individual subfarm controller. If you want to change, after the return,
  browse the list and re-set the controllers.

  rob_list -- This is a list which contains unique identifiers for the ROBs
  that will be spread through the system.

  computer -- This is a list of dal.Computer or dal.ComputerSet objects that
  will be used as ROSs.

  farm_prefix -- If this variable is set, we generate the ROS-Segment name
  structures taking the value of this string as a prefix.
  """
  import pm.ros
  import pm.utils
  available = pm.utils.computer_list(computer) #this will order the list
  sublist = pm.ros.randomize_rob_ros_allocation(rob_list, len(available))

  retval = {}
  if default_host: retval['default_host'] = default_host
  if farm_prefix: retval['name'] = farm_prefix
  for s in range(0, len(sublist)):
    sd = pm.ros.calculate_proper_subdetector(sublist[s])

    if sd in retval: retval[sd]['ros'].append((available[s], sublist[s]))
    else:
      retval[sd] = {'ros': [(available[s], sublist[s])]}
      if default_host: retval[sd]['default_host'] = default_host
      if farm_prefix:
        import eformat
        sdname = eformat.helper.SourceIdentifier(sd, 0).human_detector()
        retval[sd]['name'] = '%s-%s' % (farm_prefix, sdname)

  return retval

def ros_farm_linear(rob_list, computer, subdetector_sets=None, default_host=None, farm_prefix=None):
  """Splits ROB identifiers among different ROSs producing a farm.

  This method will generate a python dictionary that represents a ROS farm just
  in the way gen_ros_segment() expects
  It allows to put sets of subdetectors on different ros hosts
  It is similar to pm.multinode.ros_farm_random, but does not randomize the robs.
  It does NOT insist on putting each subdetector into a separate ROS host,
  as pm.multinode.ros_farm_subdet does.

  Keyword parameters:

  default_host -- This is the default host that will be used for every
  individual subfarm controller. If you want to change, after the return,
  browse the list and re-set the controllers.

  rob_list -- This is a list which contains unique identifiers for the ROBs
  that will be spread through the system.

  computer -- This is a list of dal.Computer or dal.ComputerSet objects that
  will be used as ROSs for subdetectors in use. 

  subdetector_sets -- This is a *list of lists* of sudbetector ids or subdetector groups that should be used by
  the ROSs for subdetectors in use. The top level of this list must have same length as computer!!!
  You can only give either subdetector ids or subdetector groups. 
  Example using subdetector groups:
  LAr     = [ 0x4 ]
  nonLAr  = [ 0x1, 0x2, 0x3, 0x5, 0x6, 0x7, 0x8 ]
  subdetector_sets = [ LAr, nonLAr ] 

  farm_prefix -- If this variable is set, we generate the ROS-Segment name
  structures taking the value of this string as a prefix.
  """
  import pm.ros
  import pm.utils

  if len(computer) != len(subdetector_sets):
      print('FATAL: my_ros_farm_subdet needs same number of ROS computers as subdetector_sets')
      import sys
      sys.exit(1)

  available = pm.utils.computer_list(computer) #this will order the list
  sublist = customize_rob_ros_allocation(rob_list, subdetector_sets)

  retval = {}
  if default_host: retval['default_host'] = default_host
  if farm_prefix: retval['name'] = farm_prefix
  for s in range(0, len(sublist)):
    sd = pm.ros.calculate_proper_subdetector(sublist[s])

    if sd in retval: retval[sd]['ros'].append((available[s], sublist[s]))
    else:
      retval[sd] = {'ros': [(available[s], sublist[s])]}
      if default_host: retval[sd]['default_host'] = default_host
      if farm_prefix:
        import eformat
        sdname = eformat.helper.SourceIdentifier(sd, 0).human_detector()
        retval[sd]['name'] = '%s-%s' % (farm_prefix, sdname)

  return retval

def customize_rob_ros_allocation(roblist, subdetector_sets):
  """This method will customize the allocation of ROBs to ROS.

  Returns a list that contains lists of rob identifiers. The allocation method
  uses a round-robin process, so the number of ROBs each ROS is charge is
  approximately the same.

  Keyword parameters:
  roblist -- A (python) list of ROBs that you want to distribute
  subdetector_sets -- A list of subdetector groups. The ROBs corresponding to each list
                      go to a separate ROS. User should ensure reasonably balanced ROSes.
  """
  import pm.utils
  from eformat import helper

  dbg = False

  # we make sure the user did not screw-up...
  roblist = pm.utils.uniq(roblist)
  nrobs = len(roblist)
  nros = len(subdetector_sets)
  retval = [[] for k in range(0, nros)]
  ros = 0

  if dbg:
    print('subdetector_sets =',subdetector_sets)
    print('len(roblist) =',nrobs)

  sdgroups = True
  sdtest = min( min(subdetector_sets[0]), min(subdetector_sets[min(1,nros-1)]) ) 
  if sdtest==0x0:
    print('FATAL: you have chosen a full event as a subdetector/group')
    import sys
    sys.exit(1)
  else:
    sdgroups = sdtest < 0x10
    
  if dbg:
    if sdgroups:
      print('INFO: you have requested to select ROSes by subdetector groups')
    else:
      print('INFO: you have requested to select ROSes by subdetectors')

  for k in range(0, nrobs):

    # if roblist[k] corresponds to subdetector_set n, put it in ros n

    #if dbg:
    #  print '%5d %16x %5d' % (k, roblist[k], len(roblist))

    if sdgroups:
      sd = helper.SourceIdentifier(roblist[k]).subdetector_group()
    else:
      sd = helper.SourceIdentifier(roblist[k]).subdetector_id()

    for ros in range(0, len(subdetector_sets)):
      if sd in subdetector_sets[ros]:
        retval[ros].append(roblist[k])
        break

  # just to make it nicer, we organize the list
  for k in retval: k.sort()

  return retval

def gen_rose_segment(template, farm, dcapp_config, preload=False,
    counter=global_counter):
  """Generates a multinode ROS segment that contains multiple ROS emulators.

  This method will generate a ROS segment based on your parametrization. The
  format of the segment is how it is normally setup in ATLAS Point1.

  Keyword parameters:

  template -- This must be a pm.project.Project where the default templated
  applications I should reside. It is normally loaded from a common (release)
  include file.

  farm -- This is the ROS farm description in a python-ic format. Here is a
  description::

    farm = {
     'default_host': dal.Computer|*,
     'name': string|*,
     subdetector_id: {
                      'default_host': dal.Computer|*,
                      'name': string|*,
                      'ros': [
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                             ]
                     },
     subdetector_id: {
                      'default_host': dal.Computer|*,
                      'ros': [
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                             ]
                     },
     ...
    }

  Each key in this python dictionary has to correspond to a valid subdetector
  identifier (so, they can be any value in the eformat.helper.SubDetector
  enumeration, as defined in the Event Format note, which is compatible with
  this release of the TDAQ software). The first component for each subdetector
  segment is the default host (controlling node) for that segment. Following,
  there are lists or tuples that contain, as the first entry, the node where
  the ROS application is supposed to be running and the list of ROBs it should
  handle (These are 32-bit integers).  The complete list of ROBs has to unique
  across the entire system. This will be checked in this method.

  Optionally, the user can specify subsegment default hosts and a global IS
  resource server that will be used for the ROSEs. Entries marked with "|*" are
  optional. If they are not specified, a default action is taken.

  dcapp_config -- The DCApplicationConfig object to use for the ROSE

  preload -- If the ROSE will be preloaded with data

  counter -- This is a dictionary that contains the relevant counters for the
  generation of multiple ROS(E)-Segments in a single database. If you are not
  interested in this, just don't set it. The default will work as expected.
  This dictionary will be updated by this method.
  """

  import pm.ros
  from pm.utils import uniq, safeInsert
  import eformat

  # get the templates of interest
  controller_template = \
    template.getObject("RunControlTemplateApplication", "DefRC")
  rdb_template = \
      template.getObject("InfrastructureTemplateApplication", "DefRDB")

  # check ROB identifier uniqueness
  overall_roblist = []
  for sd in [k for k in list(farm.keys()) if type(k) is int]:
    for i in farm[sd]['ros']: overall_roblist += i[1]

  if len(overall_roblist) != len(uniq(overall_roblist)):
    raise RuntimeError("Your ROB list (considering the whole segment) contains repeated identifiers. Fix this before proceeding.")

  dummy_data_size = -1 #this represents the preloaded mode
  if not preload:
    dummy_data_size=100 #good default, you can still change it after...
  rose_config = pm.ros.create_rose_config("ROSEConfiguration-1",
      dummy_size=dummy_data_size)

  subdetector_segment = []
  for sd in [k for k in list(farm.keys()) if isinstance(k, eformat.helper.SubDetector)]:
    sdname = eformat.helper.SourceIdentifier(sd, 0).human_detector()
    subfarm = farm[sd]
    all = []
    for k in range(0, len(subfarm['ros'])):
      rose = pm.ros.create_rose("ROS-%d" % counter['ROS'],
          subfarm['ros'][k][1], rose_config, dcapp_config=dcapp_config,
          computer=subfarm['ros'][k][0])
      counter['ROS'] += 1
      all.append(rose)
    name = subfarm.get('name', 'ROS-Segment-%d-%s' % \
        (counter['ROS-Segment'], sdname))
    segment = pm.ros.subsegment(name, controller_template,
        all, subfarm.get('default_host', localhost))
    subdetector_segment.append(segment)

  # better naming convention if the user is generating a single farm
  name = farm.get('name', 'ROS-Segment-%d' % counter['ROS-Segment'])
  segment = pm.ros.segment(name, controller_template,
      subdetector_segment, farm.get('default_host', localhost))
  counter['ROS-Segment'] += 1
  safeInsert(segment, 'Infrastructure', [rdb_template])
  return segment

def gen_ros_segment(template, farm, preload=False, ros_mode='emulated-dc',
    counter=global_counter, roldatagen=False, robinnp=False):
  """Generates a multinode ROS segment that contains multiple ROS.

  This method will generate a ROS segment based on your parametrization. The
  format of the segment is how it is normally setup in ATLAS Point1.

  Keyword parameters:

  template -- This must be a pm.project.Project where the default templated
  applications I should reside. It is normally loaded from a common (release)
  include file.

  farm -- This is the ROS farm description in a python-ic format. Here is a
  description::
    farm = {
     'default_host': dal.Computer|*,
     'name': string|*
     subdetector_id: {
                      'default_host': dal.Computer|*,
                      'name': string|*,
                      'detector': DFdal.SubDetector|*,
                      'ros': [
                              (ros_host, { 'modules':
                                            [(<module_id>, [rob|(rob, channel index),]),
                                             (<module_id>, ...),
                                                    ...         ],
                                            'index': <ros_global_index>,
                                            'name': <ros_name>|* }),
                              (ros_host, { ... }),
                              ...
                             ]
                     },
      subdetector_id: { ... },


     'ros_config': dal.ReadoutConfiguration|*,
     'rcapp_override_params': {},
     'ros_override_params': {},
     'segment_override_params': {},

      (definitely needed in robin modes)
     'ros_common_config': [<config_file_pathname>],

      (needed in robin modes)
     'robin_channel_config': dal.RobinChannelConfiguration,
     'robin_config': dal.RobinConfiguration,
     'robin_mem_pool': dal.MemoryPool,

      (needed in reb mode)
     'reb': {
       'input_fragment_type':  string|*
       'tcp_data_out_config_override_params': {},
       'tcp_base_port': int|*,
       'segment_override_params': {},
       'rcapp_override_params': {},
       'rcd_override_params': {},
      }
    }

  Each key in this python dictionary has to correspond to a valid subdetector
  identifier (so, they are integers as defined in eformat.helper.SubDetector
  enumeration and at the Event Format note, which is compatible with this
  release of the TDAQ software). The first component for each subdetector
  segment is the default host (controlling node) for that segment. Following,
  there are lists or tuples that contain, as the first entry, the node where
  the ROS application is supposed to be running and the list of ROBs it should
  handle (These are 32-bit integers).  The complete list of ROBs has to unique
  across the entire system. This will be checked in this method.

  Optionally, the user can specify subsegment default hosts and a global IS
  resource server that will be used for the ROSs. Entries marked with '|*' are
  optional. If they are not given, a default action is taken.

  The user can optionally provide a number of parameters through the farm
  dictionary, useful for generating pre-configured ROS segments. In robin-*
  modes the user must provide the path to the ROS common configuration file
  (usually called 'ros-common-config.xml') through the 'ros_common_config' key,
  since a number of objects in it are necessary to generate the segment.

  Additionally, a farm can define a number of *_override_params keys that can
  be used to directly override the DAL parameters of each object. The form of
  these is a dictionary with the DAL parameters as the keys, e.g.:

    'obj_override_params' : { 'DalParamName':Value, ...}

  In modes where a ROS Event Builder segment will be generated, the user must
  also provide a dictionary through the 'reb' key that defines the relevant
  parameters of the REB segment.

  preload -- If the ROS will be preloaded with data

  counter -- This is a dictionary that contains the relevant counters for the
  generation of multiple ROS-Segments in a single database. If you are not
  interested in this, just don't set it. The default will work as expected.
  This dictionary will be updated by this method.

  roldatagen -- Whether the internal data generator of the ROBINs should be
  enabled. This obviously makes sense only in ROBIN modes.
  """

  import pm.ros
  from pm.utils import uniq, safeInsert
  import eformat

  # all SubDetector objects are ultimately converted to ints, but users should
  # still prefer them when writing farms.
  for sd_obj in \
      [k for k in list(farm.keys()) if isinstance(k, eformat.helper.SubDetector)]:
    farm[sd_obj.real] = farm.pop(sd_obj)

  if ros_mode not in [\
      'robin-dc', 'preloaded-dc', 'emulated-dc', 'robin-datadriven-reb',
      'robin-datadriven-edo']:
    raise RuntimeError("Error: invalid ros-mode '%s'" % ros_mode)
  # get the templates of interest
  controller_template = \
      template.getObject("RunControlTemplateApplication", "DefRC")
  rdb_template = \
      template.getObject("InfrastructureTemplateApplication", "DefRDB")

  # if available, load the ROS common configuration as an object cache
  # (this is normally the ros-common-config.xml file)
  if 'ros_common_config' in farm:
    ros_common_objs = pm.common.load(farm['ros_common_config'])
  else: ros_common_objs = {}

  # --preload is the equivalent of --ros-mode preloaded-dc
  # convert for compatibility
  if preload and 'preloaded' not in ros_mode:
    ros_mode = 'preloaded-dc'
  #conversely
  if 'preloaded' in ros_mode: 
    preload = True

  # check ROB identifier uniqueness, if preloading is enabled. Otherwise, they
  # have to be unique only within the same ROS.
  if preload:
    # check ROB identifier uniqueness
    overall_roblist = []
    for sd_id in [k for k in list(farm.keys()) if isinstance(k, int)]:
      # modified for new farm structure
      for i in farm[sd_id]['ros']:
        if isinstance(i[1], list): #old farm
          overall_roblist += i[1]
        elif isinstance(i[1], dict):
          #new farm; each module contains its own channel list
          for m in i[1]['modules']:
            try:
                # Explicit channel assignment
                overall_roblist += [rob for rob, idx in m[1]]  
            except TypeError:
                # Plain list of ROBs
                overall_roblist += m[1]

    if len(overall_roblist) != len(uniq(overall_roblist)):
      raise RuntimeError("Your ROB list (considering the whole partition) contains repeated identifiers. Fix this before proceeding.")

  # check uniqueness of the computers where ROSs run
  computer_list = []
  for sd_id in [k for k in list(farm.keys()) if isinstance(k, int)]:
    for i in farm[sd_id]['ros']:
      if i[0] not in computer_list: computer_list.append(i[0])
      else:
        logging.warning('Computer %s is repeated in the host list for (real) ROSs.  While this is not a configuration error, your partition may run out of resources if the ResourceManager is programmed to accept only 1 ROS per machine. In this event, the actual partition may give problems upon initialization.' % i[0].id)

  # 1. Create the ROS accessory configurations
  default_ros_config = pm.ros.create_ros_config("ROS-Segment-%d-Configuration" % \
      counter['ROS-Segment'], request_handlers=4)
  default_trigger_in = pm.ros.create_ros_trigger_in("ROS-Segment-%d-TriggerIn" % \
      counter['ROS-Segment'], robinnp=robinnp)
  default_data_out = pm.ros.create_ros_data_out("ROS-Segment-%d-DataOut" % \
      counter['ROS-Segment'])
  if preload:
    default_mem_pool = \
        pm.ros.create_ros_memory_pool("ROS-Segment-%d-MemoryPool-Preloaded" \
        % counter['ROS-Segment'], pages=4, size=32768)
  else:
    default_mem_pool = pm.ros.create_ros_memory_pool("ROS-Segment-%d-MemoryPool" % \
        counter['ROS-Segment'])
  debug_output = farm.get('debug_output', None)

  mem_pool = ros_common_objs.get('ROS-MemoryPool-Headers', default_mem_pool)

  # allow the farm to override the RunControlApplication
  if 'rcapp_override_params' in farm:
    run_control_app = pm.ros.create_run_control_application( \
        farm['name']+"-ROS-Segment",
        program=ros_common_objs['run_controller'],
        override_params=farm['rcapp_override_params'])
  else: run_control_app = controller_template

  if 'robin' in ros_mode:
    channel_config = farm['robin_channel_config']

    if roldatagen:
      # This is a special case where we need to override the channel
      # configuration to enable the internal ROBIN data generator.
      # Since the configuration object usually lies in an included
      # file, we need to create a new one.

      channel_config_datagen = \
          pm.ros.create_ros_robin_channel_config(channel_config.id+'-DataGen')
      channel_config_datagen.copy(channel_config)
      channel_config_datagen.RolDataGen = True
      # incompatible
      if channel_config.Rolemu:
        raise RuntimeError("Error: RobinChannelConfiguration has Rolemu enabled: cannot enable RolDataGen")
      # discard the old reference
      channel_config = channel_config_datagen

  default_emu_config = \
    pm.ros.create_ros_emulated_config("ROS-Segment-%d-EmulatedConfig" % \
        counter['ROS-Segment'])
  emu_config = ros_common_objs.get("EmulatedReadout-Config", default_emu_config)

  reb_tcp_data_out_config = None
  if ros_mode.endswith('reb'):
    reb_tcp_data_out_config = pm.ros.create_tcp_data_out_config( \
        name="TCPDataOut-Config-%s" % farm.get('name', "Default"),
        override_params=farm['reb'].get('tcp_data_out_config_override_params', {}))

  in_top_segment = []
  for sd_id in [k for k in list(farm.keys()) if isinstance(k, int)]:
    sdname = farm[sd_id].get('name',
        eformat.helper.SourceIdentifier(sd_id*0x10000).human_detector())
    if robinnp:
      default_detector = pm.ros.create_detector('Null', id=0)
      detector = ros_common_objs.get('Null', default_detector)
      detector = farm[sd_id].get('detector', detector)
    else:
      detector = farm[sd_id].get('detector', pm.ros.create_detector(sdname, id=sd_id))
    # This is an internal per-subdetector counter used for the ROS ids
    # (for compatibility with the older script's indexing)
    counter['ROS-'+sdname] = 0
    
    all = []
    ros_per_partition = defaultdict(list)
    for k in range(0, len(farm[sd_id]['ros'])):
      current_ros = farm[sd_id]['ros'][k][1]
      readout_modules = []

      #test the first element
      if not isinstance(current_ros, dict):
        # this means it's an old farm(i.e. module_list is a channel list)
        # create a single module with index 0 and add all channels to it
        # & just use k as the ROS index (not important here)
        current_ros = { 'modules':[(0, current_ros)], 'index':k }

      rosname = ( current_ros.get('name', None) or "%s-%02d" % (sdname,counter['ROS-'+sdname]))

      module_list = current_ros['modules']
      for m in module_list:
        module_ics = []

        # Create & add channels to the current module
        for el in m[1]:
          try:
              # Explicit channel assignment
              c, idx = el
          except TypeError:
              # Plain ROB list
              c = el
              idx = m[1].index(c)

          if 'robin' in ros_mode:
            channel_index = idx
            # there are no default_* settings for robin
            if sd_id:
              channel_name = "ROL-%s-%02x%04x" % (rosname, sd_id, c)
            else:
              channel_name = "ROL-%s-%x" % (rosname, c)
            ic = pm.ros.create_ros_robin_data_channel(channel_name,
                config=channel_config, id=c,
                mem_pool=farm['robin_mem_pool'], physaddress=channel_index,)

          elif 'preloaded' in ros_mode:
            channel_name = 'ROL-' + '%06x' % c
            prel_chan_mem_pool = ros_common_objs.get('ROS-MemoryPool-Data', default_mem_pool)
            ic = pm.ros.create_ros_input_channel(channel_name,
                id=c, mem_pool=prel_chan_mem_pool)

          elif 'emulated' in ros_mode:
            channel_name = "ROL-%s-%x" % (rosname, c)
            ic = pm.ros.create_ros_input_channel(channel_name, id=c, mem_pool=mem_pool)
          else:
            raise RuntimeError("Error: invalid ros-mode '%s'" % ros_mode)
          module_ics.append(ic)

        # Create & add the module itself

        # physaddress is actually the module index in the ROS
        module_index = module_list.index(m)

        if 'preload' in ros_mode:
          module_name = "PRELOADED-MODULE-%s-%01x" % (rosname, module_index)
          module = pm.ros.create_ros_preloaded_module(module_name,
                                                      module_ics, 
                                                      physaddress=module_index, 
                                                      robinnp=robinnp)

        elif 'emulated' in ros_mode:
          # new farms should have emulated-config set, this is for the older format
          module_name = "EMULATED-MODULE-%s-%01x" % (rosname, module_index)
          module = pm.ros.create_ros_emulated_module(module_name,
                                                     module_ics, 
                                                     emu_config, 
                                                     physaddress=module_index, 
                                                     robinnp=robinnp)
        elif 'datadriven' in ros_mode:
          module_name = "ROBIN-%s-%d" % (rosname, module_index)
          module = \
              pm.ros.create_ros_robin_module(module_name,
                                             module_ics, 
                                             config=farm['robin_config'],
                                             physaddress=module_index,
                                             robinnp=robinnp,
                                             datadriven=True)
        elif 'robin' in ros_mode:
          module_name = "ROBIN-%s-%d" % (rosname, module_index)
          module = \
              pm.ros.create_ros_robin_module(module_name,
                                             module_ics, 
                                             config=farm['robin_config'],
                                             physaddress=module_index,
                                             robinnp=robinnp)
        else:
          raise RuntimeError("Error: invalid ros-mode '%s'" % \
              ros_mode)
        readout_modules.append(module)

      # Modules are ready now; create the actual ROS

      ros_config = farm.get('ros_config', default_ros_config)

      # DataOut and TriggerIn section
      #
      if ros_mode.endswith('reb'):
        # in this case we need to generate our data_out ourselves
        base_port = farm['reb'].get('tcp_base_port', 13000)
        data_out = pm.ros.create_tcp_data_out("ROS-"+rosname+"-TCPDO",
            base_port+current_ros['index'], reb_tcp_data_out_config)
      elif ros_mode.endswith('edo'):
        # No REB is generated here, just using the same DataOut
        data_out = ros_common_objs['REB-EmulatedDataOut']
      else:
        #data_out = ros_common_objs.get('ROS-DcDataOut', default_data_out)
        data_out = ros_common_objs.get('ROS-DFDataOut', default_data_out)

      if 'datadriven' in ros_mode:
        trigger_in = ros_common_objs.get('ROS-RobinDataDrivenTriggerIn', default_trigger_in)
      else:
        if robinnp:
          trigger_in = ros_common_objs.get('NPTriggerIn', default_trigger_in)
        else:
          trigger_in = ros_common_objs.get('ROS-DFTriggerIn', default_trigger_in)
      #

      ros_id = counter['ROS-'+sdname]
      if 'preload' in ros_mode:
        # use global ids for preloading
        ros_id = counter['ROS']

      ros = pm.ros.create_ros('ROS-'+rosname, config=ros_config, detector=detector,
          trigger_in=trigger_in, data_out=data_out, mem_pool=mem_pool,
          readout_module=readout_modules, debug_output=debug_output,
          id=ros_id, computer=farm[sd_id]['ros'][k][0],
          override_params=farm.get('ros_override_params', {}))

      # update the local counter
      counter['ROS-'+sdname] += 1
      # ...and the global one
      counter['ROS'] += 1

      all.append(ros)
      ros_per_partition[rosname[:-3]].append(ros)

    # ROSs are ready, create the relevant ResourceSet

    name = farm[sd_id].get('name', 'ROS-Segment-%d-%s' % \
        (counter['ROS-Segment'], sdname))

    # use resource sets instead of subsegments
    if 'robin' in ros_mode or 'emulated' in ros_mode:
      if robinnp:
        for k in ros_per_partition:
          rs = pm.ros.resource_set_and("%s-ROS" % k, 
                                       ros_per_partition[k])
          in_top_segment.append(rs)
      else:
        rs = pm.ros.resource_set_and("%s-ROS" % sdname, all)
        in_top_segment.append(rs)
    elif 'preload' in ros_mode:
      # ROSs are added directly to the segment in this mode
      # (this is the old way and it may be obsolete)
      in_top_segment += all
    else: # subsegments are not very useful
      subsegment = pm.ros.subsegment(name, run_control_app,
          all, farm[sd_id].get('default_host', None))
      in_top_segment.append(subsegment)
  if 'name' in farm:
    # no counter needed in this case
    name = 'ROS-' + farm['name'] + '-' + ros_mode
  else:
    name = 'ROS-Segment'+ ("-%d-" % counter['ROS-Segment']) + ros_mode

  segment = pm.ros.segment(name, run_control_app, in_top_segment,
      default_host=farm.get('default_host', localhost),
      override_params=farm.get('segment_override_params', {}))

  counter['ROS-Segment'] += 1
  # should be empty for -reb modes
  if 'reb' not in ros_mode or 'edo' not in ros_mode:
    safeInsert(segment, 'Infrastructure', [rdb_template])
  return segment

def gen_reb_segment(template, farm, reb_host):
  """
  Generates a REB segment that can be used with a (real) ROS segment.  A REB
  segment contains a single ROS and receives its input from the corresponding
  ROS segment. Since REB segments do not use message passing, each ROS in the
  ROS segments sends its data through a TcpDataOut objects, which is in turn
  received by the seperate input channels of an EthSequentialReadoutModule in
  REB ROS. It is therefore important for the two segments to use the correct
  TCP ports on each side and to also make sure that there will be no port
  clashes across segments in the generated partition!

  This method will generate a ROS segment based on your parametrization. The
  format of the segment is how it is normally setup in ATLAS Point1.

  Keyword parameters:

  template -- This must be a pm.project.Project where the default templated
  applications I should reside. It is normally loaded from a common (release)
  include file.

  farm -- This is the farm that was used to generate the ROS segment (a REB
  segment can only be created as the counterpart to a ROS segment). A few of
  these parameters are specific to the REB segment and are contained in a
  subdictionary:

  farm {
      ...
     'reb': {
         'input_fragment_type':  string|*
         'tcp_base_port': int|*,
         'tcp_data_out_config_override_params': {},
         'segment_override_params': {},
         'rcapp_override_params': {},
         'rcd_override_params': {},
     }
     ...
  }

  The base port parameter is used to define the TCP data ports that will be
  used and it defaults to 13000. Each port number is calculated as the sum of
  this base and the unique index number of each ROS (provided through the ROS
  farm).  The *_override_params can be used to directly override the properties
  of the generated DAL objects. (See the documentation in gen_ros_segment for a
  better description of the ROS farm).

  reb_host -- The dal.Computer object that hosts the RCD application

  counter -- This is a dictionary that contains the relevant counters for the
  generation of multiple ROS-Segments in a single database. If you are not
  interested in this, just don't set it. The default will work as expected.
  This dictionary will be updated by this method.
  """
  # Most of the logic implemented right here, since this is a special case
  # The farm should provide most of the parameters

  # load the ROS common configuration as an object cache (this is normally the
  # ros-common-config.xml file). It is currently necessary to have this in order
  # to create a REB segment, although in principle all the needed objects could
  # be generated

  from pm.utils import uniq

  if 'ros_common_config' in farm:
    ros_common_objs = pm.common.load(farm['ros_common_config'])
  else:
    raise RuntimeError("No ros_common_config key in the provided farm (needed for the REB segment)!")

  reb_rcapp = pm.ros.create_run_control_application(farm['name']+"-REB-Controller",
      computer=reb_host,
      program=ros_common_objs['rc_controller'],
      override_params=farm['reb']['rcapp_override_params'])
  #request_handlers=1 was hardcoded in the perl template
  rcd_config = pm.ros.create_ros_config('RCD-Config', request_handlers=1,
      override_params=farm['reb']['rcd_config_override_params'])

  reb_channels = []
  import eformat
  for sd_id in [k for k in list(farm.keys()) if isinstance(k, int)]:
    for ros_host in farm[sd_id]['ros']:
      index = ros_host[1]['index']
      rc = pm.ros.create_hw_input_channel('REB-Channel-'+str(index),
          index, physaddress=farm['reb'].get('tcp_base_port', 13000)+index,
          mem_pool=ros_common_objs['REB-MemoryPool-Data'])
      reb_channels.append(rc)

  # check uniqueness
  portlist = [rc.PhysAddress for rc in reb_channels]
  if len(portlist) != len(uniq(portlist)):
    raise RuntimeError("Error: Duplicate TCP ports in the ROS segment (check 'index' in the ROS farm)")

  eth_seq_module = pm.ros.create_eth_seq_readout_module( \
      name='REB-EthSequentialReadoutModule',
      channels=reb_channels, physaddress=0,
      input_fragment_type=farm['reb']['input_fragment_type'])

  detector = pm.ros.create_detector(farm['name']+'-REB', 1) #id is hardcoded as 1
  
  trigger_in = ros_common_objs['REB-DataDrivenTriggerIn']
  data_out = ros_common_objs['REB-EmulatedDataOut']
  mem_pool = ros_common_objs['ROS-MemoryPool-Headers']
  rcd = pm.ros.create_reb_ros('ROSEventBuilder', rcd_config, detector,
      trigger_in, data_out, mem_pool,
      [eth_seq_module], computer=reb_host,
      override_params=farm['reb']['rcd_override_params'])

  segment_override_params = {
      'ProcessEnvironment': [ros_common_objs['ROSEnvironment']] }
  segment_override_params.update(farm['reb'].get('segment_override_params',{}))
  segment = pm.ros.segment('REB-'+farm['name'], reb_rcapp, [rcd],
      default_host=reb_host, override_params=segment_override_params)

  return segment

# ==================
# end of ROS section
# ==================
