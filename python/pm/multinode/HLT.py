from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import re
import pm.common
import logging
import itertools

from pm.dal import dal, DFdal
from pm.dal import HLTSVdal
from pm.dal import DCMdal
from pm.dal import HLTMPPUdal
from pm.dal import PUDUMMYdal
from pm.dal import SFOngdal

from pm.common import localhost
from pm.multinode.GLOBALS import global_counter

dbg = False


def sub_rack_farm(racknodes, name, Nsubracks, infranode=None):
  """ Generates a l2pu or ef rack farm in a form suitable for splitting a physical rack
      into several logical racks, with the rack coral server at the same level as
      the logical racks.

  This rack farm should then become a list entry in
  the l2pu_farm argument of function l2_subfarm(l2sv, l2pu_farm, ...) or
  the ef_farm argument   of function ebf_subfarm(sfi, ef_farm, ...)

  Keyword arguments:

  racknodes -- if Nsubracks > 0, racknodes must be a list of dal.Computer objects for the physical rack
               in this case this list will be split into Nsubracks nearly equal-size subracks
               if Nsubracks <= 0, racknodes must be a list of lists of dal.Computer objects which become the subracks

  name      -- name of the rack farm. The subracks are then named <name>-sub-n (n starting from 1)

  Nsubracks -- number of subracks. See racknodes above.

  infranode -- a dal.Computer object specifying a node to be used for the rack servers
               if None, the last node of the last sub-racks will be used (and it will
               be removed from the sub-rack)
  
  """
  # check 1.
  if not isinstance(racknodes, list):
    raise SyntaxError('Parameter "racknodes" must be a list')

  # check 2.
  for k in racknodes:
    if not isinstance(k, dal.ComputerBase):
      raise SyntaxError('racknodes[%d] is not of type dal.ComputerBase' % \
    racknodes.index(k))

  # check 3.
  if not isinstance(name, str):
    raise SyntaxError('Parameter "name" must be a string. There is no default.')

  # check 4.
  if not isinstance(Nsubracks, int):
    raise SyntaxError('Parameter "Nsubracks" must be an int.')

  # check 5.
  if infranode is not None and not isinstance(infranode, dal.Computer):
    raise SyntaxError('Parameter "infranode" must be a dal.Computer object or None')


  # make sure the infranode is correctly dealt with
  rnodes = racknodes
  lrn = len(rnodes)
  if infranode:
    off = 0
  else:
    off = 1
  if Nsubracks > lrn - off :  # not enough nodes in list for Nsubracks
    raise ValueError('Not enough nodes provided (%d) for %d subracks' % (lrn,Nsubracks))

  # divide up the nodes if necessary
  if Nsubracks <= 0:
    if not infranode:
      infranode = rnodes[-1][-1]   # last node of last sublist is new infranode
      rnodes[-1] = rnodes[-1][:-1] # remove it from last sublist
    snodes = rnodes
  else:
    snodes = []
    if not infranode:
      infranode = rnodes[-1]
    nn = old_div((lrn+1-off), Nsubracks)
    #print 'lrn=%d, off=%d, Nsubracks=%d, nn=%d' % (lrn, off, Nsubracks, nn)
    lo = 0
    for n in range(Nsubracks):
      hi = min(lo+nn,lrn-off)
      #print 'lo=%d, hi=%d' % (lo,hi)
      ssnodes = rnodes[lo:hi]
      lo = hi
      snodes.append(ssnodes)

  lval = []
  for sn in snodes:
    rval = {}
    rval['default_host'] = infranode
    rval['is_histogram'] = infranode
    rval['is_resource']  = infranode
    suffix               = '-sub-%d' % (snodes.index(sn)+1)
    rval['name']         = name + suffix
    rval['hltpu']        = sn
    lval.append(rval)

  retval = {}
  retval['default_host'] = infranode
  retval['is_histogram'] = None
  retval['is_resource']  = None
  retval['name']         = name
  retval['subracks']     = lval

  return retval 

def hlt_subfarm(computer, default_host=None, is_histogram=None, is_resource=None, name=None):
  """Generates a HLT subfarm list, in the way gen_hlt_segment() expects.

  This method will create a python list for the representation of a HLT
  subfarm in the way gen_hlt_segment() expects.

  Keyword arguments:

  computer -- This is a list of computers or computer sets or Racks that will host HLTPU
  applications.

  default_host -- If set to a value different than None (the default), should
  be a dal.Computer that represents the default host for this segment.

  is_histogram -- If set to a value different than None (the default), should
  be a dal.Computer in which an IS server for histograms will be created.

  is_resource -- If set to a value different than None (the default), should
  be a dal.Computer in which an IS server for resources (counters) will be
  created.

  name -- If this variable is set, the name of the segment will be set to that
  value. Otherwise, we use the default. If you assign a name to this segment it
  will be *your task* to make sure that this name is unique through out the
  whole partition and include files. OKS may complain otherwise.

  This method checks for the following:

    1. All computers given are of type dal.ComputerBase
    2. Lists are lists of dal.Computers where applicable
    3. The 'name' parameter is set to a string

  If any of these is not the case, a SyntaxError exception is raised.
  """    
  
  if not isinstance(computer, list):
    raise SyntaxError('Parameter "computer" must be a list of computers')

  for k in computer:
    if not (isinstance(k, dal.ComputerBase) or isinstance(k, dal.Rack)):
      raise SyntaxError('computer[%d] is not of type dal.Rack or dal.ComputerBase' % computer.index(k))

  if default_host is not None and not isinstance(default_host, dal.Computer):
    raise SyntaxError('Parameter "default_host" must be of type dal.Computer')

  if is_histogram is not None and not isinstance(is_histogram, dal.Computer):
    raise SyntaxError('Parameter "is_histogram" must be of type dal.Computer')

  if is_resource is not None and not isinstance(is_resource, dal.Computer):
    raise SyntaxError('Parameter "is_resource" must be of type dal.Computer')

  if name is not None and not isinstance(name, str):
    raise SyntaxError('Parameter "name" must be of type str')

  retval = {}
  retval['hltpu'] = computer
  if default_host: retval['default_host'] = default_host
  if is_resource:  retval['is_resource']  = is_resource
  if is_histogram: retval['is_histogram'] = is_histogram
  if name:         retval['name']         = name
  return retval

def hlt_farm(hltsv, hlt_subfarms, sfo=[], default_host=None, is_histogram=None, name=None):
  """Generates a complete HLT farm.

  This method will create a complete HLT farm.

  Keyword arguments:

  hltsv -- This is a list of computers that will host HLTSV applications.

  hlt_subfarms -- a list of hlt_subfarm objects as generated by hlt_subfarm.

  sfo -- a list of computers where to run SFOs. If it is set to an empty list
  (the default), no SFOs will be available in the system.

  default_host -- If set to a value different than None (the default), should
  be a dal.Computer that represents the default host for this segment.

  is_histogram -- If set to a value different than None (the default), should
  be a dal.Computer in which an IS server for histograms will be created.

  name -- If this variable is set, the name of the segment will be set to that
  value. Otherwise, we use the default. If you assign a name to this segment it
  will be *your task* to make sure that this name is unique through out the
  whole partition and include files. OKS may complain otherwise.

  This method checks for the following:

    1. All computers given are of type dal.ComputerBase
    2. Lists are lists of dal.Computers where applicable
    3. The 'name' parameter is set to a string

  If any of these is not the case, a SyntaxError exception is raised.
  """    
  
  if not isinstance(hltsv, list):
    raise SyntaxError('Parameter "hltsv" must be a list of computers')

  for k in hltsv:
    if not isinstance(k, dal.ComputerBase):
      raise SyntaxError('hltsv[%d] is not of type dal.ComputerBase' % hltsv.index(k))

  if not isinstance(hlt_subfarms, list):
    raise SyntaxError('Parameter hlt_subfarms must be a list of hlt_subfarm objects')

  if not isinstance(sfo, list):
    raise SyntaxError('Parameter "sfo" must be a list of computers')

  for k in sfo:
    if not isinstance(k, dal.ComputerBase):
      raise SyntaxError('sfo[%d] is not of type dal.ComputerBase' % sfo.index(k))

  if default_host is not None and not isinstance(default_host, dal.Computer):
    raise SyntaxError('Parameter "default_host" must be of type dal.Computer')

  if is_histogram is not None and not isinstance(is_histogram, dal.Computer):
    raise SyntaxError('Parameter "is_histogram" must be of type dal.Computer')

  retval = {}
  if default_host: retval['default_host'] = default_host
  if is_histogram: retval['is_histogram'] = is_histogram
  if name:         retval['name']         = name
 
  retval['hltsv']    = hltsv
  retval['hlt_farm'] = hlt_subfarms # NOT a list of computers, but of hlt_subfarm objects!!
  retval['sfo']      = sfo

  return retval

def gen_hlt_segment(template, 
                    farm, 
                    hltpu_instances=1,
                    hltpu_numForks=1,
                    hltpu_numberOfAthenaMTThreads=8,
                    hltpu_numberOfEventSlots=8,
                    hlt_patch_area='',
                    preload=False, 
                    output=[], 
                    counter=global_counter,
                    real_hlt=False,
                    proxy=False):
  """Generates a HLT segment for a multinode system.

  This method will generate a top-level HLT segment for a multinode system.
  The segment is generated like it is normally generated for
  ATLAS Point1 operations. 

  Each segment level will use a RunControlTemplateApplication which is given as
  parameter to this method.  The controller will run on the segment's default
  host. The segment will have a specific IS server for resources (counters)
  connected to the top-level and is composed of sub-segments, each containing
  one HLT subfarm. Each HLT-subfarm contains a default host and an IS histogramming server, 
  dedicated to that subfarm.

  The user must input the farm configuration using a (python) dictionary
  representation. The "look" of this dictionary should be like this (each entry
  is a dal.Computer object or a dal.InfrastructureApplication where
  applicable)::

    farm = {'default_host': dal.Computer|*,
            'is_histogram': dal.Computer|*, (used only for the gatherer)
            'name': string|*,
            'hltsv': [dal.Computer, ...],  (list of host computer objects for HLTSVs)
            'hlt_farm': [{'default_host': dal.Computer|*,
                          'is_resource': dal.Computer|*,
                          'is_histogram': dal.Computer|*,
                          'name': string|*,
                          'hltpu': [dal.Computer, dal.ComputerSet, ...]
                          },
                          ...]
           }

  Optional values are marked with the "|*" option. This means that, if you
  don't set this entry in the dictionary a default action will be taken for
  that component. For "default_host"'s, the default action is to use the
  localhost (as returned by pm.farm.local_computer). For the IS servers, it
  means that applications will use the upper level servers. For this farm, the
  only required values are computers for the set of HLTSV's (at least
  one) and the HLTPU's (at least one).

  Keyword parameters:

  template -- This must be a pm.project.Project where the default templated
  applications I should reside. It is normally loaded from a common (release)
  include file.

  farm -- This is the simplified farm description, as explained above.

  hltpu_instances -- This is the number of instances of the HLTPU we will run.
  If you set this number to smaller than 1, a RuntimeError is raised.
  Currently, values > 1 also make no sense.

  preload -- If the HLTSVs will be preloaded with data

  output -- where the hltpu's send their output

  counter -- This is a dictionary that contains the relevant counters for the
  generation of multiple HLT-Segments in a single database. If you are not
  interested in this, just don't set it. The default will work as expected.
  This dictionary will be updated by this method.
  """
  if dbg:
    print('\nmultinode/HLT.py: gen_hlt_segment')
    print('farm',farm)

  import pm.hlt
  from pm.utils import safeInsert, computer_list

  # get the templates of interest
  rdb_template = \
      template.getObject("IPCServiceTemplateApplication", "DefRDB")
  #
  # default config rules
  #

  from pm.dal import MONSVC_CONFIGdal

  # OH

  def_OHparams = MONSVC_CONFIGdal.OHPublishingParameters('DefaultOHPublishingParameters')
  def_OHparams.PublishInterval = 5
  def_OHparams.OHServer = '${TDAQ_OH_SERVER=Histogramming}'
  def_OHparams.ROOTProvider = '${TDAQ_APPLICATION_NAME}'

  def_OHRule = MONSVC_CONFIGdal.ConfigurationRule('DefaultOHRule')
  def_OHRule.Parameters = def_OHparams


  dcm_OHparams = MONSVC_CONFIGdal.OHPublishingParameters('DcmOHPublishingParameters')
  dcm_OHparams.PublishInterval = 20
  dcm_OHparams.OHServer = '${TDAQ_OH_SERVER=Histogramming}'
  dcm_OHparams.ROOTProvider = '${TDAQ_APPLICATION_NAME}'

  dcm_OHRule = MONSVC_CONFIGdal.ConfigurationRule('DcmOHRule')
  dcm_OHRule.Parameters = dcm_OHparams


  hltsv_OHparams = MONSVC_CONFIGdal.OHPublishingParameters('HltsvOHPublishingParameters')
  hltsv_OHparams.PublishInterval = 5
  hltsv_OHparams.OHServer = 'Histogramming'
  hltsv_OHparams.ROOTProvider = '${TDAQ_APPLICATION_NAME}'

  hltsv_OHRule = MONSVC_CONFIGdal.ConfigurationRule('HltsvOHRule')
  hltsv_OHRule.Parameters = hltsv_OHparams


  hltpu_OHparams = MONSVC_CONFIGdal.OHPublishingParameters('HltpuOHPublishingParameters')
  hltpu_OHparams.PublishInterval = 80
  hltpu_OHparams.OHServer = '${TDAQ_OH_SERVER=Histogramming}'
  hltpu_OHparams.ROOTProvider = '${TDAQ_APPLICATION_NAME}'

  hltpu_OHRule = MONSVC_CONFIGdal.ConfigurationRule('HltpuOHRule')
  hltpu_OHRule.Parameters = hltpu_OHparams


  # IS

  def_ISparams = MONSVC_CONFIGdal.ISPublishingParameters('DefaultISPublishingParameters')
  def_ISparams.PublishInterval = 5
  def_ISparams.ISServer = '${TDAQ_IS_SERVER=DF}'

  def_ISRule = MONSVC_CONFIGdal.ConfigurationRule('DefaultISRule')
  def_ISRule.Parameters = def_ISparams


  dcm_ISparams = MONSVC_CONFIGdal.ISPublishingParameters('DcmISPublishingParameters')
  dcm_ISparams.PublishInterval = 5
  dcm_ISparams.ISServer = '${TDAQ_IS_SERVER=DF}'

  dcm_ISRule = MONSVC_CONFIGdal.ConfigurationRule('DcmISRule')
  dcm_ISRule.Parameters = dcm_ISparams


  hltsv_ISparams = MONSVC_CONFIGdal.ISPublishingParameters('HltsvISPublishingParameters')
  hltsv_ISparams.PublishInterval = 5
  hltsv_ISparams.ISServer = 'DF'

  hltsv_ISRule = MONSVC_CONFIGdal.ConfigurationRule('HltsvISRule')
  hltsv_ISRule.Parameters = hltsv_ISparams


  hltpu_ISparams = MONSVC_CONFIGdal.ISPublishingParameters('HltpuISPublishingParameters')
  hltpu_ISparams.PublishInterval = 5
  hltpu_ISparams.ISServer = '${TDAQ_IS_SERVER=DF}'

  hltpu_ISRule = MONSVC_CONFIGdal.ConfigurationRule('HltpuISRule')
  hltpu_ISRule.Parameters = hltpu_ISparams


  sfoWios_ISparams = MONSVC_CONFIGdal.ISPublishingParameters('SfoWiosDFISPublishingParameters')
  sfoWios_ISparams.PublishInterval = 5
  sfoWios_ISparams.ISServer = 'DF'

  sfoDefault_ISRule = MONSVC_CONFIGdal.ConfigurationRule('SfoDefaultISRule')
  sfoDefault_ISRule.Parameters = def_ISparams
  sfoDefault_ISRule.IncludeFilter = '.*'
  sfoDefault_ISRule.ExcludeFilter = '.*WIOS.*'

  sfoWios_ISRule = MONSVC_CONFIGdal.ConfigurationRule('SfoWiosISRule')
  sfoWios_ISRule.Parameters = sfoWios_ISparams
  sfoWios_ISRule.IncludeFilter = '.*WIOS.*'


  # config rule bundles

  defConfigRuleBundle = MONSVC_CONFIGdal.ConfigurationRuleBundle('DefaultConfigurationRuleBundle')
  defConfigRuleBundle.Rules = [def_OHRule, def_ISRule]

  dcmConfigRuleBundle = MONSVC_CONFIGdal.ConfigurationRuleBundle('DcmConfigurationRuleBundle')
  dcmConfigRuleBundle.Rules = [dcm_OHRule, dcm_ISRule]

  hltsvConfigRuleBundle = MONSVC_CONFIGdal.ConfigurationRuleBundle('HltsvConfigurationRuleBundle')
  hltsvConfigRuleBundle.Rules = [hltsv_OHRule, hltsv_ISRule]

  hltpuConfigRuleBundle = MONSVC_CONFIGdal.ConfigurationRuleBundle('HltpuConfigurationRuleBundle')
  hltpuConfigRuleBundle.Rules = [hltpu_OHRule, hltpu_ISRule]

  sfoConfigRuleBundle = MONSVC_CONFIGdal.ConfigurationRuleBundle('SfoConfigurationRuleBundle')
  sfoConfigRuleBundle.Rules = [def_OHRule, sfoDefault_ISRule, sfoWios_ISRule]


  configrulebundles = {}
  configrulebundles['default'] = defConfigRuleBundle
  configrulebundles['dcm']     = dcmConfigRuleBundle
  configrulebundles['hltsv']   = hltsvConfigRuleBundle
  configrulebundles['hltpu']   = hltpuConfigRuleBundle
  configrulebundles['sfo']     = sfoConfigRuleBundle
  

  hltpu_controller = \
    template.getObject("RunControlTemplateApplication", "DefRC")

  try:
    rc_controller = template.getObject("RunControlTemplateApplication", "DefRC")
  except RuntimeError:
    rc_controller = hltpu_controller
    
  hlt_controller = \
    template.getObject("RunControlTemplateApplication", "DefRC")

  if hltpu_instances < 1:
    raise RuntimeError('The minimum value for parameter "hltpu_instances" is 1')

  hltpu_tmpl = pm.hlt.create_hltpu("HLTMPPU-%d" % counter['HLTPU'],
                                   instances=hltpu_instances,
                                   hlt_patch_area=hlt_patch_area,
                                   real_hlt=real_hlt) 

  hltsv_plugin = 'internal'
  if preload:
    hltsv_plugin = 'preload'

  hltsv_list = []
  hltsv_list.append( pm.hlt.create_hltsv("HLTSV", 
                                         plugin=hltsv_plugin, 
                                         configrules=configrulebundles, 
                                         computer=farm['hltsv'][0]) )
  counter['HLTSV'] = 1

  nfarms = len(farm['hlt_farm'])
  
  hlt_pu_segment = []
  hltpu_histos = []


  if dbg:
    print('nfarms =',nfarms)

  for j in range(0,nfarms):
    hlt_farm = farm['hlt_farm'][j]
    name = hlt_farm.get('name','HLT-%d' % (j+1))
    if dbg:
      print('\nj = %d  name = %s' % (j,name))
      print('\nhlt_farm = ',hlt_farm)

    if 'subracks' in hlt_farm:

      # deal with subracks here

      # we first need a dummy parent HLTSegment that has the subracks as daughters
      # it has no apps and an empty Infrastructure, into which hltpm.common.add_db_proxy
      # will put the RackCoralProxy
      #
      # also the mucal rack server goes there (see mucal.py)
        
      subracks = hltpu_farm['subracks']
      Nsubracks = len(subracks)
      parentname = '-'.join(subracks[0]['name'].split('-')[:-2])
      def_host = subracks[0]['default_host']
      paseg = pm.hlt.hltpu_segment(parentname,
                                   hltpu_controller,
                                   hltpu=None,        # no hltpus in this segment
                                   numForks=0,
                                   numberOfAthenaMTThreads=0,
                                   numberOfEventSlots=0,
                                   configrules=None,
                                   computer=[],
                                   default_host=def_host)

      if dbg:
        print('found Nsubracks =',Nsubracks)

      segs = []
      for sr in subracks:
        srname = sr['name']
        if dbg:
          print('  subrack name =',srname)
          print('  default_host =',sr['default_host'].id)
          print()
        puseg = pm.hlt.hltpu_segment(srname,
                                     hltpu_controller,
                                     hltpu=hltpu_tmpl,
                                     numForks=hltpu_numForks,
                                     numberOfAthenaMTThreads=hltpu_numberOfAthenaMTThreads,
                                     numberOfEventSlots=hltpu_numberOfEventSlots,
                                     configrules=configrulebundles,
                                     computer=sr['hltpu'],
                                     default_host=sr.get('default_host',localhost),
                                     real_hlt=real_hlt)

        segs.append(puseg)

        paseg.Segments = segs
        paseg.Resources = []
        if dbg:
          print(paseg)

        hlt_pu_segment.append(paseg)

    else:

      # normal physical rack
      
      puseg = pm.hlt.hltpu_segment(name,
                                   hltpu_controller,
                                   hltpu=hltpu_tmpl,
                                   numForks=hltpu_numForks,
                                   numberOfAthenaMTThreads=hltpu_numberOfAthenaMTThreads,
                                   numberOfEventSlots=hltpu_numberOfEventSlots,
                                   configrules=configrulebundles,
                                   computer=hlt_farm['hltpu'],
                                   default_host=hlt_farm.get('default_host', localhost),
                                   real_hlt=real_hlt)

      hlt_pu_segment.append(puseg)

  # SFO

  # we start by creating the basic infrastructure

  sfo = []
  farm_sfo = computer_list(farm['sfo'])
  if len(farm_sfo) != 0:
    sfong_config = pm.hlt.create_sfong_config("SFO-Configuration-%d" % counter['SFO'], data_dirs=output)
    for k in range(0, len(farm_sfo)):
      sfo.append(pm.hlt.create_real_sfong("SFO-%d" % counter['SFO'],
                                          sfong_config, 
                                          configrules=configrulebundles['sfo'],
                                          computer=farm_sfo[k]))
      counter['SFO'] += 1

  # histogramming and gathering


  default_host = farm.get('default_host', localhost)
  topname = 'HLT'
  segment = pm.hlt.segment(topname,
                           rc_controller,
                           hlt_pu_segment,
                           hltsv_list,
                           sfo,
                           default_host,
                           real_hlt=real_hlt)

  if dbg:
    print('multinode/HLT.py:  gen_hlt_segment() segment=',segment)

  counter['HLT'] += 1

  # now the top-level segment construction

  name = farm.get('name', 'HLT')


  # now gathering
  is_histogram = farm.get('is_histogram', None)
  #pm.common.add_gatherer(segment, 'Histogramming',      # XXX MIG gatherer needed
  #  'HLT-.*', 'Top-HLT-', providers=list(hltsv_histos),
  #  computer=is_histogram, suffix='-HLT')

  #
  # db proxy
  # maybe not here...
  #pm.common.add_db_proxy(farm, template)

  segment.id = name

  if dbg:
    print('\n========')
    print('\nfarm:\n',farm)
    print('\n========')
    print('\nsegment:\n',segment)
    print('\n========\n\n')
  
  return segment

