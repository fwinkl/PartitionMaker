import sys
import re
import pm.common
import logging
import itertools
from pm.dal import dal, DFdal
from pm.common import localhost
from pm.multinode.GLOBALS import global_counter

# =====================
# begin of ROIB section
# =====================


def roib_farm(roib, feeder=None, default_host=None, name=None):
  """Generates a dictionary describing the farm structure of the RoIB segment.

  This method will generate the (simple) dictionary that describes the RoIB
  segment.

  Keyword parameters:

  roib -- This is the name of the single board PC that is connected to the
  RoIB. This machine is controllable by the online software

  feeder -- If this node is specified, should be the dal.Computer object to
  host the RoIB feeder.

  default_host -- This will be the default host for the controller.

  This method checks for the following:

    1. Lists are lists of dal.Computers where applicable
    2. The 'name' parameter is set to a string

  In any conditions, a SyntaxError exception is raised.
  """

  # check 1.
  if not isinstance(roib, dal.Computer):
    raise SyntaxError('Parameter "roib" must be a dal.Computer')

  if feeder is not None and not isinstance(feeder, dal.Computer):
    raise SyntaxError('Parameter "feeder" must be of type dal.Computer')

  if default_host is not None and not isinstance(default_host, dal.Computer):
    raise SyntaxError('Parameter "default_host" must be of type dal.Computer')

  # check 2.
  if name is not None and not isinstance(name, str):
    raise SyntaxError('Parameter "name" must be of type str')

  retval = {}
  if default_host: retval['default_host'] = default_host
  if feeder: retval['feeder'] = feeder
  retval['roib'] = roib
  if name: retval['name'] = name
  return retval

def gen_roib_segment(controller_template, farm, input_bitword, output_bitword,
    live_channels,  dcapp_config, datafiles=[]):
  """Generates an RoIB segment for a multinode system.

  This method will generate an RoIB segment that can be used in Point 1
  specific partitions (clearly, you need the hardware to run the RoIB itself).
  The input parametrization is small, but can be difficult to generate without
  an expert around you, since you actually need to know about the physical
  connection settings of the RoIB.

  Keyword parameters:

  controller_template -- This is the controller template to be used for a
  controller for this segment.

  farm -- This is a simplified farm description, a python dictionary, that you
  can generate with the roib_farm() method in this module. This is the
  description::

    farm = {
            'roib': dal.Computer,
            'default_host': dal.Computer|*,
            'feeder': dal.Computer|*,
            'name': string|*
           }

  Please note that by only setting the 'feeder' computer, it is not enough to
  generate a feeder object. You also need to set the datafiles parameter to
  be a non-empty list of files to be preloaded in the feeder. Parameters marked
  with '|*' are optional. If they are not given, a default action is taken.

  input_bitword -- This is the input bitword that will be used by the RoIB to
  switch on/off its connections with the LVL1 system. Check on the help of
  pm.roib.create_roib_config() for a more detailed explanation of this
  parameter.

  output_bitword -- This is the output bitword that will be used by the RoIB to
  switch on/off its connections with the LVL2 system (through the L2SVs). Check
  on the help of pm.roib.create_roib_config() for a more detailed explanation
  of this parameter.

  live_channels -- This is a list of live channels that will be enabled in the
  RoIB inputs.

  dcapp_config -- This the DCApplicationConfig object that will be used to
  configure the DFApplicationConfig of the RoIB and its feeder.

  datafiles -- This is a list of datafiles that will be loaded into the RoIB
  feeder (one will be created in the segment, if this list is not empty).
  """
  import pm.roib

  roib_config = pm.roib.create_roib_config('RoIBConfiguration-1',
      roib_input=input_bitword, roib_output=output_bitword,
      datafiles=[], live_channels=live_channels)
  roib = pm.roib.create_roib('RoIB-1', roib_config, dcapp_config,
      computer=farm['roib'])

  feeder = None
  if len(datafiles) > 0:
    feeder_config = pm.roib.create_roib_config('RoIBFeederConfiguration-1',
        roib_input=input_bitword, roib_output=output_bitword,
        datafiles=datafiles, live_channels=live_channels)
    feeder = pm.roib.create_roib('RoIBFeeder-1', feeder_config, dcapp_config,
        computer=farm.get('feeder', None))

  name = farm.get('name', 'RoIB-Segment-1')
  retval = pm.roib.segment(name, controller_template, roib, feeder,
      default_host=farm.get('default_host', localhost))

  return retval

# ===================
# end of ROIB section
# ===================
