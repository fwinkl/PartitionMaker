from __future__ import print_function
import sys
import re
import pm.common
import logging
import itertools

import os

dbg = False

from pm.dal import dal, DFdal
from pm.common import localhost

if dbg: print('multinode/PARTITIONS.py')

# ===========================
# begin of partitions section
# ===========================
  
from pm.multinode.HLT import *
from pm.multinode.ROS import *

def part_hlt(name, 
             hlt_farm, 
             ros_farm, 
             setup, 
             template, 
             hltpu_instances=1, 
             hltpu_numForks=1,
             hltpu_numberOfAthenaMTThreads=8,
             hltpu_numberOfEventSlots=8,
             hlt_patch_area='', 
             logroot='/tmp',
             workdir='/tmp/${TDAQ_PARTITION}',
             datafiles=[], 
             output=None,
             real_hlt=False,
             proxy=False,
             robinnp=False):
  """Generates a partition that contains HLT and ROS segments.
  
  This method generates a partition that has mostly defaults for a HLT + ROS
  operation on multiple hosts. Most of the things will be set to defaults, but
  then you can write functions that manipulate this base infrastructure to turn
  this partition into something useful for your testing.

  Keyword parameters:

  name -- This is the name you want assigned to this partition

  hlt_farm -- This is a (python) dictionary describing the HLT farm, as
  explained at this module's gen_hlt_segment() function.

  ros_farm -- This is a (python) dictionary describing the ROS/ROSE farm, as
  explained at this module's gen_rose_segment()/gen_ros_segment() function.

  setup -- This is a pm.project.Project object that was created with the
  "daq/segments/setup.data.xml" or any other database that contains the
  following objects (identifiers) which are linked from this partition:
  1. CommonEnvironment (type: dal.VariableSet) -- These are common variables
  provided to all applications
  2. CommonParameters (type: dal.VariableSet) -- These are the common
  parameterization that we have for the variables in the partition
  3. setup (type: dal.OnlineSegment) -- This includes many infrastructure
  applications, including, but not limited to the RootController, the
  "initial" partition, some base IS servers, etc.

  controller = template.getObject("RunControlTemplateApplication", "DefRC")

  hltpu_instances -- This is the number of instances of the HLTPU we will run. We
  always generate templated applications in multinode partitions.
  The default for this parameter is 1.

  hlt_patch_area -- a string giving the value of env var HLT-PATCH-AREA-1

  datafiles -- These are datafiles that are associated with the partition. If
  this is set, the HLTSVs and the ROSs/ROSEs will be properly set to preload
  them.

  Returns a dal.Partition object fully configured to be run.
  """
  preload = (len(datafiles) != 0)
  if dbg:
    print('multinode/PARTITION.py: part_hlt()')
    print('  preload  =',preload)
    print('  template =',template)
    print('  hlt_farm =',hlt_farm)
    print('  hlt_patch_area =',hlt_patch_area)
    print('  real-hlt =',real_hlt)

  hltseg = gen_hlt_segment(template, 
                           hlt_farm,
                           hltpu_instances=hltpu_instances,
                           hltpu_numForks=hltpu_numForks,
                           hltpu_numberOfAthenaMTThreads=hltpu_numberOfAthenaMTThreads,
                           hltpu_numberOfEventSlots=hltpu_numberOfEventSlots,
                           hlt_patch_area=hlt_patch_area,
                           preload=preload,
                           output=output,
                           real_hlt=real_hlt,
                           proxy=proxy)

  from pm.multinode.GLOBALS import global_counter
  my_ros_mode = 'emulated-dc'
  my_counter = global_counter
  my_roldatagen = False
  rosseg = gen_ros_segment(template, 
                           ros_farm, 
                           preload,
                           my_ros_mode,
                           my_counter,
                           my_roldatagen,
                           robinnp)

  df_parameters = pm.common.create_df_parameters("Default-DFParameters-1",
                                                 datafiles=pm.common.create_datafiles("DataFile", datafiles))

  partition = pm.common.create_partition(name,
                                         segments=[hltseg, rosseg], df_parameters=df_parameters,
                                         environment=setup.getObject("VariableSet", "CommonEnvironment"),
                                         parameters=setup.getObject("VariableSet", "CommonParameters"),
                                         online_segment=setup.getObject("OnlineSegment", "setup"),
                                         trigger_config=None, logroot=logroot, workdir=workdir)

  return partition

