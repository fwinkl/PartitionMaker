#
#  this is file  pm/multinode/GLOBALS.py
#
#=================
# global variables
#=================

includes = [
            #'daq/schema/core.schema.xml',
            'daq/schema/df.schema.xml',      
            'daq/schema/monsvc_config.schema.xml', 
            'daq/schema/dcm.schema.xml',     
            'daq/schema/HLTMPPU.schema.xml',
            'daq/schema/hltsv.schema.xml',   
            'daq/schema/MonInfoGatherer.schema.xml',
            'daq/schema/pudummy.schema.xml', 
            'daq/schema/siom.schema.xml',
            'daq/schema/SFOng.schema.xml',
            'daq/schema/DFTriggerIn.schema.xml',
            'daq/schema/ROSDescriptor.schema.xml',
            'daq/schema/RobinNPModule.schema.xml',
            'daq/schema/RobinNPDescriptorModule.schema.xml',
            'daq/schema/NPDescriptor.schema.xml',
            'daq/schema/PreloadedNP.schema.xml',
            'daq/sw/repository.data.xml', 
            ]
#
# These are master counters for our farm based generation. This technique will
# separate the farms in the following way:
global_counter = {}
#
# 1. Sub-level farms (L2PUs or EF farms) are numbered relatively to the farms
# they belong (L2SV or EBF). So, these do not need special numbering
# conventions. They will be called Farm-N-R, where N is the farm number while R
# is the relative subfarm number within that L2SV or EBF farm.
#
# 2. Farms (L2SV or EBF) are numbered sequentially through out the generation,
# therefore a global counter is required.
#global_counter['L2SV'] = 1
#global_counter['EF'] = 1 
#
# 3. Super-farms like L2 or EBEF are numbered sequentially through out the
# generation as well. Therefore, global counters are also required. The DFM
# names in EBEF farms match the index of the farm they are in.
#global_counter['L2SUBSEG'] = 1
#global_counter['L2'] = 1
#global_counter['L2RH'] = 1
#global_counter['EBEF'] = 1
#global_counter['SFI'] = 1
#
# the following may not all be needed
global_counter['HLTSV'] = 1
global_counter['HLTPU'] = 1
global_counter['HLT'] = 1
global_counter['DCM'] = 1
#
global_counter['SFO'] = 1
#
# 4. For ROS segments, we count the ROSs and the Segments
global_counter['ROS'] = 1
global_counter['ROS-Segment'] = 1

global_counter['PP_REC_LEVEL'] = 0
