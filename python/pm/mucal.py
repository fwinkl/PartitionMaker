#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Fri 20 Jun 2008 09:52:39 AM CEST 

"""Muon Calibration fundamentals.
"""
from __future__ import print_function

dbg = False

if dbg:
  print('entered pm/mucal.py')

import logging

from pm.dal import SIOMdal, MIGdal, DFdal, dal 
import pm.utils
import pm.common 

# Necessary memory pool for circular buffers
MemoryPool = DFdal.MemoryPool('MuonCalibrationPool')
MemoryPool.Type = 'MALLOC'
MemoryPool.NumberOfPages = 1024
MemoryPool.PageSize = 32768

# This is the Buffer configuration, default
MuonCalibrationConfig = DFdal.CircBuffer('MuonCalibrationConfig')
MuonCalibrationConfig.CircName = '/tmp/MUCalBuf/_Circ_'
MuonCalibrationConfig.CircSize = 4194304

#mucalConfig = ["'MuonCalibration_CircName'='/tmp/MUCalBuf/_Circ_'",
#               "'MuonCalibration_CircSize'='4194304'"]

mucalConfig = ["MuonCalibration_CircName=/tmp/MUCalBuf/_Circ_",
               "MuonCalibration_CircSize=4194304"]

# Input for Circular buffer readers on every XPU node
NodeReaderInput = SIOMdal.SIOMCircInput('MuonCalibrationReaderInput')
NodeReaderInput.MaxThreads = 4
NodeReaderInput.PollRateFactor = 3
NodeReaderInput.MemoryPools = [MemoryPool]

# RackServer (fixed) input
RackServerInput = SIOMdal.SIOMTCPInput('MuonCalibrationRackInput')
RackServerInput.MaxThreads = 16
RackServerInput.Port = 4444
RackServerInput.MemoryPools = [MemoryPool]

def create_siom_app(name, iss, host='AllButFirstHost'):
  """Creates a new SIOMTemplateApplication with in/outputs to be specified.

  Parameters:

  name -- This is the unique OKS name to be attributed to this application
  
  host -- if a real host object, create a normal SIOMApplication on that host
          otherwise must be a string whose value is either 'AllButFirstHost' or 'FirstHost'
          no other values allowed
  """
  if isinstance(host,dal.Computer):
    retval = SIOMdal.SIOMApplication(name)
    retval.RunsOn = host
  elif type(host) is str and (host == 'AllButFirstHost' or host == 'FirstHost'):
    retval = SIOMdal.SIOMTemplateApplication(name)
    retval.RunsOn = host
  else:
    print('Error in create_siom_app: argument host must be either a Computer or one of (FirstHost,AllButFirstHost)')
    import sys
    sys.exit(1)

  pm.common.setup_dfapp(retval, on_problems="Ignore")
  retval.ISServerName = iss
  retval.Program = pm.common.tdaqRepository.getObject('Binary','siom_application')

  if dbg:
    print('create_siom_app(name, iss, host)')
    print('  name',name)
    print('  iss ',iss)
    if isinstance(host,dal.Computer):
      print('  host',host.id)
    else:
      print('  host',host)
    print('retval=',retval)

  return retval

def populate_hlt_segment(segment, iss, top_server_output):
  """Populates an HLT segment with the muon calibration stuff.

  Parameters:

  segment -- This is the HLTSegment to be populated.

  iss -- This is the name of the IS server to be used by this rack

  top_server_output -- This is the TCP output to be used by the rack server
  """

  if dbg:
    print('\npopulate_hlt_segment:')
    print('segment',segment.id)
    print('iss',iss)
    print('top_server_output',top_server_output)

  #

  reader = create_siom_app(segment.id + '-Reader', iss=iss, host="AllButFirstHost")

  reader.Input = [NodeReaderInput]
  #node_to_rack_server = SIOMdal.SIOMTCPOutput(reader.id + '-' + rack_server_host.split('.',1)[0])
  node_to_rack_server = SIOMdal.SIOMTCPOutput(reader.id + '-MUCalRacks')
  node_to_rack_server.MinBufElem = 0
  node_to_rack_server.PollRateFactor = 5
  node_to_rack_server.RequiredFlags = 0
  node_to_rack_server.Port = 4444
  node_to_rack_server.MemoryPools = [MemoryPool]

  # find the correct data network on 10.1XY or 10.15
  #address = 'ToBeFixed'
  #for net in rack_server_host.Interfaces:   # XXX Check this!!!
  #  if net.IPAddress.find('10.147') == 0 or \
  #     net.IPAddress.find('10.150') == 0:
  #    address = net.IPAddress
  #    break

  #node_to_rack_server.ServerNode = address 
  reader.Output = [node_to_rack_server]
  
  # the rack server application
  # one for each rack
  iss = reader.ISServerName
  if dbg:
    print(' iss =',iss)
  server = create_siom_app(segment.id + '-MUCalRackServer', iss=iss, host="FirstHost")
  server.Input = [RackServerInput]
  server.Output = [top_server_output] 

  # now attach these to the segment

  pm.utils.safeInsert(segment, 'Applications', [server,reader])

  for app in segment.get('HLTRCApplication'):
    if hasattr(app,'MuonCalibrationConfig'):
      app.MuonCalibrationConfig = MuonCalibrationConfig
    else:
      app.extraParams += mucalConfig

def create_rack_output(name, top_app):
  """Generates the SIOMTCPOutput object from the top-level server application

  Parameters

  name -- The name of this Output object

  top_app -- This is the top-level application you must provide, already
  pre-configured.
  """
  if dbg:
    print('create_rack_output(name, top_app)')
    print('name   =',name)
    print('top_app=',top_app)

  retval = SIOMdal.SIOMTCPOutput('MUCalRacks-' + top_app.id)
  retval.MinBufElem = 0
  retval.PollRateFactor = 10 
  retval.RequiredFlags = 0
  retval.Port = 3333
  retval.MemoryPools = [MemoryPool]

  # find the correct data network on 10.147 or 10.150
  # XXX check !!!
  address = top_app.RunsOn.id
  for net in top_app.RunsOn.Interfaces:
    if net.IPAddress.find('10.147') == 0 or \
       net.IPAddress.find('10.150') == 0:
      address = net.IPAddress
      break
  
  retval.ServerNode = address 

  if dbg:
    print('retval =',retval)
  return retval

def create_top_level_segment(name, topserver_db, hlt_segment, build_in=True):
  """Creates the top-level Muon Calibration segment.

  Parameters:

  name -- The name of the segment

  topserver_db -- This is the database (pm.project.Project) object, provided by
  the user, containing the top-level server and its configuration.

  hlt_segment -- This is the top-level segment the user should pass in. It can
  be generated internally, during the same run by PM or externally.

  build_in -- This is a flag (True/False style) that determines if we should
  build the muon calibration applications into the given HLT segment, in which
  case the return value of this function is the modified HLT segment. Or, build
  a separate infrastructure for the calibration, in which case the return value
  of this function becomes a brand new segment, to be merged in the partition.
  """
  from pm.common import create_is_server

  if dbg:
    print('\ncreate_top_level_segment(name, topserver_db, hlt_segment, build_in):')
    print('name',name)
    print('topserver_db',topserver_db)
    print('hlt_segment',hlt_segment)
    print('build_in',build_in)

  
  # get the top-level server segment we must link-in
  server_segment = None
  server_app = None
  for s in topserver_db.getObject('Segment'):
    if s.Applications:
      for a in s.Applications:
        if isinstance(a, SIOMdal.SIOMApplicationBase):
          server_segment = s
          server_app = a
          break
    if server_segment is not None: 
      break

  if server_segment is None:
    raise RuntimeError('Cannot find in provided MUCal server segment (%s), any segment that contains a SIOMApplication object.' % topserver_db.get_impl_spec())

  # here we finally instantiate the top-level Muon Calibration segment
  if build_in:
    top = hlt_segment
  else:
    top = dal.Segment(name)
    # copies relevant properties
    top.IsControlledBy = hlt_segment.IsControlledBy
    top.DefaultHost = hlt_segment.DefaultHost

  pm.utils.safeInsert(top, 'Segments', [server_segment]) 

  # adds the IS server named SIOM
  # obs.: some mangling is required since we generate with '-iss' suffixed.
  is_server = create_is_server('siom', server_app.RunsOn)
  is_server.id = 'siom'
  is_server.Parameters = is_server.Parameters.replace('-iss', '')
  is_server.RestartParameters = is_server.Parameters
  
  pm.utils.safeInsert(top, 'Infrastructure', [is_server])

  # for every HLT segment inside "hlt_segment", add or place new infrastructure
  # at the top segment.
  top_server_tcp_output = create_rack_output('MUCal-TCPToTopServer',
                                             server_app)

  #for s in l2_segment.get('HLTSegment'):
  for s in hlt_segment.get('TemplateSegment'):
    if s.id[:4] != 'HLT-': continue  # make sure we are in HLT-1 etc

    if dbg:
      print('in TemplateSegment',s.id)
    change = s
    
    # else, we run on the DefaultHost of the segment... What can we do?
    if not build_in:
      change = dal.TemplateSegment('MUCal-%s' % s.id)
      change.IsControlledBy = hlt_segment.IsControlledBy
      #change.DefaultHost = rack_server_host

    # finally, we fill up the segment with our garbage.
    populate_hlt_segment(change, 'siom', top_server_tcp_output) 

    # and we place it there
    # XXX check
    if not build_in: pm.utils.safeInsert(top, 'Segments', [change])

  if dbg:
    print('top =',top)

  return top

def attach_top_level_segment(topserver_db, partition, build_in=True):
  """Creates the top-level Muon Calibration segment.

  Parameters:

  name -- The name of the segment

  topserver_db -- This is the database (pm.project.Project) object, provided by
  the user, containing the top-level server and its configuration.

  partition -- This is the partition you want this top-level muon calibration
  segment attached to.

  build_in -- This is a flag (True/False style) that determines if we should
  build the muon calibration applications into the given L2 segment, in which
  case the return value of this function is the modified L2 segment. Or, build
  a separate infrastructure for the calibration, in which case the return value
  of this function becomes a brand new segment, to be merged in the partition.
  """
  import re

  # XXX no more L2-Segment-*
  #exp = re.compile('^L2-Segment(\-\d+)?$')

  if dbg:
    print('attach_top_level_segment(topserver_db, partition, build_in)')
    print('topserver_db',topserver_db)
    print('partition',partition)
    print('build_in',build_in)

  exp = re.compile('^HLT(\-\d+)?$')
  for s in partition.Segments:
    if dbg:
      print('s',s)
    if exp.match(s.id):
      ms = create_top_level_segment('MUCal-' + s.id, topserver_db, s, build_in)
      if not build_in:
        pm.utils.safeInsert(s, 'Segments', [ms])
