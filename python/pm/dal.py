#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Fri Nov  9 14:21:38 2007 

"""Centralizes DAL creation to save time
"""
from __future__ import print_function
import os
import config
dbg = False

from config.dal import module as dal_module

# always build DAQ DALs from schema files installed in release to avoid side effects of rdb or oks git usage
# the TDAQ_DB_REPOSITORY cannot be unset => use 'norepo' parameter with oksconfig plugin
old_environ = dict(os.environ)
for var in ['TDAQ_DB', 'TDAQ_DB_USER_REPOSITORY']:
    if var in os.environ:
        os.environ.pop(var, None)
os.environ['TDAQ_DB_PATH'] = os.environ['TDAQ_INST_PATH'] + "/share/data"

# shared db to load all schemes
db = config.Configuration('oksconfig:&norepo')

# keep every created dal module
dals = []

dal = dal_module('dal', 'daq/schema/core.schema.xml', dals, '', db); dals.append(dal)
DFdal = dal_module('DFdal', 'daq/schema/df.schema.xml', dals, '', db); dals.append(DFdal)
SIOMdal = dal_module('SIOMdal', 'daq/schema/siom.schema.xml', dals, '', db); dals.append(SIOMdal)
if dbg: 
  print('dal.py: standard dals set up')

MIGdal = dal_module('MIGdal','daq/schema/MonInfoGatherer.schema.xml', dals, '', db); dals.append(MIGdal)
MONSVC_CONFIGdal = dal_module('MONSVC_CONFIGdal', 'daq/schema/monsvc_config.schema.xml', dals, '', db); dals.append(MONSVC_CONFIGdal)
HLTSVdal = dal_module('HLTSVdal', 'daq/schema/hltsv.schema.xml', dals, '', db); dals.append(HLTSVdal)
DCMdal = dal_module('DCMdal', 'daq/schema/dcm.schema.xml', dals, '', db); dals.append(DCMdal)
HLTMPPUdal = dal_module('HLTMPPUdal', 'daq/schema/HLTMPPU.schema.xml', dals, '', db); dals.append(HLTMPPUdal)
PUDUMMYdal = dal_module('PUDUMMYdal', 'daq/schema/pudummy.schema.xml', dals, '', db); dals.append(PUDUMMYdal)
SFOngdal = dal_module('SFOngdal', 'daq/schema/SFOng.schema.xml', dals, '', db); dals.append(SFOngdal)
DFTriggerIndal = dal_module('DFTriggerIndal', 'daq/schema/DFTriggerIn.schema.xml', dals, '', db); dals.append(DFTriggerIndal)
ROSDescriptordal = dal_module('ROSDescriptordal', 'daq/schema/ROSDescriptor.schema.xml', dals, '', db); dals.append(ROSDescriptordal)
RobinNPdal = dal_module('RobinNPdal', 'daq/schema/RobinNPModule.schema.xml', dals, '', db); dals.append(RobinNPdal)
RobinNPDescriptordal = dal_module('RobinNPDescriptordal', 'daq/schema/RobinNPDescriptorModule.schema.xml', dals, '', db); dals.append(RobinNPDescriptordal)
NPDescriptordal = dal_module('NPDescriptordal', 'daq/schema/NPDescriptor.schema.xml', dals, '', db); dals.append(NPDescriptordal)
PreloadedNPdal = dal_module('PreloadedNPdal', 'daq/schema/PreloadedNP.schema.xml', dals, '', db)

# destroy resources used by database
del db
del dals

# restore original environment
os.environ.clear()
os.environ.update(old_environ)

if dbg: 
  print('dal.py: EVOLUTION dals set up')
  from config.dal import get_classes
  d = get_classes(dal) ;                  print('dal                  d =', list(d.keys()))
  d = get_classes(DFdal) ;                print('DFdal                d =', list(d.keys()))
  d = get_classes(SIOMdal) ;              print('SIOMdal              d =', list(d.keys()))
  d = get_classes(MIGdal) ;               print('MIGdal               d =', list(d.keys()))
  d = get_classes(MONSVC_CONFIGdal) ;     print('MONSVC_CONFIGdal     d =', list(d.keys()))
  d = get_classes(HLTSVdal) ;             print('HLTSVdal             d =', list(d.keys()))
  d = get_classes(DCMdal) ;               print('DCMdal               d =', list(d.keys()))
  d = get_classes(HLTMPPUdal) ;           print('HLTMPPUdal           d =', list(d.keys()))
  d = get_classes(PUDUMMYdal) ;           print('PUDUMMYdal           d =', list(d.keys()))
  d = get_classes(SFOngdal) ;             print('SFOngdal             d =', list(d.keys()))  
  d = get_classes(DFTriggerIndal) ;       print('DFTriggerIndal       d =', list(d.keys()))  
  d = get_classes(ROSDescriptordal) ;     print('ROSDescriptordal     d =', list(d.keys()))
  d = get_classes(RobinNPdal) ;           print('RobinNPdal           d =', list(d.keys()))
  d = get_classes(RobinNPDescriptordal) ; print('RobinNPDescriptordal d =', list(d.keys()))
  d = get_classes(NPDescriptordal) ;      print('NPDescriptordal      d =', list(d.keys()))
  d = get_classes(PreloadedNPdal) ;       print('PreloadedNPdal       d =', list(d.keys()))

