"""A set of methods for generating computer farms as an OKS database.

This library contains a set of methods that help you generate computer farms
from scratch. It can SSH remote computers and decode node information
automagically.
"""

from past.utils import old_div
from pm.utils import unfold_patterns
import re
import logging

from pm.dal import dal

dbg = False

fallback_hwtag = 'x86_64-el9'

def execute_many(farm, sshtimeout):
    '''
    Execute a list of commands on a set of hosts.

    farm -  a dictionary whose key is the hostname
            and whose value is a list of strings, the commands to execute.

    returns
            a dictionary keyed by the host name, each entry containing
            another dictionary with keys:
              ['applications']:
                 a dictionary whose keys are the commands executed, each
                 value a dictionary with 'stdout', 'stderr' entries

              ['ssh']['return']: the status code of the ssh command
              ['ssh']['info']:   error message if status code != 0
    '''

    # The only portable way to see if we have a kerberos ticket
    # seems to be calling 'klist'.
    import subprocess
    kret = subprocess.run(['klist','-s'])

    from fabric import Result
    from fabric.group import ThreadingGroup

    clients = ThreadingGroup(*list(farm.keys()), connect_kwargs = { 'gss_auth': True, 'timeout': sshtimeout })

    result = {}

    for host in farm:
        result[host] = { 'applications': {},
                         'ssh' : { 'return': 0,
                                   'info': ''
                                   }
                         }
        cmds = farm[host]

    for cmd in cmds:

        try:
            ret = clients.run(cmd, hide=True)
        except Exception as ex:
            ret = ex.result

        for c, r in ret.items():
            result[c.host]['applications'][cmd] = { 'stdout' : '',
                                                    'stderr' : '' }
            if isinstance(r, Result) and r.ok:
                result[c.host]['applications'][cmd]['stdout'] = r.stdout
                result[c.host]['applications'][cmd]['stderr'] = r.stderr
            else:
                result[c.host]['ssh']['return'] = 1
                result[c.host]['ssh']['info'] = r

    return result

def network_computer(address_list, ignore_down=True, ssh_timeout=20):
  """Gets the node information via SSH returning dal.Computer objects.

  This method connects to the node IP address and retrieves its OS information.

  Keyword arguments (may be named):

  address_list -- A single pattern, string or a list of those that will be used
  for retrieving the information of the nodes you are interested in.

  ignore_down -- If this is set to "True" (the default), computers which cannot
  be reached will not be created. If set to "False", we will create a Computer
  object with default attributes.

  ssh_timeout -- How much time to wait for the results for every machine.
  """
  import sys
  import socket

  # The commands executed in each node.
  comm = [ 'hostname -f',
           'cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq',
           'cat /proc/cpuinfo',
           'cat /proc/meminfo',
           '/sbin/ip -oneline -4 addr show',
           'cat /etc/os-release',
           'uname -m' ]

  nodes = unfold_patterns(address_list)

  if len(nodes) == 0: return []

  # If any of the nodes is the local node, remove it from the list
  localname = [socket.getfqdn().split('.', 1)[0], socket.getfqdn()]
  ssh_nodes = [k for k in nodes if k not in localname]

  # Retrieving the information in parallel using the farm tools
  if len(ssh_nodes):
    farm = {}
    for n in ssh_nodes: farm[n] = comm
    ret = execute_many(farm, sshtimeout = ssh_timeout)
  else:
    ret = {}

  # Adds the localnode information, if it was asked
  if len(ssh_nodes) != len(nodes): ret[localname[0]] = get_localinfo()

  machines = []
  for name in list(ret.keys()):
    info = ret[name]['applications']

    if (int(ret[name]['ssh']['return']) != 0):
      logging.warning("Node %s is unavailable (code = %s)! Message: %s" % \
          (name, ret[name]['ssh']['return'], ret[name]['ssh']['info']))
      if not ignore_down:
        machines.append(dal.Computer(name, State=False, HW_Tag=fallback_hwtag))
    else:
      # Now we get the desired info from the generated strings.
      try:
        machines.append(decode_nodeinfo(info))
      except Exception as e:
        import traceback
        logging.warning("Problems decoding node info for node %s" % name)
        logging.warning("info[%s] = %s" % (name, info))
        logging.warning("traceback: %s" % traceback.format_exc())
        if not ignore_down:
          machines.append(dal.Computer(name,State=False,HW_Tag=fallback_hwtag))
        else:
          logging.warning("Not generating Computer object for host %s" % name)

  return machines

def local_computer():
  """Returns a dal.Computer object for the current computer."""
  try:
    return decode_nodeinfo(get_localinfo()['applications'])
  except:
    import socket
    name = socket.gethostname()
    logging.warning("Problems decoding node info for (local) node %s" % name)
    return dal.Computer(name,State=False,HW_Tag=fallback_hwtag)
  return None

def get_localinfo():
  """Runs through the known commands and fetch information for the local node.

  This method will fetch the information for the current node (CPU,
  architecture, network connectivity), without using SSH (which is faster).
  """
  import subprocess

  # The commands executed in each node.
  comm = [ 'hostname -f',
           'cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq',
           'cat /proc/cpuinfo',
           'cat /proc/meminfo',
           '/sbin/ip -oneline -4 addr show',
           'cat /etc/os-release',
           'uname -m' ]

  retval = {'ssh': {'return': 0}, 'applications': {}}
  for c in comm:
    res = subprocess.run(c, capture_output=True, shell=True)
    retval['applications'][c] = {}
    retval['applications'][c]['status'] = res.returncode
    retval['applications'][c]['stdout'] = res.stdout.decode('utf-8')

  #if dbg:
  #  print 'get_localinfo(): retval =\n',retval

  return retval

def decode_nodeinfo(info):
  """Decode the cpu, memory and net information returned by the OS.

  Receives the cpuInfo, memInfo and netInfo strings, obtained by
  'cat/proc/cpuinfo', 'cat /proc/meminfo' and '/sbin/ip', respectively.
  This method so, extracts the hardware information from these strings, and
  stores them in a map object.

  Keyword arguments (may be named):

  info -- The information returned by the FarmScanner.SSH_COMMAND.

  Returns a configured dal.Computer object with the retrieved information

  TODO: This needs a separated implementation for each decoder so it is simpler
  to read.
  """

  #Splitting the information.
  hostInfo = info['hostname -f']['stdout']
  cpufreq_safr = info['cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq']['stdout']
  cpuInfo  = info['cat /proc/cpuinfo']['stdout']
  memInfo  = info['cat /proc/meminfo']['stdout']
  netInfo  = info['/sbin/ip -oneline -4 addr show']['stdout']
  osInfo   = info['cat /etc/os-release']['stdout']
  archInfo = info['uname -m']['stdout']

  #Adding the node name.
  ret = dal.Computer(re.search('\S+', hostInfo).group(0))
  ret.CPU = 1000.0
  if len(cpufreq_safr) > 0: # then this should be a string with integer frequencies in kHz
    txt = cpufreq_safr.split()[0]
    try:
      ret.CPU = old_div(int(txt),1000) # return MHz
    except ValueError:
      # this machine doesn't seem to support the scaling_max_freq string
      # get speed from cpuinfo instead
      ll = cpuInfo.split('\n')
      for l in ll:
        indx = l.find('cpu MHz')
        if indx >= 0:
          ret.CPU = float(l.split(':')[1])
          break

  ret.Memory = int(old_div(float(re.search('MemTotal\s*:\s*(\d+)', memInfo).group(1)), 1000.0))
  ret.NumberOfCores = len(re.findall('processor\s*:\s*\d+', cpuInfo))
  typ = re.search('model name\s*:\s*(.+)', cpuInfo)
  if typ:
    ret.Type = typ.group(1)
  else:
    ret.Type = 'unkwown'
  #Getting the hardware tag information.
  #Calculating the correct HW tag.
  #Getting the OS tag.

  # parse /etc/os-release content into a dictionary
  os_info = {}
  for line in osInfo.splitlines():
      if line != '':
          k, v = line.split('=', 1)
          os_info[k] = v.strip('"')
  if 'rhel' in os_info['ID_LIKE']:
      osName = 'el'
  else:
      osName = os_info['ID']
  if osName == 'el':
      # redhat and clones: only major version
      osVersion = osName + os_info['VERSION_ID'].split('.', 1)[0]
  else:
      # e.g. ubuntu: major and minor
      osVersion = osName + ''.join(os_info['VERSION_ID'].split('.')[0:2])

  #Checking whether it is a valid architecture.
  regExp = re.compile('(?P<ARCH>\S+)-\S+')
  value = '%s-%s' % (archInfo.strip(), osVersion)

  try:
    ret.HW_Tag = value
  except ValueError as e:
    logging.warning("Problems setting hardware tag to %s at %s, forcing to %s" % \
        (value, ret.fullName(), fallback_hwtag))

  #Getting the network info.
  netInterfacesList = re.split('\n', netInfo)
  regExp = re.compile('\d+:\s*(?P<LABEL>\S+)\s+inet (?P<IP>\d+\.\d+\.\d+\.\d+)')
  ret.Interfaces = []
  for netInt in netInterfacesList:
    n = regExp.match(netInt)
    if n is not None:
      netInt = dal.NetworkInterface('%s-%s' %(ret.id, n.group('LABEL')))
      netInt.IPAddress = n.group('IP')
      netInt.InterfaceName = n.group('LABEL')
      if netInt.InterfaceName == 'lo':
        continue
      ret.Interfaces.append(netInt)

  return ret

#
def remap_hwtag_tdaq(computer, mappings={'x86_64-el9': 'x86_64-el9'}):
  """Remaps hardware tags from the computer to existing TDAQ binaries.

  This method will reset each computer in the given list to have hardware tags
  that are compatible with the current set of binaries available in the TDAQ
  release.

  Keyword arguments:

  computer -- A list of computers to change

  mappings -- This is a dictionary that maps strings to strings (existing
  hardware tags) to TDAQ available hardware tags. If the current tag does
  not exist in this dictionary, the tag remains unchanged. A warning message is
  issued in this case. A default dictionary is provided for the common cases at
  CERN.
  """
  for c in computer:
    if c.HW_Tag in mappings:
      c.HW_Tag = mappings[c.HW_Tag]
    else:
      logging.warning('No translations found for hw tag %s. Leaving it.' % \
          c.HW_Tag)

def set_network_label(computer, field, match, value="DATA"):
  """This method will set the network label of computers.

  This method allows you to easily set the "Label" field on a computer or set
  of computers NetworkInterfaces.

  Keyword parameters:

  computer -- A list of dal.Computer objects to have their interfaces set.

  field -- The name of the field you are filtering by. It has to be a valid
  filed in a dal.NetworkInterface object (common ones are "MACAddress",
  "IPAddress" and "InterfaceName").

  match -- This can be either a string or a regular expression that we will
  try the match with.

  value -- The actual value that is going to be set in the
  NetworkInterface.Label field. Valid values are "NONE", "CTRL", "DC1", "DC2",
  "EF" and "DATA" (the default).

  Raises AttributeError, if the value is not among the OKS accepted values.
  """

  for c in computer:
    if c.Interfaces is None: continue
    for ni in c.Interfaces:
      val = getattr(ni, field)
      if val is not None:
        if type(match) is str and val.find(match) != -1: ni.Label = value
        elif 'match' in dir(match) and match.match(val) is not None:
          ni.Label = value

def make_dict(computer, short_names=False):
  """Builds a python dictionary of computers in a list.

  This method builds a python dictionary from computers on a list using as
  keys, the "id" attributes of every dal.Computer.

  Keyword parameters:

  computer -- This should be a list of dal.ComputerBase objects in which, each
  should have an unique identifier. A check for this is conducted within this
  method.

  short_names -- A boolean indicating if the keys to the generated dictionary
  should correspond to the object id's or a short version of their internet
  names, which may be easier to address by the user in scripts.

  Returns a python dictionary where the keys are either the dal.Computer id's
  (or their short versions if set in the parameters), and the values are the
  dal.Computer object themselves.
  """
  retval = {}
  for c in computer:
    key = c.id
    if short_names and isinstance(c, dal.Computer):
      key = key.split('.', 1)[0]

    if key in retval:
      logging.warning('Found matching identifiers (%s, %s) at the given computer list. If you are setting the output dictionary with short names, and the names listed do not match, please consider setting short_names=False in this method. Otherwise, there is an error in your logic somewhere. Remember OKS requires object identifiers to be unique inside a database. I\'m not overwriting the existing entry.' % (retval[key].id, c.id))
      continue

    retval[key] = c
  return retval

def save(filename, computer, includes=[]):
  """Saves computers in an OKS database file.

  This method saved the computer list passed in the filename you designate.

  Keyword parameters:

  filename -- The name of the file you want to record data at

  computer -- A list of dal.Computer' or ComputerSet's you want to record in
  that file.

  includes -- A list of includes to put in the DB file generated

  All associated network information is also recorded together with the
  computers.
  """
  from pm.project import Project
  from pm.utils import merge_unique
  all_includes = merge_unique(includes, ['daq/schema/core.schema.xml'])
  db = Project(filename, all_includes)
  db.addObjects(computer)
  # the deletion of db should record the file, finally

def load(filename, short_names=False, includes=[], use_db_path=False):
  """Loads information of computers from an OKS database file.

  This method allows you to read all computer information (Computers and
  ComputerSets) inserted in any OKS database file. It returns a python
  dictionary where the keys are the dal.Computer object id's and the values are
  the dal.Computer objects themselves.

  Keyword parameters:

  filename -- the name of the file you want to read the computer objects from.

  short_names -- If this boolean is set to True, then, the keys of the returned
  dictionary will be the machines short names (i.e., without the domain
  qualifier). Check the method make_dict() in this module's documentation to
  understand possible limitations of this flag if you are having difficulties.

  includes -- If this parameter is given, the data on the include files is not
  considered to part of the information to load.

  use_db_path -- if True, tries to find the filename in the components of TDAQ_DB_PATH
  taking the first one that is found

  """

  from pm.project import Project
  import os

  mydbg = False
  fn = filename
  if mydbg:
    print('\nfarm.py: looking for file',fn)
    print('use_db_path =',use_db_path)
    print('PYTHONPATH      =',os.environ['PYTHONPATH'])
    print('TDAQ_PYTHONPATH =',os.environ['TDAQ_PYTHONPATH'])

  if use_db_path:
    tdaq_db_path = os.environ['TDAQ_DB_PATH'].split(':')
    if mydbg:
      print('TDAQ_DB_PATH =',tdaq_db_path)
    for path in tdaq_db_path:
      test_fn = path + os.sep + fn
      if mydbg:
        print('testing',test_fn)
      if os.path.exists(test_fn):
        if mydbg:
          print('found the file in path',path)
        fn = test_fn
        break

  if mydbg:
    print('farm.py: finally using file',fn)
    print()

  db = Project(fn)
  retval = make_dict(db.getObject('ComputerBase'), short_names)
  for i in includes:
    xdb = Project(i)
    for k in make_dict(xdb.getObject('ComputerBase'), short_names).keys():
      if k in list(retval.keys()): del retval[k]
  return retval

def subselect_pattern(d, pat):
  """Subselects a part of a farm and returns that in a new dictionary.

  This method will select a part of the farm dictionary in 'd', looking at the
  identifiers of the computers using the pattern 'pat'.

  Keyword arguments:

  d -- A dictionary as returned by the 'load()' method in this module

  pat -- The pattern or list of patterns to compare your match to.

  Returns a python dictionary with the nodes sub-selected.
  """
  from pm.utils import unfold_patterns
  retval = {}
  pat = unfold_patterns(pat)
  for k in list(d.keys()):
    for p in pat:
      if k.find(p) != -1: retval[k] = d[k]
  return retval

def subselect_regexp(d, regexp):
  """Subselects a part of a farm and returns that in a new dictionary.

  This method will select a part of the farm dictionary in 'd', looking at the
  identifiers of the computers using the (python) regular expression 'regexp'.

  Keyword arguments:

  d -- A dictionary as returned by the 'load()' method in this module

  regexp -- The (python) regular expression object to match the keys to. A
  string is also accepted as input in which case re.compile() is called
  internally.

  Returns a python dictionary with the nodes sub-selected.
  """
  if type(regexp) is str:
    from re import compile
    regexp = compile(regexp)

  retval = {}
  for k in list(d.keys()):
    if regexp.match(k): retval[k] = d[k]
  return retval

def filter_by_subdetectors(ros_farm, subdetectors):
  """
  This is a function to filter a ROS farm by its subdetectors.

  Arguments:
  ros_farm -- A ROS farm (as expected by pm.multinode.gen_ros_segment).
  subdetectors -- A list of dal.Detector objects that will remain in the farm.
  """
  for sd in subdetectors:
    if sd not in list(ros_farm.keys()):
      logging.warning("Warning: Subdetector %s was not added (probably because all of its ROSs were filtered out)" % sd.name)
  return dict([(sd,ros_farm[sd]) for sd in subdetectors if sd in subdetectors and sd in list(ros_farm.keys())])

def filter_hosts_by_file(hosts, filename):
  """
  This is a function to filter a host list using a file that contains the
  acceptable hostnames.

  Arguments:

  hosts -- A dictionary with the hostnames as keys and the dal.Computer objects
  as values

  filename -- The name of the file that contains the list of hostnames that
  should remain.
  """
  with open(filename, 'rb') as pclistfile:
    pclist = [line.strip() for line in pclistfile if not line.startswith('#')]
  return filter_hosts_by_list(hosts, pclist)

def filter_hosts_by_list(hosts, pclist):
  """
  This is a function that returns a host list using a list that contains the
  acceptable hostnames.

  Arguments:

  hosts -- A dictionary with the hostnames as keys and the dal.Computer objects
  as values

  pclist -- A list of the hostnames that should remain in 'hosts'.

  Returns a dictionary in the same format as the hosts parameter.
  """
  hosts = list(subselect_pattern(hosts, pclist).values())
  # subselect_pattern annoyingly returns hosts in a different format...
  return dict([((h.id).split('.')[0],h) for h in hosts])

def filter_hosts_by_pattern(hosts, pattern):
  """
  This is a wrapper over subselect_pattern that converts the return value to
  the same format as the hosts parameter.

  Arguments:

  hosts -- A dictionary with the hostnames as keys and the dal.Computer objects
  as values

  pattern -- A pattern of acceptable hosts in the format expected by
  subselect_pattern

  Returns a dictionary in the same format as the hosts parameter.
  """
  hosts = list(subselect_pattern(hosts, pattern).values())
  # subselect_pattern annoyingly returns hosts in a different format...
  return dict([((h.id).split('.')[0],h) for h in hosts])
