"""This module provides general purpose functions.

This unit provides general purpose functions that will be used through the
partitionMaker development.
"""
from __future__ import print_function

from builtins import range
from builtins import object
dbg = False

def computer_list(obj, s=None):
  """Returns a list of "Computer" objects found at the input object list. 
 
  This method will search for dal.Computer objects in the input list,
  recursively. The list initial list is composed of Computer and ComputerSet
  objects with possibly many nested levels of other ComputerSets. The scan will
  preserve the list order. This method does *not* support infinite loops.
 
  Parameters:

  obj -- This is a (python) list with objects you want to have inspected.
  Normally these are dal.Computer or dal.ComputerSet objects

  s -- This is an internal variable to hold the recursion this function
  implements. Do not use it.
  """
  from pm.dal import dal
  if s is None: s = []
  
  for k in obj:
    if isinstance(k, dal.Computer): s.append(k)
    elif isinstance(k, dal.ComputerSet): computer_list(k.Contains, s) 
  return s

def uniq(alist):
  """Uniq'ing method that preservers the order of lists.
  
  This method returns a list containing only one occurence of each item in the
  input list. The advantage of this method to the uniq method is that it
  preserves the order of the components. When producing the return list, only
  the first occurence is mantained.

  Parameters:
  alist -- A list containing the values that will be unique in the return list.
  
  Returns a list containing only one occurence of each item in the imput list,
  and preserving their order.

  Throws TypeError If the input parameter is not a valid list.
  """
  
  if not isinstance(alist, list):
    raise TypeError("Input parameter must be of type/subtype list")
  set = {}
  return [set.setdefault(e,e) for e in alist if e not in set]

def checkStringList(strList):
  """Checks whether a list contains only strings.

  Verifies, item by item, whether they are valid strings or not. The indexes of
  the non-string (or empty strings)) elements are loged and raised with an
  exception.

  Parameters:
  strList -- The list which the values you want to check whether they are valid
  strings.

  Throws TypeError If one or more values are non-string values or empty strings.
  """

  ret = []
  
  #verifying whether the nodes parameter is either a string or a list.
  i = -1
  for v in strList:
    i += 1
    if (type(v) not in [str, str]) or (v == ''):
      ret.append(i)

  if len(ret) != 0:
    raise TypeError('The input list contains invalid string values at ' + \
      'indexes:\n ' + ','.join(["%d" % (i) for i in ret])) 

def mkList(val):
  """If the parameter val is a list, returns val, otherwise, returns [val].
  """
  if isinstance(val, list): return val
  elif isinstance(val, tuple): return list(val)
  else: return [val]

def filterByRegExp(strList, regExp):
  """Filters a list of strings based on a regular expression.
  
  This method will return the string components of the input string list that
  match the regular expression specified. This method accept as input, a list
  of regular expressions.

  Parameters:
  strList -- A list containing the strings to be filtered.
  regExp  -- A regular expression, or a list of regular expressions that will
  be applied to the string list.
  
  Returns a list with the string items that matched one of the regular
  expressions passed.
  
  Throws TypeError If one or more values from the input parameters are
  non-string values or empty strings.
  """
  from re import compile
  
  if isEmpty(regExp): return []
  regExp = compile('|'.join(mkList(regExp)))
  return [s for s in strList if regExp.match(s) is not None]

def isEmpty(value):
  """Tests whether the parameter is empty or not.

  This method serves to test multiple cases where an parameter can be empty.
  For instance, a parameter can be empty either by have 'None' assigned to it,
  as well as '', [], {}, etc. This method will test all this cases to avoid
  problems.
  
  Parameters:
  value -- The value to be tested whether it is empty or not.

  Returns True if the passed value is empty.
  """

  if value is None: return True
  if hasattr(value, '__len__') and (len(value) == 0): return True
  return False

def safeInsert(obj, parameter, values):
  """Safely inserts values to a list belonging to an object.
  
  This method is a safe way to insert values in a multi-valued parameter in a
  class. It first tests whether the list is empty. If so, then a new list is
  created with the objects in the list passed. Otherwise, the objects are
  simply appended. The following operation is performed:

  if utils.isEmpty(obj.parameter): obj.parameter = values
  else: obj.parameter.extend(values)

  Parameters:
  obj       -- The object containing the attribute you want to insert values.
  parameter -- The name of the class attribute you want to insert the values.
  values    -- A list containing the values to be inserted.
  
  See pm.utils.isEmpty to understand what is considered an empty value.
  """
  par = getattr(obj, parameter)
  if isEmpty(par): par = values
  else: par.extend(values)
  setattr(obj, parameter, par)

def safeRemove(obj, parameter, values):
  """Safely removes values from a list belonging to an object.
  
  This method is a safe way to remove values from a multi-valued parameter in a
  class. If found in the list, the objects are removed. 

  Parameters:
  obj       -- The object containing the attribute you want to insert values.
  parameter -- The name of the class attribute you want to insert the values.
  values    -- A list containing the values to be inserted.
  """

  dbg = False

  par1 = getattr(obj, parameter)
  par2 = []

  if dbg:
    print('safeRemove %s par1: %s ' % (parameter,[e.id for e in par1]))

  for p in par1:
    if p not in values:
      if dbg: print('appending ',p.id)
      par2.append(p)

  par2 = uniq(par2)

  if dbg:
    print('safeRemove %s par2: %s ' % (parameter,[e.id for e in par2]))

  setattr(obj, parameter, par2)
  
def safeReverse(obj, parameter):
  """Reverse the order of the list given by parameter

  Parameters:
  obj       -- The object containing the attribute you want to reverse.
  parameter -- The name of the class attribute you want to reverse.
  """
  par = getattr(obj, parameter)
  par.reverse()
  setattr(obj, parameter, par)

def strcomp(val, pattern):
  """Compares a string val with a pattern.

  This method will compare a string value with a pattern. The pattern can be a
  compiled regular expression (re.compile()) object, i that case, the match
  object is used to check whether their are equal (pattern.match(val) is not
  None). Otherwise the test is done considering that pattern is a plain string,
  by doing val == pattern.
  """
  if hasattr(pattern, 'match'): return (pattern.match(val) is not None)
  else: return (val == pattern)

def modGet(vec, numItems = 1, startPos = 0):
  """Modular get operation.
  
  This method will get items in a vector, in a modular way.
  vec      -- The vector containing the objects to be got.
  numItems -- The number of items to be retrieved.
  startPos -- The index of the starting point for the search.

  Returns a list containing the elements retrieved. Each element appears only
  once.
  """
  vecSize = len(vec)
  ret = []
  for i in range(numItems): ret.append(vec[(startPos + i) % vecSize])
  return uniq(ret)

def unfold_patterns(l):
  """This function checks the list l for patterns and will unfold them for you
  making a full blown-up list of patterns and returning that.
  """
  from pm.pattern import expression
  from pm.utils import mkList, uniq

  l = mkList(l) #make sure we have got a list
  to_expand = []
  for k in l:
    if k.find('[') != -1: to_expand.append(k)

  for k in to_expand:
    l.remove(k)
    l.extend(expression([k]))

  l = uniq(l)
  return l
 
def merge_unique(i1, i2):
  """Merge the two include lists, making sure they remain unique.

  Keyword parameters:

  i1 -- A list or a string of includes
  i2 -- A list or a string of includes

  Returns a python list of includes, that is unique
  """
  return uniq(mkList(i1) + mkList(i2))
 
def merge_includes(inclist, modname, varname):
  """Merges includes from 'inclist' and the 'varname' variable at 'modname'.

  This method will merge the includes list given as the first parameter and the
  content of the variable 'varname' variable at the python module 'modname'.
  IF there is no 'varname' variable at the module given, returns 'inclist'.

  Keyword parameters:

  inclist -- The list of includes that is your starting point
  modname -- The name of the module (without the '.py' extension) that should
  be loaded with the new list called 'varname'.
  varname -- The name of the variable inside the module that should contain the
  list you are interested in.

  Returns a python list of includes, that is unique

  Does *not* raise ImportError. If 'varname' is not avaiable at 'modname', it
  returns 'inclist'.
  """
  try: 
    ns = {}
    exec('from %s import %s as newincludes' % (modname, varname), ns)
    newincludes = ns['newincludes']
  except ImportError: return inclist
  return merge_unique(inclist, newincludes)

def absolute_path(include, module_file_path):
  """Transforms relative names to absolute path names in modules

  This method takes as input, the file you want to have included in your
  partition and will make sure that its path is not set relatively, but
  absolutely, with respect to the module_file_path. If the path is already
  absolute (starts with os.sep), then nothing is done.
  """
  import os.path
  path = os.path.dirname(os.path.realpath(module_file_path))
  if include[0] == os.sep: return include
  else:
    if include.find(os.sep) == -1: return path + os.sep + include
    else: return path + include[include.find(os.sep):]

class hltEnv(object):

  def __init__(self):
    self.workarea = None
    self.patcharea = None
    self.release = None

  def set_hlt_env(self,kwargs):
    """ modify the environment for the HLT release
    kwargs -- a dictionary that should/may contain the keywords
              workarea
              patcharea
              release

    """
    if dbg:
      print('set_hlt_env(kwargs): kwargs =',kwargs)

    if 'workarea' in kwargs:
      self.workarea = kwargs['workarea']
      if dbg: print('opt["workarea"] =',self.workarea)
    else:
      print("\nINFO: no keyword 'workarea' in option['hlt-implementation']")
      print('      workarea from user env or no workarea will be used')
    #
    if 'patcharea' in kwargs:
      self.patcharea = kwargs['patcharea']
      if dbg: print('opt["patcharea"] =',self.patcharea)
    else:
      print("\nINFO: no keyword 'patcharea' in option['hlt-implementation']")
      print('      patcharea from user env or no patcharea will be used')
    #
    if 'release' in kwargs:
      self.release = kwargs['release']
      if dbg: print('opt["release"] =',self.release)
    else:
      print("\nINFO: no keyword 'release' in option['hlt-implementation']")
      print('      only release area from user env will be used (MUST exist in this case)')
    #
    import os
    orig_tdp = os.environ['TDAQ_DB_PATH']
    tdp = orig_tdp.split(os.pathsep)
    #
    # assume workarea  in user env is 1st element
    #        patcharea in user env is the first element containing 'AtlasP1HLT'
    #        release   in user env is the first element containing 'AtlasHLT' 
    #
    env_workarea = tdp[0]
    idx_pa = -1
    for e in tdp:
      if dbg: print('pa   e =',e)
      if 'AtlasP1HLT' in e.split(os.sep):
        idx_pa += 1
        if dbg: print('pa: found!')
        break
    #
    idx_rl = -1
    for e in tdp:
      if dbg: print('rl   e =',e)
      if 'AtlasHLT' in e.split(os.sep):
        idx_rl += 1
        if dbg: print('rl: found!')
        break
    #
    new_tdp = tdp[:]
    #
    if not self.release:
      if idx_rl < 0:
        print("\nERROR: you don't seem to have an AtlasHLT release in TDAQ_DB_PATH:")
        for e in tdp:
          print(e)
        import sys
        sys.exit(1)
    else:
      if dbg: 
        print('release =',self.release)
        print('idx_rl =',idx_rl)
        print('\nrelease.split(os.pathsep)')
        print(self.release.split(os.pathsep))
      if self.release != 'P1':
        new_tdp = self.release.split(os.pathsep) + new_tdp[:]
    #
    if self.patcharea:
      new_tdp = self.patcharea.split(os.pathsep) + new_tdp[:]
    #
    if self.workarea:
      new_tdp = self.workarea.split(os.pathsep) + new_tdp[:]
    #
    os.environ['TDAQ_DB_PATH'] = os.pathsep.join(new_tdp)
    #
    if dbg:
      print('\nnew TDAQ_DB_PATH by components:')
      for p in new_tdp:
        print(p)
    #
    print('\ngenerated partition/segment will use new TDAQ_DB_PATH:')
    print()
    print(os.environ['TDAQ_DB_PATH'])
    print()

  def get_workarea(self):
    return self.workarea

  def get_patcharea(self):
    return self.patcharea
