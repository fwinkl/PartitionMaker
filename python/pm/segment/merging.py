#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Mon 17 Sep 2007 06:50:24 PM CEST 

import pm.option.common as common
import pm.option.specific as specific
import pm.option.segcommon as segcommon

import copy
option = copy.deepcopy(common.option)

# This is a list of specific options that need to be taken from the utils
# you can look them up over there to see their meaning

wanted = ['partition-name',
          'trigger-config',
          'segments']

for k in wanted: option[k] = copy.deepcopy(specific.option[k])
for k in ['extra-includes']: option[k] = copy.deepcopy(segcommon.option[k])

for k in ['dcapp-config', 'control-only' ]: 
  if k in option: del option[k]

def gendb(**kwargs):
  """Generates a partition from its broken-down segment structure. 

  This method will generate a partition object according to user options, from
  its broken-down segment structure.

  Returns a dictionary that contains, as keys, the 'partition' (dal.Partition
  object) and the 'includes' python list of includes necessary to make the
  partition a valid OKS database.  
  """
  import pm.common 
  import pm.utils
  import pm.project
  import pm.multinode

  # get the basic includes
  includes = list(pm.multinode.includes)

  datafiles = []
  if isinstance(kwargs['data'], list):
    datafiles = pm.utils.unfold_patterns(kwargs['data'])

  obj_cache = pm.common.load(kwargs['extra-includes'])
  segments = [obj_cache[k] for k in kwargs['segments']]
  setup = pm.project.Project(kwargs['setup-file'])

  if kwargs['df-parameters'] not in obj_cache:
    df_parameters = pm.common.create_df_parameters("Default-DFParameters-1",
        network_protocol='tcp',
        datafiles=pm.common.create_datafiles("DataFile", datafiles))
  else:
    df_parameters = obj_cache[kwargs['df-parameters']]
 
  if kwargs['trigger-config'] not in obj_cache:
    raise RuntimeError('No %s@TriggerConfiguration found at the extra includes' % \
        kwargs['trigger-config'])
  
  partition = pm.common.create_partition(kwargs['partition-name'],
      segments=segments, df_parameters=df_parameters,
      environment=setup.getObject("VariableSet", "CommonEnvironment"),
      parameters=setup.getObject("VariableSet", "CommonParameters"),
      online_segment=setup.getObject("OnlineSegment", "setup"),
      trigger_config=obj_cache[kwargs['trigger-config']])
 
  # sets the RepositoryRoot at the partition level
  partition.RepositoryRoot = kwargs['repository-root']

  # adds up the segments
  partition.Segments = [segments]

  # saves the new partition on a new database
  includes = pm.utils.merge_unique(includes, kwargs['setup-file'])
  if len(kwargs['extra-includes']) != 0:
    includes = pm.utils.merge_unique(includes, kwargs['extra-includes'])
  
  return {'partition': partition, 'includes': includes} 
