#!/usr/bin/env tdaq_python
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 10-May-2006

import pm.option.segcommon as common
import pm.option.specific as specific

import copy
option = copy.deepcopy(common.option)

# This is a list of specific options that need to be taken from the common pool
# you can look them up over there to see their meaning
wanted = [
          'ros-farm',
          'dcapp-config',
          'ros-mode',
          'separate-channels',
          'remove-segment',
          'roldatagen',
          'reb-host',
          'preload-channels',
          'preload-hosts',
          'preload-subdetectors',
          'preload-group',
          'robhit-module',
          'robinnp',
          ]

for k in wanted: option[k] = copy.deepcopy(specific.option[k])

def gendb(**kwargs):
  """Generates a ROS segment you can attach to an existing partition.

  This method generates a multinode ROS(E) segment that can be attached to your
  existing partition.

  Returns a dictionary that contains, as keys, the 'segment' (dal.Segment
  object) and the 'includes' python list of includes necessary to make the
  segment a valid OKS database.
  """
  import pm.common
  import pm.utils
  import pm.multinode
  import logging
  from pm.project import Project

  # my list of includes
  includes = list(pm.multinode.includes)

  # and the farms

  if isinstance(kwargs['ros-farm'], dict):
    # farm was dynamically generated
    # this is an ugly way to pass the farm, but it does the job
    ros_farms = kwargs['ros-farm']
    includes = pm.utils.merge_unique(includes, ros_farms['includes'])
  else:
    # annoying issue
    if kwargs['ros-farm'].endswith('.py'):
      kwargs['ros-farm'] = kwargs['ros-farm'].rstrip('.py')

    ns = {}
    exec('from %s import ros_farm as ros_farms' % kwargs['ros-farm'], ns)
    ros_farms = ns['ros_farms']
    includes = pm.utils.merge_includes(includes, kwargs['ros-farm'], 'includes')

  datafiles = []
  if isinstance(kwargs['data'], list):
    datafiles = pm.utils.unfold_patterns(kwargs['data'])

  obj_cache = pm.common.load(includes)
  extra_obj_cache = pm.common.load(kwargs['extra-includes'])

  segs = []
  if not isinstance(ros_farms, list): ros_farms = [ros_farms]

  # run the generation for the many ROS farms
  for ros_farm in ros_farms:
    seg = pm.multinode.gen_ros_segment(Project(kwargs['template-file']),
                                       ros_farm, 
                                       preload=isinstance(kwargs['data'], list),
                                       ros_mode=kwargs['ros-mode'], 
                                       roldatagen=kwargs['roldatagen'], 
                                       robinnp=kwargs['robinnp'])
    segs.append(seg)
    if kwargs['ros-mode'].endswith('-reb'):
      # also create a ROS Event Builder segment (saved later in a different xml file)
      reb_hostname = kwargs['reb-host']
      try:
        reb_host = obj_cache[reb_hostname]
      except KeyError:
        # drop suffix after last - and try again
        reb_hostname =  ( str.join('-', reb_hostname.split('.')[0].split('-')[:-1]) ) + \
            '.' + str.join('.', reb_hostname.split('.')[1:])
        # let the exception float to top now
        reb_host = obj_cache[reb_hostname]

      reb_seg = pm.multinode.gen_reb_segment(Project(kwargs['template-file']),
        ros_farm, reb_host=reb_host)

    # special goodies
    import pm.special
    if kwargs['control-only']: pm.special.empty_control_tree(seg)
    #if kwargs['no-templates']: pm.special.untemplatize(seg)

  includes = pm.utils.merge_unique(includes, kwargs['template-file'])
  if len(kwargs['extra-includes']) != 0:
    includes = pm.utils.merge_unique(includes, kwargs['extra-includes'])

  retval = {'segment':segs, 'includes':includes}
  if kwargs['ros-mode'].endswith('-reb'):
    retval['rebsegment'] = [reb_seg]
  return retval
