#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 11 Sep 2007 10:46:38 AM CEST 

"""Instructions and utilities to build RoIB segments for the LVL2 trigger.
"""
from __future__ import absolute_import

from builtins import range
from pm.dal import dal, DFdal

def segment(name, controller, roib, feeder=None, default_host=None):
  """Generates an RoIB segment that can optionally contain an RoIB feeder.
  
  This method will generate an RoIB segment you can attach to your top level
  LVL2 segment (or where it is more convinient to you).

  Keyword parameters:

  name -- This is the name given to the OKS object returned. Something like
  RoIBSegment would do.

  controller -- This is either a RunControlApplication or a
  RunControlTemplateApplication to be used for controlling this segment.

  roib -- This is the RoIBApplication to be used in this segment

  feeder -- This is an optional RoIBApplication object that will play the role
  of an RoIB feeder. If you don't specify it or it is set explicetly to None,
  no feeder will be setup.

  default_host -- The dal.Computer which will be used as the default host for
  this segment. If not specified, the system will take the default host for the
  upper segment.

  Returns a dal.Segment with the applications you specified
  """
  from .dal import Segment
  retval = Segment(name, IsControlledBy=controller)
  retval.Applications = [roib]
  if feeder: retval.Applications.append(feeder)
  if default_host: retval.DefaultHost = default_host
  return retval

def create_feeder(name, roib_config, dcapp_config, computer=None):
  """Generates an RoIB feeder application object.

  This method will generate an RoIB feeder application object.

  Keyword parameters:

  name -- This is the name given to the OKS object returned. Something like
  RoIBConfiguration-1 would do.

  roib_config -- This is the DFdal.RoIBConfiguration object to use for this
  application.
  
  dcapp_config -- The DFdal.DCApplicationConfig object to use for the RoIB
  application.
 
  computer -- This is the computer object where the RoIB will run. If not
  specified, the RoIB will run on the segment's default host.
  """
  from pm.common import tdaqRepository
  
  retval = create_roib(name, roib_config, dcapp_config, computer)
  retval.Program = tdaqRepository.getObject('Binary', 'roib_feeder')

  # we make sure that the roibTtype is correctly set...
  roib_config.roibTtype = 'TRG_ROD'
  return retval
  
def create_roib(name, roib_config, dcapp_config, computer=None):
  """Generates an RoIB application object.

  This method will generate an RoIB application object.

  Keyword parameters:

  name -- This is the name given to the OKS object returned. Something like
  RoIBConfiguration-1 would do.

  roib_config -- This is the DFdal.RoIBConfiguration object to use for this
  application.
  
  dcapp_config -- The DFdal.DCApplicationConfig object to use for the RoIB
  application.
 
  computer -- This is the computer object where the RoIB will run. If not
  specified, the RoIB will run on the segment's default host.
  """
  from DFdal import RoIBApplication
  from pm.common import tdaqRepository, setup_dfapp 

  retval = RoIBApplication(name, RoIBConfiguration = roib_config)
  retval.DFApplicationConfig = dcapp_config
  if computer: retval.RunsOn = computer
  retval.Program = tdaqRepository.getObject('Binary', 'roib_main') 
  setup_dfapp(retval)
  return retval

def create_roib_config(name, roib_input, roib_output, datafiles=[], 
    live_channel=0xffff): 
  """Generates an RoIB configuration object.
  
  This method will generate an RoIB configuration object
  (i.e., a DFdal.RoIBConfiguration object). 
  
  Keyword parameters:

  name -- This is the name given to the OKS object returned. Something like
  RoIBConfiguration-1 would do.
  
  roib_input -- The bit mask of the RoI Builder input channels. Channels which
  are marked active have their bit set to 1. An RoIB developer should be able
  to get you a valid word to be set here. It depends on the RoIB <-> LVL1
  connection settings.
  
  roib_output -- The bit mask of the RoI Builder output channels. Channels
  which are marked active have their bit set to 1. You can generate this mask
  using the gen_output_channel_bitword() function in this module. It is based
  on the physical connection settings between the RoIB and the L2SVs.
  
  datafiles -- A (python) list containing test files that will be pre-loaded
  to the RoI Builder. If this list is empty (the default), then no test files
  will be passed to the RoI Builder.

  live_channel -- a bit mask describing live input channels. If not specifyed,
  then all channels will be enabled (0xffff is the default).
  
  Returns a DFdal.RoIBConfiguration object.
  """
  from DFdal import RoIBConfiguration
  
  retval = RoIBConfiguration(name)
  retval.InputModuleSlots = [15, 14, 13],
  retval.OutputModuleSlots = [16],
  retval.EnableStandalone = False
  retval.eventrate = 0
  retval.EnableFlowControl = True
  retval.MaxInputSkew = 100
  retval.ActiveInputChannels = gen_detector_strlist(roib_input, 4, 3)
  retval.ActiveOutputChannels = gen_detector_strlist(roib_output, 3, 4)
  retval.liveChannel = live_channel

  return retval

def gen_bitword(il):
  """Generates a bitword mask based on the integers passed.

  This method will generate a bitmask based on the numbers passed in the
  integer list. For each number, the corresponding bit will be set in the
  output word (a 32-bit integer).
  """
  word = 0x0
  for k in il: word |= (0x1 << k)
  return word

def gen_detector_strlist(bitword, num_boards, num_channels):
  """Generates the configuration string for the RoI builder.
  
  This method generates the configuration string for the RoI builder input and
  output channels based on a bit word. It takes the information about which
  channels are available, and converts it to the way it must be stored within a
  partition. The information is passed to this method as a bit word, where each
  bit represents a channel. Since a RoI builder contains multiple boards, and
  each board has multiple channels, the bits corresponding to each channel, in
  each board, are organized so that you have first all the channels bits for
  the first board, then the channel bits for the second board, and so on.
  Imagine that we have 4 boards, each one with 3 channels.  Then the bit word
  should be organized in the following way:

    - bit 0: Channel 0 of the first board.
    - bit 1: Channel 1 of the first board.
    - bit 2: Channel 2 of the first board.
    - bit 3: Channel 0 of the second board.
    - bit 4: Channel 1 of the second board.
    - bit 5: Channel 2 of the second board.
    - bit 6: Channel 0 of the third board.
    - ...

  Keyword parameters:

  bitword -- The bit word configuring the channels (bit 1 means enabled). If
  this value is given as python list of integers that indicate which channels
  should be enabled, the method gen_bitword() will be used.
  num_boards -- Total number of boards.
  num_channels -- Number of channels in each board.

  Returns a (python) string list with the information regarding with channels
  are enabled in the correct way to be used by the RoIBConfiguration object.
  """
  
  ret = ['' for i in range(num_boards * num_channels)]
  for b in range(num_boards):
    for c in range(num_channels):
      bit = c + (b*num_boards)
      if ( ((bitword >> bit) & 0x1) == 1 ): ret[bit] = 'SET'
  return ret

def gen_output_channel_bitword(possible_nodes, active_nodes):
  """Generates the bit word that configures the output channels for the RoIB.
  
  Generates the bit word that will configure the output channels for the RoI
  Builder. This function will get the full mappings, that associate actual
  computer (L2SV) nodes to the output buses of the RoIB and will output a word
  that corresponds to the bit pattern that activates the RoIB outputs, taking
  into consideration the nodes in which you want to run L2SVs at.
  
  Keyword parameters:
  
  possible_nodes -- This is a python list that determines the organization of
  the connections of the RoIB with the L2SV machines. It should contain the
  total list of dal.Computer objects that can hypothetically connect to the
  RoIB.
  
  active_nodes -- A list of dal.Computer objects in which we have active L2SV's.
  
  Check gen_detector_strlist() at this module for information on how the bit
  word is organized.
  """
  word = 0x0
  for node in active_nodes: word |= (1 << possible_nodes.index(node))
  return word
