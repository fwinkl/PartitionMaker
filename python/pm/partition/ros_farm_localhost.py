#!/usr/bin/env tdaq_python
"""This is an example of a preloaded ROS farm configuration for a localhost partition
"""
from __future__ import print_function

import pm.multinode
import pm.farm
import pm.utils

# We prefer to randomly split the available ROSs using a particular rob
# list. These lists can be generated on-the-fly, using the eformat bindings
# to scan datafiles, or if you prefer, just import the output of
# rosconf-from-data.py --py. We prefer that later because it is faster.

from robhit import robhit
from pm.multinode import localhost

ros_farm = pm.multinode.ros_farm_random(robhit, [localhost], default_host=localhost)

# If you want to print the farm structures, just execute this script 
if __name__ == '__main__':
  print(pm.multinode.prettyprint(ros_farm))
