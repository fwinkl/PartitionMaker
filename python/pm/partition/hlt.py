#!/usr/bin/env tdaq_python
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 10-May-2006

from __future__ import print_function
dbg = False

import pm.option.common as common
import pm.option.specific as specific

import copy
option = copy.deepcopy(common.option)

# This is a list of specific options that need to be taken from the utils
# you can look them up over there to see their meaning
wanted = ['data-networks',
          'hltpus-per-node',
          'hltpu-numForks',
          'hltpu-numberOfAthenaMTThreads',
          'hltpu-numberOfEventSlots',
          'write-dir',
          'max-events',
          'hlt-implementation',
          'hlt-extra-path',
          'partition-name',
          'hlt-farm',
          'hlt-dqm',
          'ros-farm',
          'proxy',
          'robhit-module',
          'log-root',
          'working-directory',
          'muon-calibration',
          'robinnp',
          ]

for k in wanted: option[k] = copy.deepcopy(specific.option[k])

def gendb(**kwargs):
  """Generates a partition with LVL2 and EF that runs on a single host.
  
  Builds a partition that runs a LVL2 + EF system with L2SVs, L2PUs, ROSs,
  1 pROS, 1 DFM, SFIs, EFDs, PTs and 1 SFO. The partition can be easily altered
  to work both in dummy or preloaded mode, with dummy algorithms.

  Returns a dictionary that contains, as keys, the 'partition' (dal.Partition
  object) and the 'includes' python list of includes necessary to make the
  partition a valid OKS database.  
  """
  import pm.common 
  import pm.utils
  from pm.project import Project
  import pm.multinode
  import re

  # my list of includes
  includes = list(pm.multinode.includes)
  if dbg: 
    print('\npm/partition/hlt.py:\n')
    print('dir(pm.multinode)\n',dir(pm.multinode))
    print('includes:\n',includes)

  # and the farms
  ns = {}
  exec('from %s import hlt_farm' % kwargs['hlt-farm'], ns)
  hlt_farm = ns['hlt_farm']
  includes = pm.utils.merge_includes(includes, kwargs['hlt-farm'], 'includes')
  exec('from %s import ros_farm' % kwargs['ros-farm'], ns)
  ros_farm = ns['ros_farm']
  includes = pm.utils.merge_includes(includes, kwargs['ros-farm'], 'includes')

  datafiles = []
  if isinstance(kwargs['data'], list):
    datafiles = pm.utils.unfold_patterns(kwargs['data'])

    if dbg:
      print('datafiles:')
      for f in datafiles:
        print(f)
  
  # do we need output on a file
  output = []
  if kwargs['write-dir'] and len(kwargs['write-dir']) != 0: 
    output = kwargs['write-dir']
   
  if dbg:
    print('hlt_farm =',hlt_farm)
    print('ros_farm =',ros_farm)

  try:
    hlt_impl = kwargs['hlt-implementation']['implementation']
  except KeyError:
    print("ERROR, option['hlt-implementation'] =",kwargs['hlt-implementation'])
    sys.exit(1)

  real_hlt = False
  if str.lower(hlt_impl) != 'pudummy':
    #
    # set up the HLT environment as far as needed by PM
    #
    hlt_env = pm.utils.hltEnv()
    hlt_env.set_hlt_env(kwargs['hlt-implementation'])
    #
    # add some HLT includes
    #
    includes.append('daq/sw/HLT_SW_Repository.data.xml')
    includes.append('daq/segments/HLT-Environment.data.xml')

    real_hlt = hlt_env

    if dbg:
      print('includes:')
      for i in includes: 
        print(i)
      print("kwargs['proxy'] =",kwargs['proxy'])

  part = pm.multinode.part_hlt(kwargs['partition-name'], 
                               hlt_farm, 
                               ros_farm, 
                               setup=Project(kwargs['setup-file']),
                               template=Project(kwargs['template-file']),
                               hltpu_instances=kwargs['hltpus-per-node'],
                               hltpu_numForks=kwargs['hltpu-numForks'],
                               hltpu_numberOfAthenaMTThreads=kwargs['hltpu-numberOfAthenaMTThreads'],
                               hltpu_numberOfEventSlots=kwargs['hltpu-numberOfEventSlots'],
                               hlt_patch_area=kwargs['hlt-extra-path'],
                               logroot=kwargs['log-root'],
                               workdir=kwargs['working-directory'],
                               datafiles=datafiles, 
                               output=output,
                               real_hlt=real_hlt,
                               proxy=kwargs['proxy'],
                               robinnp=kwargs['robinnp'])

  # sets the RepositoryRoot at the partition level
  part.RepositoryRoot = kwargs['repository-root']

  # sets the HLTSV configuration to a maximum number of events
  pm.common.set_all(part, 'HLTSVConfiguration', 'eventMax', kwargs['max-events'])

  # sets the datasize of the different fields.
  if not isinstance(kwargs['data'], list):
    pm.common.set_dummy_data(part, int(kwargs['data']))

  opt = kwargs['hlt-implementation']
  part.TriggerConfiguration = pm.common.create_trigger_config(opt) 

  # saves the new partition on a new database
  includes = pm.utils.merge_unique(includes, [kwargs['setup-file'], \
      kwargs['template-file']])
  if len(kwargs['extra-includes']) != 0:
    includes = pm.utils.merge_unique(includes, kwargs['extra-includes'])
  
  # special goodies
  import pm.special
  if kwargs['control-only']: pm.special.empty_control_tree(part)

  if kwargs['muon-calibration']:
    import pm.mucal
    mucal_db = pm.project.Project(kwargs['muon-calibration'])
    pm.mucal.attach_top_level_segment(mucal_db, part)
    includes.append(kwargs['muon-calibration'])

  if kwargs['hlt-dqm']: 
    pm.special.add_dqm_part(part, hlt=kwargs['hlt-dqm'])
    includes.append(kwargs['hlt-dqm'])
  datanetworks = kwargs.get('data-networks', None);
  pm.special.reset_mesg_passing(part, datanetworks) 

  return {'partition': part, 'includes': includes}
