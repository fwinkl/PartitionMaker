"""This is module contains the default ROB hit list for partitions.

This module contains teh default ROB hit list for TDAQ partitions, that will be
used in the event you have nothing better. It is just a dummy list, nothing
really special or done using any detector data as an example.

The format of the hit list is a python list object in which every entry is a
32-bit number (a python integer) that corresponds to the full source identifier
(as in the event format note) for the ROBs. The spreading of these ROBs among
the system ROSs/ROSEs is not done here.

Please note that this module is going to be executed when you import it into
python. You can take advantage of that to create the numbers on the fly, read
them from a file or whatever. Just make sure that, when the module execution is
finished, the variable 'robhit' is a list and contains the numbers you want.
"""

# the variable this module should contain is a python list. For the default
# thing, we ## DON'T ## just create 32 numbers using the range method.
# add 32 real robid's instead, to remove some warnings
#
robhit = []
robhit.append(0x00111705)
robhit.append(0x00111706)
robhit.append(0x00111707)
robhit.append(0x00111708)
robhit.append(0x00111709)
robhit.append(0x00111710)
robhit.append(0x00111711)
robhit.append(0x00111712)
robhit.append(0x00111714)
robhit.append(0x00111715)
robhit.append(0x00111716)
robhit.append(0x00111717)
robhit.append(0x00111718)
robhit.append(0x00111719)
robhit.append(0x00111720)
robhit.append(0x00111721)
robhit.append(0x00111805)
robhit.append(0x00111806)
robhit.append(0x00111807)
robhit.append(0x00111808)
robhit.append(0x00111809)
robhit.append(0x00111810)
robhit.append(0x00111811)
robhit.append(0x00111812)
robhit.append(0x00111814)
robhit.append(0x00111815)
robhit.append(0x00111816)
robhit.append(0x00111817)
robhit.append(0x00111818)
robhit.append(0x00111819)
robhit.append(0x00111820)
