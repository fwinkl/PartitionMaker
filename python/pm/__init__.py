"""PartitionMaker is a tool for generating OKS databases from python.

PartitionMaker allows the user to script OKS databases, providing a gateway
between python and OKS.
"""
#if oks git repository is used, clone it once and share between all PM instances
import os
import subprocess
import atexit
import shutil

if not os.environ.get('PARTITION_MAKER_NO_OKS_REPOSITORY_CLONE'):
  user_repo=subprocess.getoutput("oks_clone_repository").rstrip()
  if user_repo:
    os.environ['TDAQ_DB_USER_REPOSITORY'] = user_repo
    atexit.register(shutil.rmtree, user_repo, True)
