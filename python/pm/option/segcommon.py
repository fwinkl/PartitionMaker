"""A list of common options that are used with partition generators.
"""

# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 21 Aug 2007 12:21:38 PM CEST 

###########################################
# a list of common options to all plugins #
###########################################

option = {}

option['control-only'] = {'short': 'C', 'arg': False,
  'group': 'Partition making',
  'default': None,
  'description': 'If this flag is set, the output will not contain any applications, but just the control tree (useful for controller-only tests)'}

option['extra-includes'] = {'short': 'I', 'arg': True,
  'group': 'Partition making',
  'description': 'Extra OKS includes to your output',
  'default': []}

#if the system runs preloaded, specify a list of files. Please, attention, the
#files should be visible to the HLTSV and ROS machines!
option['data'] = {'short': 'd', 'arg': True, 'default': 100,
  'group': 'Dataflow',
  'description': 'List of data files to preload or the size of the dummy ROBs'}

option['template-file'] = {'short': 'J', 'arg': True,
  'group': 'Partition making',
  'default': 'daq/sw/common-templates.data.xml',
  'description': 'Which standard template file to include in your partition. This file contains the RC and RDB templates to be used at your segment or partition.'}

option['post-processor'] = {'short': 'Z', 'arg': True,
  'group': 'Partition making',
  'default': [],
  'description': 'If you set this option it has to point to python module that contains a function named "modify" that will take the object generated (segment or partition) and post process it using such a function. This function should just modify the required objects and attributes and return the outcome. PartitionMaker will persistify that result. The specification of the module path follows the python standard, separating paths with dots. Any valid python module in PYTHONPATH or sys.path will work. Multiples of this option may be given at the command line in which case the filters are applied sequentially.'}

