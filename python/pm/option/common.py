"""A list of common options that are used with partition generators.
"""

# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 21 Aug 2007 12:21:38 PM CEST 

###########################################
# a list of common options to all plugins #
###########################################
import pm.option.segcommon

option = dict(pm.option.segcommon.option)

#add the possibility to set the RepositoryRoot from outside
option['repository-root'] = {'short': 'R', 'arg': True, 'default': '',
  'group': 'Partition making',
  'description': 'Add a RepositoryRoot to the partition'}

option['setup-file'] = {'short': 'A', 'arg': True,
  'group': 'Partition making',
  'default': 'daq/segments/setup.data.xml',
  'description': 'Which setup file to include in your partition'}
  
