"""A list of specific options that can be used with partition generators.
"""
from __future__ import print_function

# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 21 Aug 2007 12:22:37 PM CEST

import os
import socket

dbg = False

if dbg: print('pm/option/specific.py:')

option = {}

option['ros-farm'] = {'short': 'r', 'arg': True,
  'group': 'Machine allocation',
  'default': 'pm.partition.default_farm',
  'description': 'This parameter defines the name of a python module that contains a dictionary named "ros_farm". This dictionary should contain the python farm description of the ROS farm as described at the documentation of the function "gen_ros_segment" at the submodule pm.multinode. If the module contains another variable called "includes", it will be used as the OKS hardware database and will be included in the final generated database. For preloaded localhost partitions, you can use pm.partition.ros_farm_localhost'}

option['sfo-farm'] = {'short': 'o', 'arg': True,
  'group': 'Machine allocation',
  'default': 'pm.partition.default_farm',
  'description': 'This parameter defines the name of a python module that contains a dictionary named "sfo_farm". This dictionary should contain the python farm description of the ROS farm as described at the documentation of the function "gen_sfo_segment" at the submodule pm.multinode. If the module contains another variable called "includes", it will be used as the OKS hardware database and will be included in the final generated database'}


option['separate-channels'] = {'short': '', 'arg': False, 'default': None,
  'group': 'Dataflow',
  'description': 'Use this option to have all the channel objects put in a separate database file'}

option['remove-segment'] = {'short': '', 'arg': False, 'default': None,
  'group': 'Dataflow',
  'description': 'Use this option to have all the channel objects put in a separate database file'}


option['ros-mode'] = {'short': '', 'arg': True, 'default': 'preloaded-dc',
  'group': 'Dataflow',
  'description': 'The mode of the ROS segments generated.(supported: preloaded-dc, emulated-dc, robin-dc, robin-datadriven-reb, robin-datadriven-edo)'}

option['roldatagen'] = {'short': '', 'arg': False, 'default': None,
  'group': 'Dataflow',
  'description': "In robin-* mode ROS segments, this is an option to enable the internal data generator of the ROBINs"
  }

option['preload-hosts'] = {'short': '', 'arg': True, 'default': '',
  'group': 'Dataflow',
  'description': '(only when generating ROS segments) Use this option to specify a list of hosts (a file with a hostname on each line) that will be used to generate a preloaded farm'}

option['preload-channels'] = {'short': '', 'arg': True, 'default': '',
  'group': 'Dataflow',
  'description': '(only when generating ROS segments) Use this option to specify a list of channels (a file with a channel ID on each line) that will be used to generate a preloaded farm'}

option['preload-subdetectors'] = {'short': '', 'arg': True, 'default': '',
  'group': 'Dataflow',
  'description': '(only when generating ROS segments) When generating a preloaded segment from host and channel lists, use this option to use only the channels of the specified subdetectors. Use the IDs of the subdetectors separated by commas, e.g. --preload-subdetectors=0x66,0x67' }

option['preload-group'] = {'short': '', 'arg': True, 'default': '',
  'group': 'Dataflow',
  'description': '(only when generating ROS segments) When generating a preloaded segment, use this option to treat multiple subdetectors as a single (i.e. assign their channels to the same ROSs). Use the IDs of the subdetectors separated by commas and use brackets for each group, e.g. --preload-group=[0x66,0x67],[0x11,0x12]'}


option['reb-host'] = {'short': '', 'arg': True, 'default': '',
  'group': 'Dataflow',
  'description': 'Use this option to override the REB RCD host specified by a ROS farm'}

option['robinnp'] = {'short': '', 'arg': False, 'default': None,
  'group': 'Dataflow',
  'description': 'Generate segments compatible with RobinNP'}


#Should the SFO write the built data to a file?
option['write-dir'] = {'short': 'f', 'arg': True, 'default': [],
  'group': 'Dataflow',
  'description': 'List of directories where to place SFO output files. If you are using an SFO, the first entry in this list will be also used to place the index files.'}

#if this option is different than 0, it will trigger the HLTSV to only run the
#system for the predetermined number of events that you specify.
option['max-events'] = {'short': 'Q', 'arg': True, 'default': 0,
  'group': 'Dataflow',
  'description': 'Maximum number of events to run the HLTSV for (0 means forever)'}

option['multifile-oks'] = {'short': 'W', 'arg': False,
  'group': 'Partition making',
  'description': 'Generates the OKS partition in multiple files. Normally, one by top-level segment with a separated hardware file',
  'default': None}

option['robhit-module'] = {'short': 'B', 'arg': True,
  'default': 'pm.partition.default_robhit',
  'group': 'Dataflow',
  'description': 'The name of a python module that is going to be imported, containing a python list named "robhit" that will be used for the allocation of ROBs in the system.'}

#the default name of the partition
option['partition-name'] = {'short': 'p', 'arg': True, 'default': 'foobar',
  'group': 'Partition making',
  'description': 'The name of the OKS partition'}

option['log-root'] = {'short': 'l', 'arg': True, 'default': os.path.isdir('/logs') and '/logs/${TDAQ_VERSION}' or '/tmp',
  'group': 'Partition making',
  'description': 'directory for log files'}

option['working-directory'] = {'short': 'w', 'arg': True, 'default': os.path.isdir('/logs') and '/logs/${TDAQ_VERSION}/${TDAQ_PARTITION}' or '/tmp/${TDAQ_PARTITION}',
  'group': 'Partition making',
  'description': 'working directory'}

option['trigger-config'] = {'short': 'T', 'arg': True,
  'group': 'HLT',
  'default': '',
  'description': 'The name of the dal.TriggerConfiguration object to link into the HLT processors. This object must be present among your extra includes (-I option). If this parameter is empty, a default TriggerConfiguration, suitable for the occasion will be generated and added to your output.' }

option['hlt-datanet-mask'] = {'short': 'U', 'arg': True,
                              'group': 'Dataflow',
                              'default': '',
                              'description': 'If set, should be the network mask for the NetworkInterfaces to be used for the communication between DCMs and SFOs.' }

option['data-networks'] = { 'short' : 'N', 'arg': True,
  'group': 'Dataflow',
  'default': [],
  'description': 'If set, should be a list of network masks for the NetworkInterfaces to be used for the communication.' }

option['segments'] = {'short': 'X', 'arg': True,
  'group': 'Partition making',
  'default': [],
  'description': 'The name of the dal.Segment objects that should be retrieved from the extra-includes (-I flag) to be merged into this partition.' }

#if not evo:
option['dcapp-config'] = {'short': 'H', 'arg': True,
                          'group': 'Partition making',
                          'default': '',
                          'description': 'The name of the dal.DCApplicationConfig object to link into the applications that need it. This object must be present among your extra includes (-I option). If this parameter is empty, a default DCApplicationConfig, suitable for the occasion will be generated and added to your output.' }

option['muon-calibration'] = {'short': '', 'arg': True,
                              'group': 'Dataflow',
                              'default': '',
                              'description': 'If set, should be the (relative) path to the top-level muon calibration server segment database, for the HLT Trigger. This database should contain one segment object which describes the top-level muon calibration server object.' }

option['hlt-dqm'] = {'short': '', 'arg': True,
                     'group': 'Data Quality Monitoring',
                     'default': '',
                     'description': 'If set, should be the (relative) path to a DQM segment for the HLT. This segment should contain a single segment that will be linked in through the highest segment at this trigger level.' }

option['hlt-farm'] = {'short': 't', 'arg': True,
                      'group': 'Machine allocation',
                      'default': 'pm.partition.default_farm',
                      'description': 'This parameter defines the name of a python module that contains a dictionary named "hlt_farm". This dictionary should contain the python farm description of the HLT farm as described at the documentation of the function "gen_hlt_segment" at the submodule pm.multinode. If the module contains another variable called "includes", it will be used as the OKS hardware database and will be included in the final generated database'}

option['hltpus-per-node'] = {'short': 'M', 'arg': True,
                             'group': 'Farm size',
                             'default': 1,
                             'description': 'Number of HLTPU\'s per node. '}

option['hltpu-numForks'] = {'short': 'x', 'arg': True,
                             'group': 'HLT',
                             'default': 1,
                             'description': 'Number of children forked by each HLTMPPU. '}

option['hltpu-numberOfAthenaMTThreads'] = {'short': 'T', 'arg': True,
                             'group': 'HLT',
                             'default': 1,
                             'description': 'Number of AthenaMT threads. '}

option['hltpu-numberOfEventSlots'] = {'short': 'E', 'arg': True,
                             'group': 'HLT',
                             'default': 1,
                             'description': 'Number of event slots. '}

option['hlt-implementation'] = {'short': '', 'arg': True, 'group': 'HLT',
                                'description': "if set, must be a dict having at least 1 key 'implementation': one of {'implementation': 'pudummy'},  {'implementation': 'JobOptions', ... },  {'implementation': 'DB', ... } and  {'implementation': 'DBPython',  ... }. The ... are implementation specific additional keywords that can be used to override the defaults"}

option['hlt-implementation']['default'] = {'implementation': 'pudummy'}

# other choices for 'implementation' are (these are never default)
#option['hlt-implementation'] = {'implementation': 'JobOptions',... }
#option['hlt-implementation'] = {'implementation': 'DB',        ... }
#option['hlt-implementation'] = {'implementation': 'DBPython',  ... }


option['hlt-extra-path'] = {'short': '', 'arg': True, 'group': 'HLT',
                            'description': 'a list of strings giving the HLT_EXTRA_PATH'}

option['hlt-extra-path']['default'] = []


option['hlt-wrapper'] = {'short': '', 'arg': True, 'group': 'HLT',
                         'description': 'if True, generate HLTMPPU_wrapper@Script instead of HLTMPPU_main@Binary'}

option['hlt-wrapper']['default'] = False


option['proxy'] = {'short': '', 'arg': True, 'group': 'HLT',
                   'description': 'if False, generate no DBproxies. If set to True or "node", generate Top-, Rack- and Node-level DBProxy. If set to "rack", generate Top- and Rack-level proxies. If set to "top", generate only Top-level DBproxies'}

option['proxy']['default'] = False

if dbg:
  print('option/specific.py: option =\n',option)
