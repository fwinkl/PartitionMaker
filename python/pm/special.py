"""This module contains functions to help you produce variations on partitions.

The "special" module contains a few utilities to manipulate generated segment
and partitions, removing or adding components from them to make up slightly
different systems than their originals.
"""
from __future__ import absolute_import
from builtins import range
dbg = False

import re
import logging

from pm.dal import DFdal, dal 

def empty_control_tree(object):
  """Recursively empties the control tree of a dal object

  This method will recursively empty the control tree so, in the end, the only
  part left in a segment structure are the control applications and templates.
  It is useful for controller related tests.

  Keyword arguments:

  object -- This is the dal object that will be freed of its resources
  applications.
  """
  if hasattr(object, 'Applications'): object.Applications = []
  if hasattr(object, 'TemplateApplications'): 
    object.TemplateApplications = []
  if hasattr(object, 'Resources'): object.Resources = []
  if hasattr(object, 'Segments') and object.Segments is not None:
    for k in range(len(object.Segments)):
      object.Segments[k] = empty_control_tree(object.Segments[k])
  return object

def untemplatize(oksobj, 
    what=re.compile('^(L2PU|EFD_|PT)TemplateApplication$'), counters={}):
  """This method acts recursively removing templated applications from your DB.

  This method will look for template applications that match the pattern
  specified in "what" and un-templatize them.

  Keyword parameters:

  oksobj -- This is the dal (python) object you want to un-templatize. Can be a
  partition object or a segment.

  what -- This should be a regular expression object as returned by
  re.compile() (see that module's help) that will allow me to filter to to
  un-templatize. The default action is to remove all templates from L2PUs, PTs
  and EFDs. This covers:

    1. L2PUTemplateApplication
    2. PTTemplateApplication
    3. EFD_TemplateApplication

  You can also cover the controller by setting this variable to something like
  re.compile('.+TemplateApplication$').

  counters -- These are counters that are used to manipulate the application
  numbering so it is a nice sequence in the end. You don't need to pass it to
  this function since
  """
  from pm.utils import computer_list
  
  if hasattr(oksobj, 'TemplateApplications'): # this is an HLT segment
    if not oksobj.Applications: oksobj.Applications = []
    if not oksobj.Resources: oksobj.Resources = []
    for k in oksobj.TemplateApplications:

      # covers the L2PU cases
      if k.className() == 'L2PUTemplateApplication' and \
          what.match(k.className()):
        hosts = computer_list(oksobj.TemplateHosts)
        for i in range(len(hosts)):
          for j in range(k.Instances):
            if 'l2pu' not in counters: counters['l2pu'] = 1
            newapp = DFdal.L2PUApplication('L2PU-%d' % counters['l2pu']) 
            newapp.RunsOn = hosts[i]
            newapp.copy(k) 
            counters['l2pu'] += 1
            oksobj.Resources.append(newapp)
    
      # covers the PT cases
      elif k.className() == 'PTTemplateApplication' and \
          what.match(k.className()):
        hosts = computer_list(oksobj.TemplateHosts)
        for i in range(len(hosts)):
          for j in range(k.Instances):
            if 'pt' not in counters: counters['pt'] = 1
            newapp = DFdal.PTApplication('PT-%d' % counters['pt']) 
            newapp.RunsOn = hosts[i]
            newapp.copy(k) 
            counters['pt'] += 1
            oksobj.Resources.append(newapp)
    
      # covers the EFD cases
      elif k.className() == 'EFD_TemplateApplication' and \
          what.match(k.className()):
        hosts = computer_list(oksobj.TemplateHosts)
        for i in range(len(hosts)):
          for j in range(k.Instances):
            if 'efd' not in counters: counters['efd'] = 1
            newapp = DFdal.EFD_Application('EFD-%d' % counters['efd']) 
            newapp.RunsOn = hosts[i]
            newapp.copy(k) 
            counters['efd'] += 1
            oksobj.Applications.append(newapp)
    
    oksobj.TemplateHosts = []

    # this object should not have anymore template applications
    oksobj.TemplateApplications = []

  # covers the controller case
  if hasattr(oksobj, 'IsControlledBy') and \
    oksobj.IsControlledBy.className() == 'RunControlTemplateApplication' and \
    what.match('RunControlTemplateApplication'):
    if 'ctrl' not in counters: counters['ctrl'] = 1
    newapp = dal.RunControlApplication('Controller-%d' % counters['ctrl'])
    newapp.RunsOn = oksobj.DefaultHost
    newapp.copy(oksobj.IsControlledBy)
    oksobj.IsControlledBy = newapp
    counters['ctrl'] += 1

  if hasattr(oksobj, 'Segments') and isinstance(oksobj.Segments, list):
    for k in oksobj.Segments: untemplatize(k, what, counters)

  return oksobj


def reset_mesg_passing(oksobj, datanetworks = None, ef_netmask=None ):
  """Reset applications so they work with specific message passing parameters.

  Keyword parameters:

  oksobj -- This is the OKS object you want modified, can be a segment or a
  partition object.

  ef_netmask -- This is an optional network mask that will determine the network
  interface to be used for the EFD, when communicating with both the SFIs and
  the SFOs in the system.

  Returns the input oksobj, modified.
  """

  import re

  # now we handle the EFD special case
  for k in oksobj.get('EFIOConfiguration'):
    k.NetMask = ef_netmask
    
  if datanetworks:
    oksobj.DataFlowParameters.DefaultDataNetworks = datanetworks

  return oksobj

def add_seg(seg, include, segname=None):
  """Adds a new child segment into the segment given.

  This method will search a dal.Segment at the include file passed (optionally
  with a name) and will attach this segment to the segment passed as the first
  parameter.

  Keyword parameters:

  seg -- This is the dal.Segment that will become the parent of the segment in
  the include file specified as second parameter

  include -- This is the include file from where to "fish" the new child
  segment to be included

  segname -- This is the (optional) segment name inside the include file that
  you would like to reach. If nothing is specified, the default behavior is to
  use the first segment returned. This should work well if only have 1 segment
  in your include file and you don't want to worry about naming conventions.
  """
  from .project import Project
  from .utils import safeInsert
  
  incdb = Project(include)
  child = None
  if segname:
    child = incdb.getObject('Segment', segname)
  else:
    child = incdb.getObject('Segment')[0]
  safeInsert(seg, 'Segments', [child])

def add_dqm_part(part, hlt=None):
  """Adds the DQM segments for L2/EF for the partition given as parameter.

  This method will search for the DQM top level segments in the include files
  given and will attach them to both the L2 and EBEF super-segments.

  Keyword arguments:

  part -- This is the dal.Partition object you want to update
  
  l2 -- This is the include file that contains the L2 DQM segment to include in
  the L2-Segment

  ef -- This is the inclue file that contains the EF DQM segment to include in
  the EBEF-Segment
  """
  for s in part.Segments:
    if s.id == 'HLT-Segment' and hlt: add_seg(s, hlt)

        
