"""
Describes how to build ROS segments using PartitionMaker.

This module contains a fully functional description of the logic around
building ROS segments using the pythonic bindings of PartitionMaker. It allows
you to create a segment with as many ROSs as you want.
"""
from __future__ import division
from __future__ import print_function

from builtins import range
from past.utils import old_div
from builtins import object
import pm.utils
import re
import sys

import logging
logger = logging.getLogger('PartitionMaker')


from pm.dal import dal, DFdal, DFTriggerIndal,\
    ROSDescriptordal, NPDescriptordal, RobinNPDescriptordal, RobinNPdal, PreloadedNPdal

class override_params(object):
  """
  This is a decorator that allows the caller to override parameters in the
  returned DFdal.* objects of the functions in this file. For example,
  instead of calling segment(...), you can use the call

  segment(..., override_params={ 'Param1': value, ...})

  which will call() segment and then manipulate the attributes of the returned
  object. Remember that the overriding is applied AFTER the call.

  Typical usage is to allow the farm modules to pass their specific configuration
  params without having to modify PartitionMaker code.
  """

  def __init__(self, f):
    self.f = f

  def __call__(self, *args, **kwargs):
    if 'override_params' in kwargs:
      override_params = kwargs.pop('override_params')
      dal_obj = self.f(*args, **kwargs)
      #__dict__.extend may be faster but overrides OKS checks!
      for p in override_params:
        setattr(dal_obj, p, override_params[p])
      return dal_obj
    else:
      return self.f(*args, **kwargs)

@override_params
def segment(name, controller, ros_segment, default_host=None):
  """Generates a top-level ROS segment with many ROS/ROSE segments inside.

  This method will return a dal.Segment object, configured to contain the many
  ros segments passed as parameter.

  Keyword parameters:
  name -- A name for this segment. Something like 'ROS-Segment' would
  do, but this depends on your layering strategy.

  controller -- This is an OKS RunControlTemplateApplication used in this
  segment as a controller.

  ros_segment -- A single or a list of full ROS segments (each should contain
  1 or many ROSs).

  default_host -- An OKS computer which should serve as the default host for
  this segment. If a value of None is given (the default), this segment will
  inherit the upper segment's default host. Or the partition's default host in
  the last instance. If the partition's default host is not set, the current
  host will be used.

  Checks executed:
    1. Checks if we don't have repeated ROB identifiers in the whole segment.
       (not yet implemented)

  Returns an OKS dal Segment object
  """

  segment = dal.Segment(name)
  segment.IsControlledBy = controller
  #segment.DefaultHost = default_host

  if not ros_segment:
    logging.warning("Your ROS segment appears to be empty: if you are generating from CSV"\
  " you should check that you are using the correct file for your hosts database (otherwise ignore this).")

  if isinstance(ros_segment, list) and len(ros_segment):
    sample = ros_segment[0]
  else: sample = ros_segment

  if isinstance(sample, dal.Segment):
    segment.Segments = ros_segment
  else:
    segment.Resources = ros_segment

  return segment

@override_params
def subsegment(name, controller, ros, default_host=None):
  """Generates a ROS segment with ROS/ROSEs inside

  This method will return a dal.Segment object that contains a number of ROS
  applications.

  Keyword parameters:

  name -- A name for this segment. Something like 'L2-Segment-Bla-bla' would
  do, but this depends on your layering strategy.

  controller -- This is an OKS RunControlTemplateApplication used in this
  segment as a controller.

  ros -- A list of ROS/ROSE applications that will be placed in this segment.

  env -- A Variable or VariableSet or lists of them, so that we get the
  environment right for this segment.

  default_host -- An OKS computer which should serve as the default host for
  this segment. If a value of None is given (the default), this segment will
  inherit the upper segment's default host. Or the partition's default host in
  the last instance. If the partition's default host is not set, the current
  host will be used.

  Returns an OKS dal Segment object
  """
  segment = dal.Segment(name)
  segment.IsControlledBy = controller
  segment.DefaultHost = default_host
  segment.Applications = []
  segment.Resources = []
  for r in ros:
    if isinstance(r, DFdal.ROSE): segment.Applications.append(r)
    else: segment.Resources.append(r)

  return segment

def resource_set_and(name, ros, default_host=None):
  """ Generates a ResourceSetAND with real ROSs contained

  This method will return a dal.ResourceSetAND object that contains a number of
  ROS applications. An AND set means that all individual ROSs need to be
  disabled in order to disable the set.

  Keyword parameters:

  name -- A name for this resource set. Normally a ResourceSet is associated
  with a subdetector, so use something like "ResourceSetAND-sdname-ROS"

  ros -- A list of ROS/ROSE applications that will be placed in this resource
  set.

  Returns an OKS dal.ResourceSetAND object
  """
  rs = dal.ResourceSetAND(name)
  rs.Contains = ros
  return rs


import eformat
def calculate_proper_subdetector(roblist):
  """Returns a sensible subdetector based on the ROB list input.

  This method will return a sensible subdetector identifer (as defined in
  eformat) given the roblist input. This number is chosen like this:
  If all ROBs have the same subdetector identifier, that subdetector identifier
  is returned. Otherwise, subdetector identifier zero is returned.
  """
  example = eformat.helper.SourceIdentifier(roblist[0]).subdetector_id()
  for r in roblist[1:]: #we skip the first entry, naturally
    if eformat.helper.SourceIdentifier(r).subdetector_id() != example:
      return eformat.helper.SubDetector.FULL_SD_EVENT

  #if you got here, return the value of example
  return example

def adjust_ros_usage(nros, nros_allocated, subdet):
  if nros-nros_allocated < 1:
    # Should not happen.
    logging.fatal("Application ERROR:  %d ROSs allocated but only %d ROSs available." % (nros_allocated, nros))
    sys.exit(1)
  max = 0
  subd = None
  for sd in list(subdet.keys()):
    (robs, ross) = subdet[sd]
    if ross == 0:
      logging.fatal("Application ERROR: subdetector %s has %d ROSs and %d ROBs. At least one expected." % (sd, ross, len(robs)))
      sys.exit(1)
    robsPerROS = old_div(len(robs),ross)
    if max < robsPerROS:
       max = robsPerROS
       subd = sd
  # Add a ROS to selected subdetector
  (robs, ross) = subdet[subd]
  ross += 1
  subdet[subd] = (robs, ross)
  return nros_allocated+1

def subdetector_rob_ros_allocation(roblist_in, nros, subdetAsked):
  """This method will allocate ROBs according to subdetector ID and
  then minimise the number of ROBs in each ROS while not mixing
  ROBs from different subdetectors in the same ROS.

  Returns a list of lists, list of ROSs that each hold a list of rob identifiers.

  Keyword parameters:
  roblist     -- A (python) list of ROBs that you want to distribute
  nros        -- The number of ROSs used in this(these) subdetector(s).
  subdetAsked -- A (python) list of subdetector IDs.  Distribute ROBs over available
                 ROSs as even as possible with ROSs only using one subdetector ID.
                 ROBs from other subdetector IDs are ignored.
  """
  #define detectors/groups that are known to have read out links, ROLs, and thus ROSs.

  ros_subdetectorGroups = [ 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8 ]

  # 0x00 = Full event
  ros_subdetectors = [
     0x11, 0x12, 0x13, # Pixel
     0x21, 0x22, 0x23, 0x24, # SCT
     0x31, 0x32, 0x33, 0x34, # TRT
     0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, # LAR
     0x51, 0x52, 0x53, 0x54, # TILE
     0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, # MUON
     0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, # LVL1 Trigger
     0x81, 0x82, 0x83, 0x84] # Forward

  import pm.utils

  # Check that ROB IDs are unique.
  roblist = pm.utils.uniq(roblist_in)
  if len(roblist) != len(roblist_in):
    logging.warning("Revisit your ROB list. There are a total of %d ROB identifiers found whereof %d are unique.\n\
       Continue with the unique ROBs." % (len(roblist_in), len(roblist)))

  # Give an warning if a wanted subdetectors, subdetAsked, is not in ros_subdetectors/groups.
  use_subdet = []
  for sd in subdetAsked:
    try: sdName = eformat.helper.SubDetector.values[sd]
    except: sdName = "UNKNOWN"
    if sd not in ros_subdetectorGroups:
      if sd not in ros_subdetectors:
        try:
          if sd in list(eformat.helper.SubDetector.values.keys()):
            print("warning:  0x%02x, '%s', is a wanted subdetector but is not known to have ROLs. Will still be used if found." % (sd, sdName))
            use_subdet.append(sd)
          else:
            print("warning: Sub detector 0x%02x is not defined as an ATLAS subdetector. REMOVED from subdetector list." % sd)
            subdetAsked.remove(sd)
        except KeyError:
          print("warning: KeyError: Sub detector 0x%02x is not defined as an ATLAS subdetector. REMOVED from subdetector list." % sd)
          subdetAsked.remove(sd)
        except:
          print("warning: Unknown exception %s for sub detector 0x%02x. REMOVED from subdetector list." % (sys.exc_info, sd))
          subdetAsked.remove(sd)
      else:
        logger.debug(" add subdetector=0x%x, '%s'." % (sd, sdName))
        use_subdet.append(sd) # known ROS subdetector
    else: # ros_subdetectorGroups
      logger.debug(" replace the subdetectorGroup=%d by it's subdetectors:" % sd)
      min = sd*16
      max = min + 16
      for sd2 in range(min, max):
        try: sdName = eformat.helper.SubDetector.values[sd2]
        except: sdName = "UNKNOWN"
        if sd2 in ros_subdetectors:
          logger.debug("     subdetector=0x%02x, '%s'." % (sd2, sdName))
          use_subdet.append(sd2)

  # Make a dict of subdetectors to use that has a list of its ROBs and later a ROS count, i.e., {sub_det, [robs], ros_count/[ROSs]}.
  subdet = {}
  for rob in roblist:
    subdet_key = calculate_proper_subdetector([rob])
    if subdet_key in use_subdet:
      if subdet_key not in subdet:
        subdet[subdet_key] = ([], 0)
        try: sdName = eformat.helper.SubDetector.values[sd]
        except: sdName = "UNKNOWN"
        logger.debug(" Found ROB for subdetector=0x%02x, '%s'." % (subdet_key, sdName))
      (robs, ross) = subdet[subdet_key]
      robs.append(rob)
      subdet[subdet_key] = (robs, ross)

  # Check used sundetectors and make a list of used ROBs.
  used_robs = []
  found_subdet = []
  for sd in subdet:
    (robs, ross) = subdet[sd]
    found_subdet.append(sd)
    for rob in robs:
      used_robs.append(rob)
    try: sdName = eformat.helper.SubDetector.values[sd]
    except: sdName = "UNKNOWN"
    logger.debug(" subdetector=0x%02x, '%s', has %d ROBs and %d ROSs" % (sd, sdName, len(robs), ross))

  logger.debug("ROSs=%d, subdetector groups and IDs asked=%d, found subdetector IDs=%d, used_robs=%d, total_robs=%d" % (nros, len(subdetAsked), len(subdet), len(used_robs), len(roblist)))

  use_subdet.sort()
  used_robs.sort()
  # Check if all subdetectors found (SKIP??)
  if len(use_subdet) != len(subdet):
    if len(use_subdet) < len(subdet):
      logging.info("%d subdetector IDs and groups asked for resulted in %d subdetector IDs for which %d ROBs were found." % (len(subdetAsked), (len(use_subdet), len(subdet))))
      not_used_sd = ""
      for sd in use_subdet:
        if sd not in subdet: not_used_sd = sd + " "
      logging.warning("Subdedetector(s) '%s'were wanted but had no ROBs defined." % not_used_sd)

  # Check that there are at least as many ROSs as subdetectors.
  if nros < len(subdet):
    logging.fatal("There are a total of %d subdetectors but only %d ROSs (possible cause: a host may not be defined in your hosts database) => EXIT." % (len(subdet), nros))
    sys.exit(1)

  # How many ROSs should be allocated to each subdetector ?
  min_robs_per_ros = int((old_div(float(len(used_robs)),float(nros))) + 0.5)
  subdet_allocated = 0
  nros_allocated = 0
  robs_allocated = 0
  for sd in subdet:
    (robs, ross) = subdet[sd]
    if len(robs) <= min_robs_per_ros:
      subdet[sd] = (robs, 1)
      logger.debug("subdet %s has %d robs and will get 1 ROS." % (sd, len(robs)))
      nros_allocated += 1
      robs_allocated += len(robs)
      subdet_allocated += 1

  logger.debug("Pass1: robs_allocated=%d, min_robs_per_ros=%d. %d subdetectors got 1 ROS. %d subdetectors and %d ROSs left to allocate." % (robs_allocated, min_robs_per_ros, subdet_allocated, len(subdet)-subdet_allocated, nros-nros_allocated))

  # Do I need to do pass 2 ?
  if nros-nros_allocated < len(subdet)-subdet_allocated:
    logging.fatal("Application ERROR: There are only %d ROSs left to handle %d subdetectors => EXIT." % (nros-nros_allocated, len(subdet)-subdet_allocated))
    sys.exit(1)

  # recalculate no of ROBs per ROS.
  if len(used_robs) != robs_allocated:
    min_robs_per_ros = int((old_div((len(used_robs)-(robs_allocated)),float(nros-nros_allocated))) + 0.5)
    for sd in subdet:
      (robs, ross) = subdet[sd]
      if ross == 0:
        ross = old_div(len(robs),min_robs_per_ros)
        if ross == 0: ross = 1
        subdet[sd] = (robs, ross)
        nros_allocated += ross
        robs_allocated += len(robs)
        subdet_allocated += 1
  logger.debug("Pass2: min_robs_per_ros=%d. %d subdetectors and %d ROSs allocated. %d ROSs left to allocate." % (min_robs_per_ros, subdet_allocated, nros_allocated, nros-nros_allocated))

  if  robs_allocated != len(used_robs):
    logging.fatal("Application ERROR: Number of input ROBs=%d is different to allocated ROBs %d => EXIT." % (len(used_robs), robs_allocated))
    sys.exit(1)

  passes_done = 3
  while nros_allocated != nros:
    nros_allocated = adjust_ros_usage(nros, nros_allocated, subdet)
    logger.debug("Pass%d: %d ROSs allocated of totally %d ROSs." %   (passes_done, nros_allocated, nros))
    passes_done += 1

  # subdet now contain the list of ROBs and the (balanced) ROS count for each subdetector.
  # Split the ROBs butween available subdetector ROSs.
  retval = [[] for k in range(0, nros)]
  ros_count = 0
  for sd in list(subdet.keys()):
    (robs, ross) = subdet[sd]
    rob_count = 0
    robsPerRos = old_div(len(robs),ross)
    while ross > 0:
      if ross == 1: rob_range = list(range(rob_count, len(robs)))
      else: rob_range = list(range(rob_count, rob_count+robsPerRos))
      for rob in rob_range:
        retval[ros_count].append(robs[rob])
      logger.debug("ROS-%d in subdetector %s has %d ROBs starting with ROB 0x%08x." %   (ros_count, sd, robsPerRos, robs[rob_count]))
      rob_count += robsPerRos
      ros_count += 1
      ross -= 1

  if ros_count != nros:
    logging.fatal("Application ERROR: There are %d ROSs used but %d available." % (ros_count, nros))
    print(subdet)
    sys.exit(1)

  # To make it nicer, sort the ROB list
  for k in retval: k.sort()
  return retval

def randomize_rob_ros_allocation(roblist, nros):
  """This method will randomize the allocation of ROBs to ROS.

  Returns a list that contains lists of rob identifiers. The allocation method
  uses a round-robin process, so the number of ROBs each ROS is charge is
  approximately the same.

  Keyword parameters:
  roblist -- A (python) list of ROBs that you want to distribute
  nros -- The number of ROSs in the system you want to build.
  """
  import random
  import pm.utils

  # we make sure the user did not screw-up...
  roblist = pm.utils.uniq(roblist)

  random.seed(0) # make it random, but no so random!

  retval = [[] for k in range(0, nros)]
  ros = 0

  for k in range(0, len(roblist)):
    k = random.randint(0, len(roblist)-1)
    retval[ros].append(roblist[k])
    del roblist[k]
    ros += 1
    if (ros % nros) == 0: ros = 0

  # just to make it nicer, we organize the list
  for k in retval: k.sort()

  return retval

def create_rose_config(name, dummy_size=-1):
  retval = DFdal.ROSEConfiguration(name)
  if dummy_size > -1:
    retval.runMode = "Internal"
    retval.robSizeDistribution = 'C|%d' % dummy_size
  else:
    retval.runMode = "Preloaded"
  retval.subDetectorId = 0
  return retval

def create_rose(name, robs, rose_config, dcapp_config, computer=None,
  interfaces=None):
  """Creates a single emulated ROS (ROSE) application.

  This method should be used to create a single ROSE application, that can be
  used in ROS segments.

  Keyword arguments (may be named):

  name -- The OKS database name this object will take

  robs -- A list of ROB (source identifiers, as in eformat) that this ROS
  should serve

  rose_config -- The ROSEConfiguration object to be used by this ROSE

  dcapp_config -- The DFApplicationConfig to be used for this data collection
  application.

  dummy_size -- If you are running with dummy data, set in this parameter the
  number of 32-bit words to generate as the payload of each ROB sent into the
  system. A setting of "-1" will setup the ROSE to read-in data from datafiles
  (runMode will be set to "Preloaded").

  computer -- The dal.Computer object where this application is supposed to
  run. If the computer is not set, the application wil run on the default host
  for its segment.

  interfaces -- If you decide to specify this parameter, it should contain a
  list of DFdal.InterfaceMap objects that specify how the ROS object should
  communicate with the the DFMs, SFIs and L2Ps, i.e., its standard peers.

  Returns a configured ROSE dal object.
  """
  from pm.common import tdaqRepository, setup_dfapp

  rose = DFdal.ROSE(name)
  setup_dfapp(rose)
  if computer is not None: rose.RunsOn = computer
  rose.Program = tdaqRepository.getObject('Binary', 'rose_main')

  rose.ActionTimeout = 300
  rose.DFApplicationConfig = dcapp_config
  rose.robs = robs
  #rose.Interfaces = interfaces
  rose.ROSEConfiguration = rose_config
  return rose

def create_ros_memory_pool(name, pages=32, size=4096, alloc_type="MALLOC"):
  """Creates a new MemoryPool for real ROSs.

  This method returns a new MemoryPool object that can be used with real ROS
  InputChannel objects. It determines the maximum amount of data you can
  transfer in each ROB fragment.

  Keyword parameters:

  name -- The OKS name for this object

  pages -- The number of pages of memory available for each fragment. The
  number of pages available should be at least equal to the number of request
  handlers associated with the ROS you are configuring.

  size -- The size of each page in bytes. This number should be tunned so, in
  most cases, each ROB can be transported in a single page. You must know the
  maximum amount of data each ROB contains to choose this number properly. The
  default (4kb) should work in most conditions.

  alloc_type -- The type of memory allocation to use. "MALLOC", which is the
  default, should be good for most cases.
  """
  return DFdal.MemoryPool(name, Type=alloc_type, NumberOfPages=pages,
      PageSize=size)

def create_ros_input_channel(name, id, mem_pool):
  """Creates an new InputChannel for the real ROS.

  InputChannels are used with the real ROS to defined the ROB identifiers to
  are bound to a certain machine. The "Id" entry of these objects normally
  represent the 16-bit identifier as returned by the
  eformat.helper.SourceIdentifier.module_id() method and are clipped beyond
  these 16-bits. The sole exception being when you use
  PreloadedReadoutModule's, in which case the full 32-bit identifiers are
  taken.

  Keyword parameters:
  name -- The name to give to this object inside OKS
  id -- This should be either a 16 or 32-bit module identifier (integer)
  mem_pool -- This is the memory configuration to be used for this channel.
  This object should be as large as the largest fragment of this type that it
  can be shipped by this ROS.
  """
  return DFdal.InputChannel(name, Id=id, MemoryConfiguration=mem_pool)


def create_ros_robin_data_channel(name, id, physaddress, mem_pool, config):
  """
  Creates an new RobinDataChannel for the real ROS. It is necessary to provide
  a RosChannelConfiguration object to create this type of input channel

  Keyword parameters:
  name -- The name to give to this object inside OKS
  id -- This should be either a 16 or 32-bit module identifier (integer)
  physaddress -- The physical address of the channel in the ROBIN module
  mem_pool -- This is the memory configuration to be used for this channel.
  config -- The RobinChannelConfiguration associated with this data channel.
  """
  return DFdal.RobinDataChannel(name, Id=id, PhysAddress=physaddress,
      MemoryConfiguration=mem_pool, Configuration=config)

def create_ros_robin_channel_config(name, roldatagen=False):
  """
  Creates an new RobinChannelConfiguration for the real ROS.

  Keyword parameters:
  name -- The name to give to this object inside OKS
  roldatagen -- Whether to enable the internal ROBIN data generator
  """
  return DFdal.RobinChannelConfiguration(name, RolDataGen=roldatagen)

def create_ros_robin_channel_config_mux(name, selector_name, configs):
  """
  Creates an new RobinChannelConfigurationMUX for the real ROS.

  Arguments:
  name -- The name to give to this object inside OKS
  selector_name -- The value to give to the 'Selector' dal.Variable 'Name'
  attribute of the generated selector
  configs -- A list of RobinChannelConfiguration objects (selector will point
  to first in the list)
  """
  selector = dal.Variable(name+'-Selector')
  # the environment variable name
  selector.Name = selector_name
  if configs:
    selector.Value = configs[0].id
  return DFdal.RobinChannelConfigurationMUX(name, Configurations=configs, Selector=selector)

def create_hw_input_channel(name, id, physaddress, mem_pool):
  """Creates a HW_InputChannel.

  Keyword parameters:
  name -- The OKS database name for the generated object
  id -- The id to be associated with this channel
  physaddress -- The physical address of this channel
  mem_pool -- The memory configuration to be used with this channel
  """
  return DFdal.HW_InputChannel(name, Id=id, PhysAddress=physaddress,
      MemoryConfiguration=mem_pool)

def create_eth_seq_readout_module(name, channels, physaddress, input_fragment_type):
  """
  Creates an EthSequentialReadoutModule that will contain the specified input
  channels.

  name -- The OKS database name for the generated object
  physaddress -- The physical address of this module
  input_fragment_type -- The type of fragment that this module will receive
  """

  return DFdal.EthSequentialReadoutModule(name, Contains=channels,
      InputFragmentType=input_fragment_type, PhysAddress=physaddress)

def create_ros_preloaded_module(name, input_channel, physaddress=0, robinnp=False):
  """Creates a PreloadedReadoutModule for the ROS.

  This method will return a ReadoutModule implementation for the real ROS,
  configured with the input channels defined in the input.

  Keyword parameters:

  name -- The OKS database name for the generated object
  input_channel -- The list of input channels to associated with this readout
  module

  Returns a PreloadedReadoutModule or PreloadedModuleNP  of the type you specified.
  """
  if robinnp:
    return PreloadedNPdal.PreloadedModuleNP(name, PhysAddress=physaddress, Contains=input_channel)
  else:
    return DFdal.PreloadedReadoutModule(name, PhysAddress=physaddress, Contains=input_channel)


def create_ros_emulated_config(name):
  """Returns the default EmulatedReadoutConfiguration object.

  This method returns the default EmulatedReadoutConfiguration with the name
  you assign to it.
  """
  return DFdal.EmulatedReadoutConfiguration(name)

def create_ros_emulated_module(name, input_channel, config, 
                               physaddress=0, robinnp=False):
  """Creates an EmulatedReadoutModule for the ROS.

  This method will return a ReadoutModule implementation for the real ROS,
  configured with the input channels defined in the input.

  Keyword parameters:

  name -- The OKS database name for the generated object

  input_channel -- The list of input channels to associated with this readout
  module

  config -- The EmulatedReadoutConfiguration object to be used for this module.

  Checks performed:
    1. Scans all input channels to verify none have a repeated (16-bit) module
       identifier. It will warn the user if this is the case.

  Returns a EmulatedReadoutModule of the type you specified.
  """
  if robinnp:
    retval = ROSDescriptordal.EmulatedDescriptorModule(name, PhysAddress=physaddress, Configuration=config)
  else:
    retval = DFdal.EmulatedReadoutModule(name, PhysAddress=physaddress, Configuration=config)

  # Check 1.
  import eformat
  existing = []
  input_channel = input_channel
  for ic in input_channel:
    mid = eformat.helper.SourceIdentifier(ic.Id).module_id()
    if mid not in existing: existing.append(mid)
    else:
      logging.warning("You have a repeating (0x%04x) module identifier attached to the same EmulatedReadoutModule (%s). This will not work as you expect." % (mid, name))

  # wrap-up and return
  retval.Contains = input_channel
  return retval

def create_ros_robin_module(name, input_channel, config, physaddress=0, 
                            robinnp=False, datadriven=False):
  """Creates a RobinReadoutModule for the ROS.

  This method will return a RobinReadoutModule implementation for the real ROS,
  configured with the input channels defined in the input.

  Keyword parameters:

  name -- The OKS database name for the generated object
  input_channel -- The list of input channels to associated with this readout
  module
  config -- The RobinConfiguration object for this ROBIN

  Returns a RobinReadoutModule.
  """
  if robinnp:
    if datadriven:
      return RobinNPdal.\
          RobinNPReadoutModule(name, PhysAddress=physaddress,
                               Contains=input_channel, 
                               Configuration=config)
    else:
      return RobinNPDescriptordal.\
          RobinNPDescriptorReadoutModule(name, PhysAddress=physaddress,
                                         Contains=input_channel, 
                                         Configuration=config)
  else:
    return DFdal.RobinReadoutModule(name, PhysAddress=physaddress,
                                    Contains=input_channel, 
                                    Configuration=config)

@override_params
def create_detector(name, id=0):
  """Creates a Detector object.
  name -- The OKS database name for the generated object
  id -- The logical id of the detector
  """
  return dal.Detector(name, State=False, LogicalId=id)

@override_params
def create_ros_trigger_in(name, delete_grouping=100, robinnp = False):
  """Creates a DcTriggerIn configuration for a ROS.

  Keyword parameters:
  name -- The OKS name this object will have
  delete_grouping -- How many clears to receive before actually deleting
  something inside.
  """
  #return DFdal.DcTriggerIn(name, DeleteGrouping=delete_grouping,
  #    ClearLossThreshold=0)
  if robinnp:
    return NPDescriptordal.NPTriggerIn(name, 
                                        DeleteGrouping=delete_grouping,
                                        nDataServerThreads=2)
  else:
    return DFTriggerIndal.DFTriggerIn(name, 
                                      DeleteGrouping=delete_grouping,
                                      nDataServerThreads=2)

@override_params
def create_ros_data_out(name):
  """Creates a DcDataOut object for real ROS configurations.
  """
  #return DFdal.DcDataOut(name)
  return None # not used anymore

@override_params
def create_tcp_data_out_config(name):
  """
  Creates a TCPDataOutConfig object that can be used by TCPDataOut objects
  for real ROS configurations.
  """
  return DFdal.TCPDataOutConfig(name)

@override_params
def create_tcp_data_out(name, port, config):
  """
  Creates a TCPDataOut object for real ROS configurations.
  """
  tdo = DFdal.TCPDataOut(name)
  tdo.DestinationPort = port
  tdo.Configuration = config
  return tdo

@override_params
def create_ros_config(name, request_handlers=8):
  """Creates a real ROSConfiguration object.

  This method returns our default choice for the ROSConfiguration object. Pay
  attention on the number of request handlers. They should be smaller or equal
  the number of pages you have in the MemoryPool object associated with the
  ROS.

  Keyword parameters:
  name -- The OKS database name this object will get
  request_handlers -- The number of request handlers that this ROS will have.
  This number determines how many requests the IOManager will be able to work
  on in parallel. This number should be, most of the time, left with its
  default. The less request handlers, the more memory you have available for
  preloading data.
  """
  return DFdal.ReadoutConfiguration(name,
      NumberOfRequestHandlers=request_handlers)


@override_params
def create_run_control_application(name, computer=None, program=None):
  """
  Creates a RunControlApplication object for the real ROS.

  name -- The OKS database name this object will take
  computer -- The host that this application will run on
  program -- The binary program
  """
  return dal.RunControlApplication(name, RunsOn=computer, Program=program)

@override_params
def create_ros(name, config, detector, trigger_in, data_out,
    mem_pool, readout_module, debug_output, id=0, computer=None, interfaces=None,):
  """
  Creates a single ROS Application.

  This method should be used to create a single ROS application, that can be
  used in ROS segments.

  Keyword arguments (may be named):

  name -- The OKS database name this object will take

  config -- The DFdal.ReadoutConfiguration object this ROS will use.

  detector -- The dal.Detector associated with this ROS

  trigger_in -- The DFTriggerIndal.DFTriggerIn object this ROS will use. Can be a single
  element or a list.

  data_out -- The Output object(s)

  mem_pool -- The memory pool this ROS should use

  debug_output -- The output for the debug stream

  readout_module -- The readout module implementation this ROS should be using.
  This is a list of objects.

  computer -- The dal.Computer object where this application is supposed to
  run. If not set (None value), the ROS will run in the default host for its
  segment.

  interfaces -- If you decide to specify this parameter, it should contain a
  list of DFdal.InterfaceMap objects that specify how the ROS object should
  communicate with the the DFMs, SFIs and L2Ps, i.e., its standard peers.

  Checks implemented:

    1. Checks if the subdetector associated with this ROS has a meaningful
       LogicalId, given the list of readout modules associated(only if 32-bit
       channel ids are used)

    2. Checks if the number of pages in the memory pool is greater or equal to
       the number of request handlers configured.

  Returns a configured ROS DFdal object.
  """
  from pm.common import tdaqRepository

  # Check 1.
  if detector.LogicalId != 0:
    roblist = []
    for rm in readout_module:
      for ic in rm.Contains: roblist.append(ic.Id)
    # only check 32-bit ids
    if roblist[0] & 0xFF0000 and \
        detector.LogicalId != calculate_proper_subdetector(roblist):
        logging.warning("The detector LogicalId (0x%02x) to be associated with object %s is not optimal. A better value would be 0x%02x." % (detector.LogicalId, name, calculate_proper_subdetector(roblist)))

  # Check 2.
  if mem_pool.NumberOfPages < config.NumberOfRequestHandlers:
    logging.warning("For %s, the number of pages in the memory pool (%d) is smaller than the number of request handlers (%d). This may NOT work." % \
      (name, mem_pool.NumberOfPages, config.NumberOfRequestHandlers))

  ros = DFdal.ROS(name, Contains=readout_module, Trigger=[trigger_in],
      Output=data_out, DebugOutput=debug_output, Configuration=config,
      Detector=detector, MemoryConfiguration=mem_pool, Id=id,
      InterfaceName='rc/commander')

  if computer is not None: ros.RunsOn = computer
  ros.Program = tdaqRepository.getObject('Binary', 'ReadoutApplication')

  # ROS specific settings
  #ros.ShortTimeout = 5
  ros.InitTimeout = 60
  ros.ActionTimeout = 60
  ros.IfExitsUnexpectedly = "Restart"
  for rm in readout_module:
    if isinstance(rm, DFdal.PreloadedReadoutModule):
      ros.ActionTimeout = 600
      break

  #ros.Interfaces = interfaces

  return ros

@override_params
def create_reb_ros(name, config, detector, trigger_in, data_out,
    mem_pool, readout_module, computer=None, interfaces=None):
  """Creates a single RCD Application.

  This method should be used to create a single RCD(a type of ROS used in REB
  segments) application, that can be used in REB segments.

  Keyword arguments (may be named):

  name -- The OKS database name this object will take

  config -- The DFdal.ReadoutConfiguration object this ROS will use.

  detector -- The dal.Detector associated with this ROS

  trigger_in -- The DFdal.DcTriggerIn object this ROS will use. Can be a single
  element or a list.

  data_out -- The DFdal.DcDataOut object this ROS will use

  mem_pool -- The memory pool this ROS should use

  readout_module -- The readout module implementation this ROS should be using.
  This is a list of objects.

  computer -- The dal.Computer object where this application is supposed to
  run. If not set (None value), the ROS will run in the default host for its
  segment.

  interfaces -- If you decide to specify this parameter, it should contain a
  list of DFdal.InterfaceMap objects that specify how the ROS object should
  communicate with the the DFMs, SFIs and L2Ps, i.e., its standard peers.

  Checks implemented:

    1. Checks if the number of pages in the memory pool is greater or equal to
       the number of request handlers configured.

  Returns a configured ROS DFdal object.
  """
  from pm.common import tdaqRepository

  # Check 1.
  if mem_pool.NumberOfPages < config.NumberOfRequestHandlers:
    logging.warning("For %s, the number of pages in the memory pool (%d) is smaller than the number of request handlers (%d). This may NOT work." % \
      (name, mem_pool.NumberOfPages, config.NumberOfRequestHandlers))

  reb_ros = DFdal.RCD(name, Contains=readout_module, Trigger=[trigger_in],
    Output=data_out, Configuration=config, Detector=detector,
    MemoryConfiguration=mem_pool)
  if computer is not None: reb_ros.RunsOn = computer
  reb_ros.Program = tdaqRepository.getObject('Binary', 'ReadoutApplication')

  return reb_ros
