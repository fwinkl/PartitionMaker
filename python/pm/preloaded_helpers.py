from __future__ import division
from builtins import hex
from builtins import range
from past.utils import old_div
__author__ = "Giorgos Boutsioukis <georgios.boutsioukis@cern.ch>"

import pm
import pm.ros
from eformat import helper

import logging
logger = logging.getLogger('PartitionMaker')


def name_from_hostname(hostname):
  "This is primarily for hostnames of the form: pc-fwd-ros-bcm-00"
  hostname = hostname.split('.')[0]
  hostname = hostname.upper()
  bits = hostname.split('-')
  try:
    return bits[1]+'-'+bits[3]+'-'+bits[4]
  except IndexError:
    #well, I tried
    return hostname

def generate_preloaded_farm(hosts, roblist_path, subdetector_filter=[], groups=[], host_order=None):
  """
  This is a helper function that generates preloaded ROS segments by allocating
  the specified channels to the specified hosts. Each ROS host will only be
  assigned channels that belong to the same subdetector (determined by the
  channel's ID). The assignment algorithm tries to balance the channel/ROS
  ratio.

  Arguments

  hosts -- A dictionary of hosts with their hostnames as keys and dal.Computer
  objects as values. See the pm.farm for functions that generate, load and
  filter such dictionaries.

  roblist_path -- The (absolute) path to the file containing the list of
  channel IDs. Lines beginning with ! or # are ignored.

  subdetector_filter -- If not empty, only the channels of the given
  subdetectors will be assigned. Can be a list of subdetector IDs as integers
  or a list of eformat.helper.SubDetector objects (but not mixed).

  groups -- This is a list of lists, i.e. [[0x66,0x67], [..], ...]. Each
  sublist cause the listed subdetectors to be treated as a single subdetector
  and their channels can be assigned to the same ROSs. Can be a list of
  subdetector IDs as integers or a list of eformat.helper.SubDetector objects
  (but not mixed).

  Returns

  A ros_farm, as expected by pm.multinode.gen_ros_segment
  """

  roslist = []
  roblist = {} #per subdetector
  numrobs = 0
  numros = 0

  # convert to Subdetector objects (sanity check)
  if len(subdetector_filter) and isinstance(subdetector_filter[0], int):
    subdetector_filter = [helper.SubDetector.values[sid] for sid in subdetector_filter]
  for group in groups:
    if len(group) and isinstance(group[0], int):
      group = [helper.SubDetector.values[sid] for sid in group]

  merged_sds = []
  # combine grouped subdets in one (the first)
  for group in groups:
    for sd in group[1:]: merged_sds.append(sd)

  if not host_order is None:
    roslist = host_order
  else:
    roslist = list(hosts.keys())

  with open(roblist_path, 'rt') as roblistfile:
    for line in roblistfile:
      # skip commented lines
      if line.startswith('#') or line.startswith('!'): 
        continue
      # derive subdet ids & convert to SubDetector objects
      robid = int(line.strip(), 16)
      rob_sd = helper.SubDetector.values[helper.SourceIdentifier(robid).subdetector_id()]
      # filter out by subdetector
      if len(subdetector_filter) \
          and helper.SubDetector.values[rob_sd] \
          not in subdetector_filter:
        continue
      if rob_sd in merged_sds:
          merged_group_first = [g[0] for g in groups if rob_sd in g][0]
          rob_sd = merged_group_first
      if rob_sd not in roblist:
          roblist[rob_sd] = []
      roblist[rob_sd].append(robid)
      numrobs += 1
  assert len(roblist)==len(set(roblist))

  # subdetectors derived from unique ids in channel list
  subdets = list(roblist.keys())
  assert len(roslist) >= len(subdets), "The channel list contains more subdetectors (%d) than the number of ROS hosts (%d). Consider grouping some subdetectors or adding more hosts" % (len(subdets), len(roslist))

  # ros to subdetector assignment

  # assign 1 ros to each subdetector
  sd_ros_count = dict.fromkeys(subdets, 1)
  unallocated_ros = len(roslist) - len(subdets)

  while unallocated_ros>0:
    # on each round, allocate a ROS to the subdetector with the highest ROB/ROS ratio
    lucky_sd = max(list(roblist.keys()), key=lambda sd: old_div(float(len(roblist[sd])),sd_ros_count[sd]))
    # group: only assign 
    sd_ros_count[lucky_sd] += 1
    unallocated_ros -= 1

  #for sd in sd_ros_count:
  #  print hex(sd.real), " : ", sd_ros_count[sd], float(len(roblist[sd]))/sd_ros_count[sd]
  #print sum([sd_ros_count[sd] for sd in sd_ros_count])

  ros_farm = dict([ (sd,{'ros':[]}) for sd in roblist ])
  #special value for ROSs with mixed channels
  ros_farm[helper.SubDetector.FULL_SD_EVENT] = {'ros':[]}

  #slip these through for naming
  ros_farm['_channelfile'] = roblist_path.split('/')[-1].split('.')[0]

  # assign the channels to each ros and create the farm
  last_ros_index = 0
  assigned_robs = 0
  for sd in roblist:
    sd_channel_list = roblist[sd]
    last_channel_index = 0
    for ros_index in range(last_ros_index, last_ros_index+sd_ros_count[sd]):
      ros = hosts[roslist[ros_index]]
      ros_in_sd_index = ros_index - last_ros_index

      numchannels = old_div(len(sd_channel_list),sd_ros_count[sd])
      # if the channels can't be divided exactly by the ros count, assign an extra channel
      # to the first (numchannels % numROSs) ROSs
      if ros_in_sd_index < len(sd_channel_list)%sd_ros_count[sd]:
        numchannels +=  1

      # add all channels to a single module with index 0
      ros_channels = sd_channel_list[last_channel_index:last_channel_index+numchannels]
      # check if the channels have uniform detector ID
      # otherwise set the ID to zero
      calculated_sd = pm.ros.calculate_proper_subdetector(ros_channels)
      selected_sd = sd
      if calculated_sd != sd:
        selected_sd = helper.SubDetector.FULL_SD_EVENT
      
      ros_farm[selected_sd]['ros'].append(\
        (ros, {'modules':[(0,ros_channels)], 'index':ros_index, 'name':name_from_hostname(ros.id)}) )
      last_channel_index += numchannels

    assert last_channel_index == len(sd_channel_list), "Not all channels were assigned! (failed in subdetector) %d != %d " % (last_channel_index, len(sd_channel_list))
    assigned_robs += len(sd_channel_list)
    last_ros_index += sd_ros_count[sd]

  # Review the farm. Remove Subdetectors with no ROSs:
  for k in list(ros_farm.keys()):
    try:
      ros_list = ros_farm[k]['ros']
      if not ros_list:
        ros_farm.pop(k)
    except KeyError:
      pass
    except TypeError:
      pass

  assert numrobs == assigned_robs, "Not all channels were assigned! %d != %d " % (numrobs, assigned_robs)
  return ros_farm


def feed_with_preloaded_params(ros_farm):
  import os

  # setup ros_farm with default params
  includes = [ \
      'daq/segments/ROS/ros-common-config.data.xml',
      'daq/segments/ROS/ros-specific-config-TDQ.data.xml',
      ]
  ros_farm['ros_common_config'] = ['daq/segments/ROS/ros-common-config.data.xml']
  obj_cache = pm.common.load(includes)

  # also try to load the onl file (the controller is often defined in it)
  try:
    obj_cache.update(pm.common.load(['daq/hw/hosts-onl.data.xml']))
    includes.append('daq/hw/hosts-onl.data.xml')
  except RuntimeError:
    # give a hint if it was needed
    #if '-onl-' in os.getenv('DEFAULT_CONTROLLER_HOST'):
    controller = os.getenv('DEFAULT_CONTROLLER_HOST')
    if controller and '-onl-' in controller:
      logging.warning("Could not load daq/hw/hosts-onl.data.xml; generation may fail")

  ros_farm['includes'] = includes

  if '_channelfile' in ros_farm: 
    ros_farm['name'] = ros_farm['_channelfile']
  else:
    ros_farm['name'] = 'PRELOADED'

  roscount = 0
  for sd in [key for key in ros_farm if ( isinstance(key, helper.SubDetector) or isinstance(key,int) )]:
    ros_farm[sd]['name'] = 'PRELOADED_' + hex(sd.real).replace('0x','',1)
    roscount += len(ros_farm[sd]['ros'])

  if roscount:
    ros_farm['name'] += '-' + str(roscount)

  if os.getenv('DEFAULT_CONTROLLER_HOST'):
    ros_farm['default_host'] = obj_cache[os.getenv('DEFAULT_CONTROLLER_HOST')]
  else: ros_farm['default_host'] = None

  ros_farm['mem_pool'] = obj_cache['ROS-MemoryPool-Data']
  ros_farm['ros_config'] = obj_cache['ROS-Config-TDQ']

  ros_farm['segment_override_params'] = {
      'ProcessEnvironment': [obj_cache['ROSEnvironment']]
      }
  ros_farm['ros_override_params'] = { \
      'ActionTimeout':10,
      'ProbeInterval':5,
      'FullStatisticsInterval':60, 
      'Parameters':'-a 3',
      'RestartParameters':'-a 3',
      'InitTimeout':60,
      'RestartableDuringRun':1,
      }

  # ros_farm is mutable, return nothing
