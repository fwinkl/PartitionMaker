"""
Describes how to build HLT segments using PartitionMaker.

This module contains a fully functional description of the logic around
building HLT segments using the pythonic bindings of PartitionMaker. 
"""
from __future__ import print_function

from builtins import range
import pm.utils
from pm.multinode import localhost
import re
import logging

dbg = False

from pm.dal import SFOngdal, HLTMPPUdal, PUDUMMYdal, HLTSVdal, DCMdal, MIGdal, MONSVC_CONFIGdal, DFdal, dal 

def create_hltpu(name, instances, hlt_patch_area='',real_hlt=False): 
  """Returns a HLTPUApplication, ready to be used.

  This method will return a HLTMPPUApplication, fully configured.

  Keyword parameters:
  
  name -- The name (or id) of the object in the OKS database

  instances -- This is the number of instances this object will have. The magic
  value "0" will cause this method to produce a normal application instead of a
  templated version.
  
  hlt_patch_area -- the value for the HLT_PATCH_AREA env variable

  real_hlt -- True if a real HLTMPPU is used, False if PuDummy

  """

  from pm.common import tdaqRepository
  
  if instances < 0:
    hltpu = HLTMPPUdal.HLTMPPUApplication(name)
  else:
    hltpu = HLTMPPUdal.HLTMPPUApplication(name, Instances=instances)

  #hltpu_wrapper = tdaqRepository.getObject('Script', 'HLTMPPU_wrapper')
  hltpu_main = tdaqRepository.getObject('Binary', 'HLTMPPU_main')
  hltpu.Program = hltpu_main
  hltpu.IfExitsUnexpectedly = 'Handle'
  hltpu.IfFailsToStart = 'Ignore'
  hltpu.Lifetime = 'Configure_Unconfigure'

  if real_hlt:

    if dbg:
      print('real_hlt is True, add hltRepository and hltEnvironment')

    hltRepository = pm.project.Project('daq/sw/HLT_SW_Repository.data.xml')
    hltRepo = hltRepository.getObject('SW_Repository','HLT-Repository')
    pm.utils.safeInsert(hltpu,'Uses',[hltRepo])

    hltEnvironment = pm.project.Project('daq/segments/HLT-Environment.data.xml')
    hltEnv= hltEnvironment.getObject('VariableSet','HLT-Environment')
    pm.utils.safeInsert(hltpu,'ProcessEnvironment',[hltEnv])

    hlt_pa_list = []
    n = 1
    hlt_pa = dal.Variable('HLT-PATCH-AREA-1')
    hlt_pa.Name = 'HLT_EXTRA_SW_PATH'
    hlt_esp = ''
    for a in hlt_patch_area:
      #hlt_pa = dal.Variable('HLT-PATCH-AREA-%s' % n)
      #hlt_pa.Name = 'HLT_EXTRA_SW_PATH'
      #hlt_pa.Value = a
      if n>1: hlt_esp += ':'
      hlt_esp += a
      n += 1
    hlt_pa.Value = hlt_esp

    pm.utils.safeInsert(hltpu,'ProcessEnvironment',[hlt_pa])

    #
    # sw repositories
    #
    tagRepository = pm.project.Project('daq/sw/tags.data.xml')
    tags_o = [ 'x86_64-el9-gcc13-opt']
    tags_d = [ 'x86_64-el9-gcc13-dbg']
    tags_opt = []
    tags_dbg = []

    for t in tags_o:
      try:
        tags_opt.append(tagRepository.getObject('Tag',t))
      except RuntimeError:
        pass

    for t in tags_d:
      try:
        tags_dbg.append(tagRepository.getObject('Tag',t))
      except RuntimeError:
        pass

    tags    = tags_opt + tags_dbg
    tagsdbg = tags_dbg + tags_opt
    #
    # SW_PackageVariables
    #
    jobo_path   = hltRepository.getObject('SW_PackageVariable','JOBOPTSEARCHPATH')
    python_path = hltRepository.getObject('SW_PackageVariable','PYTHONPATH')
    xml_path    = hltRepository.getObject('SW_PackageVariable','XMLPATH')
    data_path   = hltRepository.getObject('SW_PackageVariable','DATAPATH')
    calib_path  = hltRepository.getObject('SW_PackageVariable','CALIBPATH')
    #
    workarea  = real_hlt.get_workarea()
    patcharea = real_hlt.get_patcharea()
    if workarea:
      workarea_root  = ''.join(workarea.partition('InstallArea')[:2])
      workarea_tag   = workarea.split(':')[1].split('InstallArea/')[1].split('/')[0].split('-')[-1]
    if patcharea:
      patcharea_root = ''.join(patcharea.partition('InstallArea')[:2])
      patcharea_tag  = patcharea.split(':')[1].split('InstallArea/')[1].split('/')[0].split('-')[-1]

    if dbg:
      print('tags      =',tags)
      print('tagsdbg   =',tagsdbg)
      print('workarea  =',workarea)
      print('patcharea =',patcharea)
      if workarea:
        print('workarea_root  =',workarea_root)
        print('workarea_tag   =',workarea_tag)
      if patcharea:
        print('patcharea_root =',patcharea_root)
        print('patcharea_tag  =',patcharea_tag)

    if patcharea:
      atlp1hlt_repo = dal.SW_Repository('HLT-AtlasP1HLT-Repository')
      atlp1hlt_repo.Name = 'HLT-AtlasP1HLT-Repository' 
      atlp1hlt_repo.InstallationPath = patcharea_root
      if patcharea_tag == 'opt':
        atlp1hlt_repo.Tags = tags
      else:
        atlp1hlt_repo.Tags = tagsdbg
      atlp1hlt_repo.AddProcessEnvironment = [jobo_path, python_path, xml_path, data_path, calib_path]
      pm.utils.safeInsert(hltpu,'Uses',[atlp1hlt_repo])

    if workarea:
      developer_repo = dal.SW_Repository('HLT-Development-Repository')
      developer_repo.Name = 'HLT-Development-Repository'
      developer_repo.InstallationPath = workarea_root
      if workarea_tag == 'opt':
        developer_repo.Tags = tags
      else:
        developer_repo.Tags = tagsdbg
      developer_repo.AddProcessEnvironment = [jobo_path, python_path, xml_path, data_path, calib_path]
      pm.utils.safeInsert(hltpu,'Uses',[developer_repo])

    #
    # reverse the order of repositories in Uses
    #
    pm.utils.safeReverse(hltpu,'Uses')

  if dbg:
    print('\npm/hlt.py: create_hltpu()')
    print('  hltpu =\n',hltpu)
    print('  real_hlt =',real_hlt)

  return hltpu
  
def create_hltsv(name, plugin='', configrules=None,  computer=None):
  """Returns a HLTSVApplication.

  This method will return a HLTSVApplication that can be put into a HLT top segment.

  Keyword parameters (may be named):
  name -- The name of the L2SV application object in the OKS database.

  configrules -- a ConfigurationRuleBundle for HLTSV

  computer -- This is the dal.Computer object representing the computer where
  this HLTSV will run. If this value is not given (the default is None), then
  this application will run on the default host of the segment.

  """
  from pm.common import tdaqRepository  #, setup_dfapp
  
    
  retval = HLTSVdal.HLTSVApplication(name, 
      Program = tdaqRepository.getObject('Binary', 'hltsv_main'))

  # RoIB plugins

  plugin_filar    = HLTSVdal.RoIBPluginFilar('RoIBPluginFilar-1',Libraries=['libsvl1filar'])
  plugin_internal = HLTSVdal.RoIBPluginInternal('RoIBPluginInternal-1',Libraries=['libsvl1internal'])
  plugin_preload  = HLTSVdal.RoIBPluginPreload('RoIBPluginPreload-1',Libraries=['libsvl1preloaded'])
  plugin_ttc2lan  = HLTSVdal.RoIBPluginTTC2LAN('RoIBPluginTTC2LAN-1',Libraries=['libsvl1ttc2lan'])

  #
  # if no datafiles given, use plugin_internal
  #
  if plugin == 'internal':
    retval.RoIBInput = plugin_internal
  elif plugin == 'preload':
    retval.RoIBInput = plugin_preload
  elif plugin == 'filar':
    retval.RoIBInput = plugin_filar
  elif plugin == 'ttc2lan':
    retval.RoIBInput = plugin_ttc2lan
  else:
    print('ERROR: unknown RoIB plugin name requested!')
    sys.exit(1)

  if 'hltsv' in configrules:
    retval.ConfigurationRules = configrules['hltsv']
  else:
    retval.ConfigurationRules = configrules['default']

  if computer is not None: retval.RunsOn = computer

  retval.InitTimeout = 60
  retval.RestartableDuringRun = True
  retval.InterfaceName = 'rc/commander'
  
  if dbg:
    print('\npm/hlt.py: create_hltsv() retval =\n',retval)
    print('plugin =',plugin)
    print('plugin_internal =',plugin_internal)
    print('plugin_preload  =',plugin_preload)

  return retval

def hltpu_segment(name, 
                  controller,
                  hltpu=None,
                  numForks=1,
                  numberOfAthenaMTThreads=8,
                  numberOfEventSlots=8,
                  configrules=None, 
                  computer=[], 
                  default_host=None, 
                  real_hlt=False):
  """Generates an "atomic" segment that only contains HLTPUs.
  
  This method will generate a segment that only contains HLT Processing Units.
  It cannot run by itself, but can be associated with segments containing an HLTSV
  in order they get triggered. 

  Keyword parameters:

  name -- A name for this segment. Something like 'L2PU-Segment-Bla-bla' would
  do, but this depends on your layering strategy.

  hltpu -- This is either a single HLTPU template or a list of HLTPU Application
  objects that should be run in this segment. In the case the computer list is
  not empty (next parameter), this has to be a template.
  Special case:
    If hltpu is None, a hltpu segment w/o any hltpus is generated.
    This is needed for sub-rack support.
  
  configrules -- a dict of  ConfigurationRuleBundles to be used by HLTMPPU etc

  computer -- This is a list of those of dal.Computer or ComputerSet or Rack objects.
  Each will have a number of HLTMPPU instances running. 
  
  controller -- This is an OKS RunControlTemplateApplication used in this
  segment as a controller.

  default_host -- An OKS computer which should serve as the default host for
  this segment. If a value of None is given (the default), this segment will
  inherit the upper segment's default host. Or the partition's default host in
  the last instance. If the partition's default host is not set, the current
  host will be used. 

  real_hlt -- if True generate everything needed for real HLT

  Returns an OKS dal TemplateSegment object
  """
  if len(computer) != 0 and not isinstance(hltpu, HLTMPPUdal.HLTMPPUApplication):
    raise RuntimeError("You passed a list of computers but not a HLTPU template")
  if len(computer) == 0 and isinstance(hltpu, HLTMPPUdal.HLTMPPUApplication):
    raise RuntimeError("You passed a HLTPU template, but not a list of computers")

  #
  # for localhost, create a dummy rack in list racks.
  #
  if dbg:
    print('computer[0] =',computer[0])
    print('pm.multinode.localhost =',pm.multinode.localhost)

  if len(computer) == 0 or (len(computer)==1 and computer[0] == pm.multinode.localhost):
    dummyrack = dal.Rack('dummyRack')
    dummyrack.Nodes = [ default_host, default_host ] # needs at least 2 hosts
    dummyrack.LFS = [ default_host ]
    racks = [ dummyrack ]

    segment = dal.TemplateSegment(name)
    segment.IsControlledBy = controller
    segment.Racks = racks
  else: 
    segment = dal.TemplateSegment(name)
    segment.IsControlledBy = controller
    segment.Racks = computer
   
  if dbg:
    print('hltpu_segment:\nsegment =',segment)
    print('computer =',computer)
  #
  # prepare hltrc
  #
  hltDataSource = HLTMPPUdal.HLTDFDCMBackend('DataSource-is-DCM')
  hltDataSource.library = 'dfinterfaceDcm'

  hltInfoService = HLTMPPUdal.HLTMonInfoImpl('hltMonSvc')
  hltInfoService.library = 'MonSvcInfoService'

  # Configuration Rule Stuff
  #
  #param   = MONSVC_CONFIGdal.ISPublishingParameters('HLTMPPU_IS')
  #param.ISServer = 'DF_IS'
  #
  #confRule = MONSVC_CONFIGdal.ConfigurationRule('HLTMPPU_MonSvcInfo')
  #confRule.Parameters = param
  #
  #bundle =  MONSVC_CONFIGdal.ConfigurationRuleBundle('HLTMPPU_Rules')
  #bundle.Rules = [ confRule ]

  hltInfoService.ConfigurationRules = configrules['hltpu']

  from pm.common import tdaqRepository
  hltrc = HLTMPPUdal.HLTRCApplication('HLTRC')
  hltrc.DataSource = hltDataSource
  hltrc.InfoService = hltInfoService
  hltrc.Program = tdaqRepository.getObject('Binary', 'HLTRC_main')
  hltrc.InitTimeout = 60
  hltrc.RestartableDuringRun = True
  hltrc.IfExitsUnexpectedly = 'Handle'
  hltrc.IfFailsToStart = 'Ignore'
  hltrc.InterfaceName = 'rc/commander'
  hltrc.numForks = numForks
  hltrc.numberOfAthenaMTThreads = numberOfAthenaMTThreads
  hltrc.numberOfEventSlots = numberOfEventSlots

  #
  # prepare dcm
  #
  dcm_L1Source = DCMdal.DcmHltsvL1Source('dcmL1Source')

  dcm_dataCollector = DCMdal.DcmRosDataCollector('dcmRosDataCollector')
  dcm_processor = DCMdal.DcmHltpuProcessor('dcmHltpuProcessor')
  dcm_output = DCMdal.DcmSfoOutput('dcmSfoOutput')

  dcmapp = DCMdal.DcmApplication('dcm')

  # dcm specific attributes and rels

  dcmapp.InitTimeout = 60
  dcmapp.InterfaceName = 'rc/commander'
  dcmapp.IfExitsUnexpectedly = 'Handle'
  dcmapp.IfFailsToStart = 'Ignore'
  dcmapp.l1Source = dcm_L1Source
  dcmapp.dataCollector = dcm_dataCollector
  dcmapp.processor = dcm_processor
  dcmapp.output = dcm_output
  dcmapp.ConfigurationRules = configrules['dcm']

  # general attrs

  dcmapp.Program = tdaqRepository.getObject('Binary', 'dcm_main')

  if hltpu is not None:
    if len(computer) == 0: segment.Resources = [hltpu]  # XXX check this logic
    else:
      segment.Hosts = []
      segment.Applications = [ hltpu, hltrc, dcmapp]
      
      hltpu.InitializationDependsFrom = [ hltrc ]
  #
  # Infrastructure
  #
  df_histo  = dal.IPCServiceTemplateApplication('DF_Histogramming')
  df_is     = dal.IPCServiceTemplateApplication('DF_IS')
  df_defrdb = dal.IPCServiceTemplateApplication('DefRDB')

  defmig_is = MIGdal.MIGApplication('DefMIG-IS')
  defmig_oh = MIGdal.MIGApplication('DefMIG-OH')

  pm.utils.safeInsert(segment,"Infrastructure", [df_histo, df_is, df_defrdb])
  pm.utils.safeInsert(segment,"Resources", [defmig_is, defmig_oh])

  if real_hlt:
    hltRepository = pm.project.Project('daq/sw/HLT_SW_Repository.data.xml')
    hltparams = hltRepository.getObject('VariableSet','HLT-Parameters')
    pm.utils.safeInsert(segment,'Parameters',[hltparams])

  if dbg:
    print('\npm/hlt.py: hltpu_segment()')
    print('segment:\n',segment)

  return segment

def subsegment(name, controller, hlt_segment, default_host=None):
  """Generates a HLT subsegment with HLTPUs

  This method will return a dal.Segment object that contains a single
  HLTSVApplication and a number of HLTPU sub-segments.

  Keyword parameters:

  name -- A name for this segment. Something like 'HLT-Segment-Bla-bla' would
  do, but this depends on your layering strategy.

  controller -- This is an OKS RunControlTemplateApplication used in this
  segment as a controller.

  hlt_segment -- A single or a list of HLTPU-only segments (rack segments).
  
  default_host -- An OKS computer which should serve as the default host for
  this segment. If a value of None is given (the default), this segment will
  inherit the upper segment's default host. Or the partition's default host in
  the last instance. If the partition's default host is not set, the current
  host will be used.

  Returns an OKS dal Segment object
  """
  segment = dal.Segment(name)
  segment.IsControlledBy = controller
  segment.DefaultHost = default_host 
  segment.Segments = hlt_segment
  defrdb = dal.IPCServiceTemplateApplication("DefRDB")
  pm.utils.safeInsert(segment,"Infrastructure",[defrdb])
  if dbg:
    print('\npm/hlt.py: subsegment()')
    print('segment =\n',segment)

  return segment

def segment(name, controller, hlt_segment, hltsv, sfo=None, default_host=None, real_hlt=False):
  """Generates a top-level HLT segment with many HLTPU-only segments and 1 HLTSV.

  This method will return a dal.Segment object, configured to contain the many
  full HLT subsegments.

  Keyword parameters:
  name -- A name for this segment. Something like 'L2-Segment' would
  do, but this depends on your layering strategy.

  controller -- This is an OKS RunControlTemplateApplication used in this
  segment as a controller.

  hlt_segment -- A list of full HLTPU segments (each should contain 1 or many HLTPU-only subsegments).

  hltsv -- a non-empty list of HLTSV host Computer objects, or single one (which can be non-list)

  sfo -- A single or a list of of SFOng applications that should be used for this segment.
  If this parameter is set to None, then the segment will not have any SFOng's.


  default_host -- An OKS computer which should serve as the default host for
  this segment. If a value of None is given (the default), this segment will
  inherit the upper segment's default host. Or the partition's default host in
  the last instance. If the partition's default host is not set, the current
  host will be used.

  Returns an OKS dal Segment object. Any settings that are specific to your
  application can be done with the returned object.
  """

  import os

  segment = dal.Segment(name)
  segment.IsControlledBy = controller
  segment.Segments = hlt_segment

  if type(hltsv) is list:
    if len(hltsv) == 0:
      raise RuntimeError("you passed an empty list of HLTSV hosts to hlt.segment")
    if dbg:
      print('hltsv:\n',hltsv)
    pm.utils.safeInsert(segment,'Resources',hltsv)
  else:
    if hltsv is None:
      raise RuntimeError("you passed None as an HLTSV hosts to hlt.segment")
    pm.utils.safeInsert(segment,'Resources',[hltsv])

  for s in range(0, len(sfo)):
    if dbg: print('sfo[%d] IS an SFOngApplication, appended to Resources' % s) 
    pm.utils.safeInsert(segment,'Resources',[sfo[s]])
    

  from pm.common import tdaqRepository
  
  topmig_is = MIGdal.MIGApplication('TopMIG-IS')
  topmig_oh = MIGdal.MIGApplication('TopMIG-OH')
  pm.utils.safeInsert(segment,'Resources',[topmig_is, topmig_oh])

  df_is     = dal.IPCServiceTemplateApplication('DF_IS')
  df_histo  = dal.IPCServiceTemplateApplication('DF_Histogramming')
  pm.utils.safeInsert(segment,'Infrastructure',[df_is, df_histo])

  if dbg:
    print('\npm/hlt.py: segment()\n')
    print('hlt_segment =\n',hlt_segment)
    print('segment =\n',segment)
    print('real_hlt =',real_hlt)
    #import traceback
    #traceback.print_stack()

  return segment

def create_sfong_config(name, data_dirs=[]):
  """Creates an SFO configuration object.

  This method returns a real SFO configuration object based on the paramters
  specfied.

  Keyword paramters (may be named):

  name -- This is the identifier in the database to be used
  
  data_dirs -- A list of strings that defines to which directories to put data. 
  """
  sfong_config = SFOngdal.SFOngConfiguration(name)
  if data_dirs:
    sfong_config.RecordingEnabled = True
    sfong_config.DataDirectories = data_dirs
  else:
    sfong_config.RecordingEnabled = False

  sfong_config.NInputThreads = 4
  sfong_config.NWorkerThreads = 6

  return sfong_config
    

def create_real_sfong(name, sfong_config, configrules=None, computer=None):
  """Creates a single real SFO application.

  This method returns a real SFO application, that will run on the computer
  specified.

  Keyword parameters (may be named):
  name -- This is the identifier assigned to this object
  
  sfong_config -- An SFOng configuration object to be used for the generated SFOng.

  configrules -- a ConfigurationRuleBundle

  computer -- An OKS dal Computer object where this SFI will run. If this
  parameter is not given or explicetly set to None, this SFO will run on the
  segment's default host.

  Returns an SFOngApplication object.
  """
  from pm.common import tdaqRepository, setup_dfapp
  
  retval = SFOngdal.SFOngApplication(name,
      SFOngConfiguration=sfong_config,
      Program = tdaqRepository.getObject('Binary', 'SFOng_main'))

  retval.InitTimeout = 60
  retval.IfExitsUnexpectedly = 'Error'
  retval.IfFailsToStart = 'Error'
  retval.InterfaceName = 'rc/commander'
  retval.ConfigurationRules = configrules
  if computer is not None: retval.RunsOn = computer

  return retval
