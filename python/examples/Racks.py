#! /usr/bin/env tdaq_python

"""
This module provides a few utility functions for geoname to coordinate conversions as well as the following objects:

idx2geo --> dictionary for index to geo conversion e.g. idx2geo['01'] = 'Y.13-06.D2'
geo2idx --> dictionary for geo to index conversion e.g. geo2idx['Y.13-06.D2'] =  '01'
Rack --> class representing one rack
acks --> list of Rack objects: all the racks found in the OKS DB
"""
from __future__ import print_function

from past.builtins import cmp
from builtins import map
from builtins import str
from builtins import range
from builtins import object
dbg = True

def geotorcl(geoname):
    """ Y.13-06.D2 --> (6,13,2) """

    rc,l =geoname.split('.')[1:]
    l = int(l[1:]) #'D2' --> 2
    c,r = list(map(int, rc.split('-'))) #'13-06' --> 13,6
    return (r,c,l)
    
def rcltogeo(r,c,l):
    """ (6,13,2) --> Y.13-06.D2 """

    return 'Y.%02d-%02d.D%1d' % (c,r,l)


def _buildgeo2idx():

    idx2geo = {}
    
    for idx in range(1,96):

        l = r = c = 0

        if idx < 44:
            ##Level 2
            l = 2

            if idx > 8:
                ## Row 4 & 2
                r = 4
                c = 29 - idx
                if c < 3:
                    r = 2
                    c += 17 
            
            else:
                ## Row 6
                r = 6
                if idx <= 6:
                    c = 14 - idx
                else:
                    c = 11 - idx
                
        else:
            ##Level 1
            l = 1
            r = 6
            c = idx - 41

            if c > 19:
                r -= 2
                c -= 17
            if c > 20:
                r -= 2
                c-= 18

        idx2geo['%02d' % idx] = rcltogeo(r,c,l)

    return idx2geo


idx2geo = _buildgeo2idx()
geo2idx = dict([(v,k) for k,v in list(idx2geo.items())])
del k,v

class Rack(object):
    """
    Represents a rack. Includes the type (tpu,xpu,ef), the geoname and the index,
    and the list of LFS(s) and nodes. The latter are objects of type
    config.dal.Computer
    """
    
    def __init__(self, rack = None, nodes = None, lfs = None, \
                 geoname = None, idx = None,  typ='xpu'):

        if geoname and idx:
            raise ValueError("Either 'geoname' or 'idx' must be used, not both")

        if geoname:
            self.geoname = geoname
            self.idx = geo2idx[self.geoname]
        elif idx:
            self.idx = idx
            #self.geoname = idx2geo[self.idx]
            self.geoname = str(self.idx)
        else:
            raise ValueError("Either 'geoname' or 'idx' must be not None")
        
        self.nodes = nodes
        self.typ = typ
        self.lfsnodes = lfs
        self.rack = rack

    def __str__(self):
        fmt = 'rack: %s idx: %s   lfs: %s  typ: %03s  nodes: %2d    first: %24s   last: %24s'
        s = fmt % (self.rack.id, self.idx, self.lfsnodes[0].id, self.typ, \
                   len(self.nodes), self.nodes[0].id, self.nodes[-1].id)
        return s

    def FirstNode(self):
        return int(self.nodes[0].id.split('-')[3].split('.')[0])

    def LastNode(self):
        return int(self.nodes[-1].id.split('-')[3].split('.')[0])

    def __repr__(self):
        return self.__str__()


def _fetchnodes(holder, input):

    for el in input:
        try:
            lst = el.Contains
            _fetchnodes(holder, lst)
        except AttributeError:
            holder.append(el)
        

def _fetchracks():
    import pm.project
    from operator import attrgetter
  
    try:
        db = pm.project.Project('daq/hw/hosts.data.xml')
    except RuntimeError:
        return []
    racks = db.getObject('Rack')

    if dbg:
        print('racks: ',racks)

    result = []
    
    for r in racks:
        typ = r.id.split('-',1)[0]
        idx = r.id.split('-',2)[-1]
        if dbg:
            print('rack = %s, typ = %s, idx = %s' % (r.id,typ,idx))
        lfs = []
        _fetchnodes(lfs, r.LFS)
        nodes = []
        _fetchnodes(nodes, r.Nodes)
        nodes.sort(key=attrgetter('id'))
        result.append(Rack(r, nodes, lfs, idx = idx, typ = typ ))
        
    result.sort(cmp=lambda a,b: cmp(a.rack.id,b.rack.id))

    return result

Racks = _fetchracks()


        
if __name__ == '__main__':

    for r in Racks:
        print(r)

