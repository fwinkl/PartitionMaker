#!/usr/bin/env tdaq_python
"""This is an example of the LVL2 farm configuration on Point 1.
"""
from __future__ import print_function
from __future__ import absolute_import

from builtins import range
import os, sys

import pm.multinode
from pm.multinode import localhost
import pm.farm
import pm.utils
from . import Racks

dbg = True

#
# figure out tdaq release number
#
tdip = os.environ['TDAQ_INST_PATH'].split('/')
tdaq = tdip[-2].split('-')

# This module's includes
if tdaq[0].startswith('nightly'):
    print('\n---------------------------')
    print('INFO: using release',tdaq[0])
    print('---------------------------\n')
elif tdaq[0] != 'tdaq' or int(tdaq[1]) < 5 or int(tdaq[2]) < 2 or int(tdaq[3]) < 0:
    print('----------------------------------------------------------')
    print('ERROR: this script must be used with release tdaq-05-02-00 or higher')
    print(tdaq)
    print('----------------------------------------------------------')
    sys.exit(1)

includes = [
    'daq/hw/hosts.data.xml',
    ]

try:
    db = pm.project.Project('daq/hw/hosts.data.xml')
except RuntimeError:
    includes = []

rs = Racks.Racks

if len(rs) == 0:
    print('WARNING: could not find any Rack classes in hw db!')
    #sys.exit(1)

rns_xpu = []
rns_ef = []

rd = {}
for r in rs:
    rd[r.geoname] = r
    if dbg: 
        print(r, r.typ)
    if r.typ == 'ef':
        rns_ef.append(r.geoname)
    else:
        rns_xpu.append(r.geoname)

n_ef  = len(rns_ef)
n_xpu = len(rns_xpu)

print('Found these numbers of racks')
print('EF racks:       %3d' % n_ef)
print('[XT]PU racks:   %3d' % n_xpu)

print('\nlist of include files for this segment:')
for i in includes:
    print(i)
print()

hosts = {}
for k in includes: hosts.update(pm.farm.load(k, short_names=True))

L_hltsv    = 1
Nsubfarms  = 1
phys_racks = n_ef + n_xpu
log_racks  = n_ef + n_xpu

print('\nrequested:\n----------')
print('HLTSV hosts            :',L_hltsv)
print('physical racks         :',phys_racks)
print('HLT subfarms           :',Nsubfarms)

#
# determine the name of the cluster
#
try:
    cluster = rs[0].nodes[0].id.split('.')[0].split('-')[1]
except IndexError:
    cluster = 'unknownCluster'

# hltsv is the list of hltsv host entries to be used

hltsv = []

# usually just 1 HLTSV

if cluster == 'tbed':
    svname = 'pc-tbed-r3-01'
    default = hosts['vm-tbed-onl-02'] # as per Gio's partition
    sfo = [localhost]                 # need to find a better way to specify SFOs 

elif cluster == 'tdq':
    svname = 'pc-tdq-dc-02'           
    default = hosts['pc-tdq-onl-80']  # this it used to be in P1
    sfo = []                          # need to find a better way to specify SFOs 
    for i in range(12):
        h = 'pc-tdq-sfo-%02d' % (i+1)
        print('SFO ',h)
        sfo.append(hosts[h])

elif cluster == 'preseries':
    svname = 'pc-preseries-l2sv-01'
    default = hosts['pc-preseries-onl-02']
    sfo = ['pc-preseries-sfo-02']

else:
    print('WARNING: apparently not running on P1, PreSeries or TBED') 
    print('         change hltsv host name manually')
    localhost = pm.farm.local_computer()
    svname = localhost.id
    sfo = [localhost]
    default = localhost

if len(hosts) > 0:
    try:
        hltsv.append(hosts[svname])
    except KeyError:
        hltsv.append(localhost)
else:
    hltsv.append(localhost)

print('\nusing the following hosts for HLTSV:')
for l in hltsv:
    print(l.id)

print('\nPhysical racks:\n===============')

for R in Racks.Racks:
    print(R.rack.id)

nn = phys_racks
hltpu_subfarmlist = {}
if len(Racks.Racks) > 0:
    hltpu_subfarmlist[0] = [R.rack for R in Racks.Racks]
else:
    from pm.dal import dal 
    dummyRack = dal.Rack('dummyRack')
    dummyRack.Nodes = [ localhost, localhost ] # needs at least 2 hosts
    dummyRack.LFS = [ localhost ]
    print(dummyRack)
    hltpu_subfarmlist[0] = [dummyRack]

# hlt_subfarm   is a list of hlt subfarms, one for each hltsv

hlt_subfarms = Nsubfarms * [None]

if cluster == 'tdq':
    hltsv_onl = hosts['pc-tdq-onl-80'] # controllers on onl-80 as per Gio's request
elif cluster == 'tbed':
    hltsv_onl = hosts['vm-tbed-onl-02']
else:
    hltsv_onl = localhost

for s in range(Nsubfarms):
    hlt_subfarms[s] = pm.multinode.hlt_subfarm(hltpu_subfarmlist[s],
                                               default_host=hltsv_onl,
                                               is_histogram=None,
                                               is_resource=None,
                                               name=None)

print('len(hltsv)        : %d' % len(hltsv))
print('len(hlt_subfarms) : %d' % len(hlt_subfarms))

hlt_farm = pm.multinode.hlt_farm(hltsv,
                                hlt_subfarms,
                                sfo=sfo,
                                default_host=default)

# if you want to pretty print in the end and to verify this module is
# actually loadable, uncomment the following line

print('\n\n========\nhlt_farm:\n========\n')
print('hlt_farm.keys()',list(hlt_farm.keys()))
print("hlt_farm['hlt_farm']")

for f in hlt_farm['hlt_farm']:
    print(type(f),list(f.keys()))

print('\nprettyprinted:')
print(pm.multinode.prettyprint(hlt_farm))

