#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Mon 10 Dec 2007 02:09:04 PM CET 

"""Sets Histogram transformations on DCApplicationConfig objects.
"""
from __future__ import print_function

from builtins import str
from pm.dal import *
import pm.project
import EventApps.myopt
import re
import sys
import logging

help_msg="""Sets histogram transformations on DCApplicationConfig objects. 

The general form of this program is used for resetting range on histograms:

  $ pm_histo.py --add '/DEBUG.*Time.*|autobinon' mypartition.data.xml

Will add the given transformation to all DCApplicationConfig objects found on
the database 'mypartition.data.xml', directly or indirectly.

To list transformations in the DCApplicationConfig use this form:

  $ pm_histo.py --list mypartition.data.xml

To remove transformations in the DCApplicationConfig use this form:

  $ pm_histo.py --remove '.*DEBUG.*' mypartition.data.xml

Please note that the "remove" option uses python regular expressions which are
a superset of the regular expressions allowed by IS. The construction above
will remove all transformations from all DCApplicationConfig objects found in
the database given that have DEBUG on their "Operation" string.

These are allowed operations and their syntax according to Tomasz Bold:

setrange|left|right - define new range for histogram while keeping the bin
widths same. Moreover the new limits are aligned to old edges of old bins. So
histograms from other nodes are mergable with that changed one. Here the
resulting histogram would have more bins.

widenrange|number - is widening range i.e. widenrange|1.5 is making histogram
of 1.5 wider range (moving to the right). Again there will be more bins. And
bins will be aligned to the edges of old bins.

redefine|Nbins|left|right - i.e. redefine|100|0.|55. - makes the histogramming
of range 0-55 with 100 bins. Here the old binning is totaly discarded.
Histogram is not mergeable with old.

autobinon - turns on autobinning a la kCanRebing

autobinoff - disables above

The order in which these transformations are added to the database *IS*
relevant and respected by the application's histogramming infrastructure.
"""

# we start with our list of options to the program
option = {}
option['add'] = {'short': 'a', 
                 'arg': True, 
                 'default': [], 
                 'group': 'global',
                 'description': 'Adds a new transformation operation to the selected DCApplicationConfig objects selected'
                }

option['remove'] = {'short': 'r', 
                    'arg': True, 
                    'default': [], 
                    'group': 'global',
                    'description': 'Removes a transformation operation from the selected DCApplicationConfig objects selected. (This parameter should be a valid python regular expression.) To remove all, for example, use ".*"'
                    }

option['select'] = {'short': 's', 
                    'arg': True, 
                    'default': '.*', 
                    'group': 'global', 
                    'description': 'Selects a subset of the DCApplicationConfig objects to modify. By default, I modify them all. (This parameter should be valid python regular expression.)'
                   }

option['list'] = {'short': 'l', 
                  'arg': False, 
                  'default': None, 
                  'group': 'global', 
                  'description': 'Lists transformations attached to each DCApplicationConfig'
                 }

def oks_name(s):
  translate = {'|': '-', '.': '_', '*': '_'}
  retval = []
  for k in s:
    if k in translate: retval.append(translate[k])
    else: retval.append(k)
  return ''.join(retval)

def main():
  
  parser = EventApps.myopt.Parser(extra_args=True, post_options=help_msg)
  for (k,v) in list(option.items()):
    parser.add_option(k, v['short'], v['description'], v['arg'],
        v['default'], v['group'])
    
  if len(sys.argv) < 2:
    print(parser.usage())
    sys.exit(2)

  (kwargs, extra) = parser.parse(sys.argv[1:], 
      prefix='"%s" options:' % sys.argv[0])

  # check for the extra argument
  if len(extra) != 1:
    logging.warning('There should be one and only one extra argument: the database file I cannot accept: %s' % extra)
    sys.exit(1)

  # get all HLT application objects and their DCApplicationConfig counterparts
  db = pm.project.Project('oksconfig:%s' % extra[0])
  config = db.getObject('DCApplicationConfig')

  # find the highest HistoOperation identifier available
  highest_op = 1
  regexp = re.compile('\S*-(?P<n>\d+)$')
  for k in db.getObject('HistogramOperation'):
    m = regexp.match(k.id)
    if m and int(m.group('n')) >= highest_op: highest_op = int(m.group('n'))+1
 
  if kwargs['select']:
    target = re.compile(kwargs['select'])
    config = [k for k in config if target.match(k.id) is not None]

  save = []
  for to_remove in kwargs['remove']:
    save = list(config)
    target = re.compile(to_remove)
    for obj in config:
      for update in obj.refDC_HistogramTypeUpdate:
        save.append(update)
        update.ref_HistogramOperationConfig = [k for k in \
          update.ref_HistogramOperationConfig if \
          target.match(k.Operation) is None]

  for to_add in kwargs['add']:
    save = list(config)
    for obj in config:
      transform = None
      transform_name = '%s-Transforms' % obj.id
      for update in obj.refDC_HistogramTypeUpdate:
        if update.id == transform_name: transform = update
        
      if not transform:
        transform = DFdal.DC_HistogramTypeUpdate(transform_name, name='TRANS',
            ref_HistogramOperationConfig=[])
        if obj.refDC_HistogramTypeUpdate:
          obj.refDC_HistogramTypeUpdate.append(transform)
        else:
          obj.refDC_HistogramTypeUpdate = [transform]
        logging.info('Added %s to %s' % (repr(transform), repr(obj)))
         
      new_obj = DFdal.HistogramOperation('HistogramOperation-'+str(highest_op),
          Operation=to_add)
      highest_op += 1
      if new_obj in transform.ref_HistogramOperationConfig:
        logging.warning('Not re-adding \'%s\' to %s' % (to_add, repr(obj)))
        continue

      if transform.ref_HistogramOperationConfig:
        transform.ref_HistogramOperationConfig.append(new_obj)
      else:
        transform.ref_HistogramOperationConfig = [new_obj]
      db.updateObjects([transform], recurse=True)
        
      logging.info('Added operation \'%s\' to %s' % (to_add, repr(obj)))

  if save:
    db.updateObjects(save, recurse=False)
        
  if kwargs['list']:
    for obj in config:
      print('Transformations for %s, in order:' % repr(obj))
      counter = 1
      for update in obj.refDC_HistogramTypeUpdate:
        for op in update.ref_HistogramOperationConfig:
          print(("  [%2d] '" % counter ) + op.Operation + "'")
          counter += 1

  sys.exit(0)
 
if __name__ == '__main__': main()
