#!/usr/bin/env tdaq_python
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 21 Aug 2007 12:14:52 PM CEST 
from __future__ import print_function
import os, sys
import pm
import pm.farm
import pm.common
import pm.project

from pm.partition.utils import parse_cmdline, post_process
from pm.segment.ros import option, gendb
from pm.preloaded_helpers import generate_preloaded_farm, feed_with_preloaded_params


args = parse_cmdline(gendb.__doc__, option)
if args['preload-hosts'] or args['preload-channels']:
  assert args['preload-hosts'] and args['preload-channels'], \
      "If you want to generate a new preloaded ROS farm, specify the " \
      "host & channel files with --preload-hosts and --preload-channels. " \
      "Otherwise, use --ros-farm to provide your farm."
  #assert args['use-ros'], "Preloading is only supported for real ROSs, add --use-ros to your command line"

  hostlistpath = os.path.abspath(args['preload-hosts'])
  hostdb = os.getenv('ROS_HOSTFILE') or "daq/hw/hosts-ros.data.xml"
  if not os.getenv('ROS_HOSTFILE'):
    print("INFO: ROS_HOSTFILE environment variable not set; falling back to default host db '%s'" % hostdb)

  hosts = pm.farm.load(hostdb, short_names=True)
  # remove ComputerSet etc.
  hosts_ = hosts.copy()
  for k in hosts_:
    if not hosts[k].isDalType('Computer'):
      hosts.pop(k)

  # filter hosts by file
  with open(hostlistpath, 'rt') as hostlistfile:
    hostlist = [line.strip() for line in hostlistfile if not line.startswith('#')]
  hosts = pm.farm.filter_hosts_by_list(hosts, hostlist)

  assert hosts, "You seem to have filtered out all the hosts. Check that you provided the correct hosts database (I'm using %s, use the envirnoment variable ROS_HOSTFILE to change this) and the host list file." % hostdb

  roblist_path = os.path.abspath(args['preload-channels'])

  subdetector_filter = []
  if args['preload-subdetectors']:
    assert all([id.startswith('0x') for id in args['preload-subdetectors'].split(',')]), "--preload-subdetectors: You need to specify each subdetector id explicitly in hex by prepending it with '0x'!"
    subdetector_filter = [int(id, 16) for id in args['preload-subdetectors'].split(',')]

  groups = []
  if args['preload-group']:
    group_idstrings = [gis.lstrip('[').rstrip(']') for gis in args['preload-group'].split('],[')]
    for gis in group_idstrings:
      assert all([id.startswith('0x') for id in gis.split(',')]), "--preload-group: You need to specify each subdetector id explicitly in hex by prepending it with '0x'!"
    groups = [[int(id, 16) for id in gis.split(',')] for gis in group_idstrings]

  ros_farm = generate_preloaded_farm(hosts, roblist_path, \
      subdetector_filter=subdetector_filter, groups=groups, host_order=hostlist)

  feed_with_preloaded_params(ros_farm)
  args['ros-farm'] = ros_farm
  args['ros-mode'] = 'preloaded-dc'

d = gendb(**args)
dbname = '.'.join(os.path.basename(sys.argv[0]).split('.')[:-1])
if len(d['segment']) == 1:
  dbname = d['segment'][0].id

if 'rebsegment' in d:
  rebseg = d['rebsegment'][0]

  # get the reb host and point the ROSs to it
  if 'reb-host' in args:
    # the raw argument might contain the interface suffix (e.g. -dc1)
    rcd_hostname = args['reb-host']
  else:
    rcd_hostname = rebseg.get('RCD')[0].RunsOn.id

  rosseg = d['segment'][0]
  rosseg.get('TCPDataOutConfig')[0].DestinationNode = [rcd_hostname] #string

  reb_dbname = rebseg.id
  # its probably not a good idea to use the normal segment
  # post_process on the REB segment
  # reb_segment = post_process(args, d['rebsegment'])

  # use the same includes as the normal segment
  reb_save_db = pm.project.Project(reb_dbname+'.data.xml', d['includes'])
  reb_save_db.addObjects(d['rebsegment'])

d['segment'] = post_process(args, d['segment'])

try:
    os.remove(dbname + '.data.xml')
    print('previous version of',dbname+'.data.xml','removed')
except:
    pass

save_db = pm.project.Project(dbname + '.data.xml', d['includes'])
save_db.addObjects(d['segment'])

if 'separate-channels' in args and args['separate-channels']:
  # this code separates the channels & detectors from the
  # main ROS segment and into a separate file.

  channels = save_db.getObject('InputChannel')

  # "..-robin-dc" -> "..-robin-channels"
  channel_mode = args['ros-mode'].split('-')[0]
  dbname_no_mode = dbname.split(channel_mode)[0]
  channels_filename = dbname_no_mode + channel_mode + '-channels.data.xml'

  #save channels in /tmp/pm.<pid>~/daq/segments/ROS (so that includes are correct)
  temp_channels_incdir = ("/tmp/pm.%d~/" % os.getpid())
  temp_channels_dir = temp_channels_incdir + "/daq/segments/ROS/"
  store_path_var = os.environ['TDAQ_DB_PATH']
  os.environ['TDAQ_DB_PATH'] = temp_channels_incdir+':'+os.environ['TDAQ_DB_PATH']

  try:
    os.makedirs(temp_channels_dir)
  except os.error:
    pass #suppress 'already exists' error; if something went wrong, we'll know

  # save to temp
  channels_includes = d['includes']
  temp_daq_channels_filename = temp_channels_dir + channels_filename

  # if the channels are already in the includes, skip saving the channel file
  extra_objs = pm.common.load(args['extra-includes'])
  if not any([c.id in extra_objs for c in channels]):
    channels_db = pm.project.Project(temp_daq_channels_filename, channels_includes)

  # remove 'extra' includes from channel file (these are normally for the segment)
    for i in args['extra-includes']:
      channels_db.removeInclude(i)
    channels_db.addObjects(channels)

  try:
    os.remove(os.path.abspath(channels_filename))
    print('previous version of',channels_filename,'removed')
  except:
    pass
  # we can't directly remove the objects from the segment db, since it would leave
  # dangling references. we also can't directly add the include file because of
  # namespace collisions. so, just create it again with the new include file
  #d['includes'].append('daq/segments/ROS/'+channels_filename)

  try:
    os.remove(dbname + '.data.xml')
    print('Separating channels: previous version of',dbname+'.data.xml','removed')
  except:
    pass

  save_db = pm.project.Project(dbname + '.data.xml', d['includes'])
  if not any([c.id in extra_objs for c in channels]):
    #ok, no clashes
    save_db.addInclude('daq/segments/ROS/'+channels_filename)
  save_db.addObjects(d['segment'])

  # restor TDAQ_DB_PATH
  os.environ['TDAQ_DB_PATH'] = store_path_var

if 'remove-segment' in args and args['remove-segment']:
  # remove the segment object
  seg_obj = save_db.getObject('Segment')
  save_db.removeObjects(seg_obj)


# delete temporary channels file & dir
try:
  import shutil
  shutil.move(os.path.abspath(temp_daq_channels_filename), os.path.abspath(channels_filename))
  shutil.rmtree(temp_channels_dir)
except:
  pass # suppress all errors (undefined vars, delete/permission
       # errors; we don't really care)


