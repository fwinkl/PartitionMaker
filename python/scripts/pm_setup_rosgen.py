#!/usr/bin/env tdaq_python
from __future__ import print_function
from builtins import input
import os, shutil, sys
import logging

USAGE = """Usage: %s [<destination dir>]

  Deploys the ROS generation kit under a local directory.
  If a destination directory is not provided, the kit will be deployed
  under ./roskit"""

DEPLOY_MESSAGE = """Deploying the ROS generation kit from:
  %s
to:
  %s

Press any key to continue...
""" 

# Check the environment sanity
# Only look for showstoppers
def check_env_sanity(SOURCE, DESTINATION):
  error = False

  if os.getenv('TDAQ_INST_PATH') is None:
    logging.error("TDAQ_INST_PATH is not set")
    error = True

  if not os.path.exists(SOURCE):
    logging.error("Could not find the ROS generation kit in %s" % SOURCE)
    error = True

  if os.path.exists(DESTINATION):
    logging.error("Destination directory: %s already exists" % DESTINATION)
    error = True

  if error:
    sys.exit(1)


def main():
  global USAGE, DEPLOY_MESSAGE
  # Substitute name of the script
  USAGE = USAGE % os.path.basename(sys.argv[0])

  # Default subdirectory unless we get an argument
  DESTINATION = os.getcwd() + '/roskit'

  if len(sys.argv)>1:
    # Display usage if we get any kind of option
    if sys.argv[1].startswith('-'):
      print(USAGE)
      sys.exit()
    else:
      DESTINATION = os.getcwd() + '/' + sys.argv[1]

  SOURCE = os.getenv('TDAQ_INST_PATH') + '/share/data/PartitionMaker/contrib'

  check_env_sanity(SOURCE, DESTINATION)

  # Substitute the directories in the message
  DEPLOY_MESSAGE = DEPLOY_MESSAGE % (SOURCE, DESTINATION)
  input(DEPLOY_MESSAGE)

  shutil.copytree(SOURCE, DESTINATION)

  # The release will be readonly on AFS, so we need to set some files back to
  # r/w
  for root, dirs, files in os.walk(DESTINATION):
    for f in files:
      if f.endswith('csv') or f == 'Makefile':
        os.chmod(os.path.join(root, f), 0o644)

if __name__=='__main__':
  main()

