#!/usr/bin/env tdaq_python
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 14 Aug 2007 01:50:33 PM CEST 

"""Generates a NetworkInterface database as we like it.
"""
from __future__ import print_function

help_msg="""Generates a message passing configuration database as we like it.
"""

def setup_parser():
  """Sets-up the command line parser."""
  from EventApps.myopt import Parser
  retval = Parser(short_intro=help_msg, extra_args=True)
  retval.add_option(int='dfm-l2sv', short='a',
                    description='The DFM <-> L2SV network label to use. An emtpy value causes the connection be done via the control network', 
                    arg=True,
                    default="")
  retval.add_option(int='dfm-ros', short='b',
                    description='The DFM <-> ROS network label to use. An emtpy value causes the connection be done via the control network', 
                    arg=True,
                    default="")
  retval.add_option(int='dfm-sfi', short='c',
                    description='The DFM <-> SFI network label to use. An emtpy value causes the connection be done via the control network', 
                    arg=True,
                    default="")
  retval.add_option(int='l2pu-l2sv', short='d',
                    description='The L2PU <-> L2SV network label to use. An emtpy value causes the connection be done via the control network',
                    arg=True,
                    default="")
  retval.add_option(int='l2pu-ros', short='e',
                    description='The L2P <-> ROS network label to use. An emtpy value causes the connection be done via the control network', 
                    arg=True,
                    default="")
  retval.add_option(int='ros-sfi', short='f',
                    description='The ROS <-> SFI network label to use. An emtpy value causes the connection be done via the control network', 
                    arg=True,
                    default="")
  retval.add_option(int='l2rh-sfi', short='g',
                    description='The L2RH <-> SFI network label to use. An emtpy value causes the connection be done via the control network', 
                    arg=True,
                    default="")
  retval.add_option(int='l2rh-dfm', short='i',
                    description='The L2RH <-> DFM network label to use. An emtpy value causes the connection be done via the control network', 
                    arg=True,
                    default="")
  retval.add_option(int='l2rh-l2pu', short='j',
                    description='The L2RH <-> L2PU network label to use. An emtpy value causes the connection be done via the control network', 
                    arg=True,
                    default="")
  return retval

def genmaps(f, t, label):
  """Generates an interface maps for a certain connection type."""
  if len(label) == 0: return []
 
  from pm.dal import DFdal
  way1 = DFdal.InterfaceMap("%s-%s" % (f, t), 
                            Peer=t, Label=label, Protocol="default")
  way2 = DFdal.InterfaceMap("%s-%s" % (t, f),
                            Peer=f, Label=label, Protocol="default")
  return [way1, way2]

def main():
  import sys
  import pm.project
  
  parser = setup_parser()
 
  if len(sys.argv) == 1:
    print(parser.usage())
    sys.exit(1)

  opt, args = parser.parse(sys.argv[1:])

  if len(args) != 1:
    raise RuntimeError("The number of extra arguments should be == 1, got %s" % args)

  db = pm.project.Project(args[0], ['daq/schema/df.schema.xml'])
  objs = []
  objs.extend(genmaps("DFM", "L2SV", opt['dfm-l2sv']))
  objs.extend(genmaps("DFM", "ROS", opt['dfm-ros']))
  objs.extend(genmaps("DFM", "SFI", opt['dfm-sfi']))
  objs.extend(genmaps("L2PU", "L2SV", opt['l2pu-l2sv']))
  objs.extend(genmaps("L2PU", "ROS", opt['l2pu-ros']))
  objs.extend(genmaps("ROS", "SFI", opt['ros-sfi']))
  objs.extend(genmaps("LVL2ResultHandler", "SFI", opt['l2rh-sfi']))
  objs.extend(genmaps("LVL2ResultHandler", "DFM", opt['l2rh-dfm']))
  objs.extend(genmaps("LVL2ResultHandler", "L2PU", opt['l2rh-l2pu']))
  db.addObjects(objs)
  return 0

if __name__ == "__main__":
  from sys import exit
  exit(main())
