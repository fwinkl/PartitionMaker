#!/usr/bin/env tdaq_python

from __future__ import print_function
import pm.project

# OBSOLETE since a long time
#import pm.tdaqpartition   
#import pm.farmscanner

import sys
import re
from EventApps import myopt
import logging

def setOptions():
	p = myopt.Parser(extra_args = False, short_intro = 'Generates a partition based on already existent segments.')
	p.add_option('output', 'o', 'The output partition file name', True, None)
	p.add_option('ros-file', 'r', 'The ros segment file name', True, None)
	p.add_option('ros-seg-name', 'R', 'The ID of the top level ROS segment name. If not specified, the name of the file (without the .data.xml) will be used ', True, None)
	p.add_option('hlt-file', 'f', 'The ros segment file name', True, None)
	p.add_option('hlt-seg-name', 'H', 'The ID of the top level HLT segment name. If not specified, the name of the file (without the .data.xml) will be used ', True, None)
	p.add_option('default-host', 'd', "The name of the Partition's default host. It must be a node defined in one of the segments file (or its includes)", True, None)

	return p	


def main():
	p = setOptions()
	
	if len(sys.argv) is 1:
		print(p.usage())
		sys.exit(1)
	
	opt = p.parse(sys.argv[1:])
	
	if (opt['output'] is None):
		logging.error('You must provide an output partition file name.')
		sys.exit(2)

	if (opt['default-host'] is None):
		logging.error('You must provide an default host for the partition obejct.')
		sys.exit(3)

	try:
		rosSeg = hltSeg = None
		regExp = re.compile('(?P<NAME>.+)\.data\.xml')
		proj = pm.project.Project(opt['output'],[])
		
		if opt['ros-file'] is not None:
			segName = opt['ros-seg-name']
			if segName is None: segName = regExp.match(opt['ros-file']).group('NAME')
			rosSeg = pm.project.Project(opt['ros-file']).getObject('Segment', segName)
			proj.addInclude(opt['output'], opt['ros-file'])
			proj.commit()
			print('ROS segment added!')

		if opt['hlt-file'] is not None:
			segName = opt['hlt-seg-name']
			if segName is None: segName = regExp.match(opt['hlt-file']).group('NAME')
			hltSeg = pm.project.Project(opt['hlt-file']).getObject('Segment', segName)
			proj.addInclude(opt['output'], opt['hlt-file'])
			proj.commit()
			print('HLT segment added!')
			
			
		defHost = proj.getObject('Computer', opt['default-host'])
		cluster = pm.farmscanner.FarmScanner()
		cluster.addNodes(defHost, pm.farmscanner.UI)
		
		partObj = pm.tdaqpartition.TDAQPartition(cluster = cluster, 
                                                         name = opt['output'], 
                                                         rosSeg = rosSeg, 
                                                         hltSeg = hltSeg)
		for incFile in partObj.getIncludes()[opt['output']]: 
			proj.addInclude(opt['output'], incFile)
		proj.commit()
		proj.addObjects(partObj.getPartition())
		
		print('Done!')
		
	except Exception as m:
		logging.error('Error generating the partition file!\n%s' % m)

if __name__ == "__main__":
	main()
