#!/usr/bin/env tdaq_python
from __future__ import print_function
import pm.option.common as common
import pm.option.specific as specific
import pm.option.segcommon as segcommon
"""
 <file path="daq/sw/repository.data.xml"/>
 <file path="daq/sw/common-templates.data.xml"/>
 <file path="daq/hw/hosts.data.xml"/>
 <file path="daq/segments/HLT-Common.data.xml"/>
 <file path="daq/segments/HLT/SFODBConnection.data.xml"/>
 <file path="daq/sw/HLT_SW_Repository.data.xml"/>
 <file path="daq/segments/HLT/RoIBSegment_LVL1.data.xml"/>
"""
dbg = False

def gendb(**kwargs):
    """Adds mucal stuff to an HLT segment.

    This method adds the muon calibnration infrastructure to an exising HLT segment file

    Returns a dictionary that contains, as keys, the 'segment' (dal.Segment
    object) and the 'includes' python list of includes necessary to make the
    segment a valid OKS database.  
    """
    import pm.project
    import pm.utils
    import pm.multinode
    import pm.mucal

    includes = list(pm.multinode.includes)
    # now add the explicit includes that are needed to avoid creating duplicate objects
    includes.append("daq/sw/repository.data.xml")
    includes.append("daq/sw/common-templates.data.xml")
    includes.append("daq/hw/hosts.data.xml")
    includes.append("daq/segments/HLT-Common.data.xml")
    includes.append("daq/segments/HLT/SFODBConnection.data.xml")
    includes.append("daq/sw/HLT_SW_Repository.data.xml")
    #includes.append("daq/segments/HLT/RoIBSegment_LVL1.data.xml")
    #includes.append("daq/segments/HLT/RoIBInfrastructure_LVL1.data.xml")
    #includes.append("combined/segments/connections/RoIB_L1Source.data.xml")

    if len(kwargs['extra-includes']) != 0:
        if dbg:
            print("kwargs['extra-includes'] =",kwargs['extra-includes'])
        #includes = pm.utils.merge_unique(includes, kwargs['extra-includes'])
        if dbg:
            print('includes =',includes)

    ps  = pm.project.Project(kwargs['extra-includes'][0])
    seg = ps.getObject('Segment','HLT')

    if dbg:
        print('seg =',seg)

    if kwargs['muon-calibration']:
        mucal_db = pm.project.Project(kwargs['muon-calibration'])
        pm.mucal.create_top_level_segment('MUCal-' + seg.id, 
                                          pm.project.Project(kwargs['muon-calibration']), 
                                          seg)
        includes.append(kwargs['muon-calibration'])
    else:
        print('ERROR: No MUCalServer segment file provided')
        sys.exit(1)


    return {'segment': [seg], 'includes': includes}

import os, sys
from pm.dal import dal

import copy
option = copy.deepcopy(common.option)

if dbg:
    print('option.keys:\n',list(option.keys()))
    print('common.option.keys:\n',list(common.option.keys()))
    print('segcommon.option.keys:\n',list(segcommon.option.keys()))
    print('specific.option.keys:\n',list(specific.option.keys()))

# This is a list of specific options that need to be taken from the utils
# you can look them up over there to see their meaning

wanted = ['muon-calibration']
for k in wanted: 
    option[k] = copy.deepcopy(specific.option[k])

if dbg:
    print('after deepcopy:\noption.keys:\n',list(option.keys()))

from pm.partition.utils import parse_cmdline, post_process
from pm.segment.hlt import option
import pm.project

args = parse_cmdline(gendb.__doc__, option)
d = gendb(**args)

p0 = pm.common.load(args['extra-includes'])
o0 = [k for (k,o) in list(p0.items()) if not (isinstance(o,dal.ComputerBase) or isinstance(o,dal.NetworkInterface))]

ps = pm.project.Project(args['extra-includes'][0])
objs1 = ps.getAllObjects()

trgconf = ps.getObject('TriggerConfiguration')

if dbg:
    print('dir(ps) ',dir(ps))
    print('d =',d)
    print("\nbefore post_process: d['segment'] =", d['segment'])
    print('trgconf ',trgconf)
o1 = [o.id for o in objs1 if not (isinstance(o,dal.ComputerBase) or isinstance(o,dal.NetworkInterface))]


dbname = args['extra-includes'][0].split('.')[0]
if dbg: print('dbname: ',dbname)
d['segment'] = post_process(args, d['segment'])

if dbname == '': dbname = 'HLT'
dbname += '-mucal'
if dbg:
    print('dbname =',dbname)

save_db = pm.project.Project(dbname + '.data.xml', d['includes'])
save_db.addObjects(d['segment'])
save_db.addObjects(objs1)
objs2 = save_db.getAllObjects()
o2 = [o.id for o in objs2 if not (isinstance(o,dal.ComputerBase) or isinstance(o,dal.NetworkInterface))]

if dbg:
    o0.sort()
    o1.sort()
    o2.sort()

    print('\nfrom pm.common.load:\n',o0)
    print('\nfrom pm.project.Project:\n',o1)
    print('\nafter addObjects:\n',o2)

    in0not1 = [e for e in o0 if e not in o1]
    in1not0 = [e for e in o1 if e not in o0]

    in1not2 = [e for e in o1 if e not in o2]
    in2not1 = [e for e in o2 if e not in o1]

    in0not2 = [e for e in o0 if e not in o2]
    in2not0 = [e for e in o2 if e not in o0]

    print('\nin o0 but not in o1', in0not1)
    print('\nin o1 but not in o0', in1not0)

    print('\nin o1 but not in o2', in1not2)
    print('\nin o2 but not in o1', in2not1)

    print('\nin o0 but not in o2', in0not2)
    print('\nin o2 but not in o0', in2not0)

