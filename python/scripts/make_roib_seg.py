#!/usr/bin/env tdaq_python

from __future__ import print_function
import pm.project
from pm.dal import dal
from pm.dal import DFdal

import sys

if len(sys.argv) > 1:
    roib_host = sys.argv[1]
else:
    roib_host = 'sbc-tdq-roib-00'


includes = ['daq/schema/df.schema.xml',
            'daq/sw/repository.data.xml',
            'combined/segments/connections/RoIB_L1Source.data.xml',
            'HLT-internal.data.xml']

# ----------------------------------------
# start working on RoIBInfrastructure_LVL1
# ----------------------------------------

p1 = pm.project.Project('RoIBInfrastructure_LVL1.data.xml',includes)
#
# RoIBInputChannels
#
l1calo_cp3            = DFdal.RoIBInputChannel('L1CALO_cp3')
l1calo_cp3.ChannelID  = 3
l1calo_cp3.L1Source   = p1.getObject('Resource','RoIB_L1Source_L1CALO_cp3')
#
l1calo_jep0           = DFdal.RoIBInputChannel('L1CALO_jep0')
l1calo_jep0.ChannelID = 7
l1calo_jep0.L1Source  = p1.getObject('Resource','RoIB_L1Source_L1CALO_jep0')
#
l1calo_jep1           = DFdal.RoIBInputChannel('L1CALO_jep1')
l1calo_jep1.ChannelID = 11
l1calo_jep1.L1Source  = p1.getObject('Resource','RoIB_L1Source_L1CALO_jep1')
#
inputchannel_2           = DFdal.RoIBInputChannel('InputChannel_2')
inputchannel_2.ChannelID = 2
#
inputchannel_6           = DFdal.RoIBInputChannel('InputChannel_6')
inputchannel_6.ChannelID = 6
#
inputchannel_10           = DFdal.RoIBInputChannel('InputChannel_10')
inputchannel_10.ChannelID = 10
#
l1calo_cp0           = DFdal.RoIBInputChannel('L1CALO_cp0')
l1calo_cp0.ChannelID = 1
l1calo_cp0.L1Source  = p1.getObject('Resource','RoIB_L1Source_L1CALO_cp0')
#
l1calo_cp1           = DFdal.RoIBInputChannel('L1CALO_cp1')
l1calo_cp1.ChannelID = 5
l1calo_cp1.L1Source  = p1.getObject('Resource','RoIB_L1Source_L1CALO_cp1')
#
l1calo_cp2           = DFdal.RoIBInputChannel('L1CALO_cp2')
l1calo_cp2.ChannelID = 9
l1calo_cp2.L1Source  = p1.getObject('Resource','RoIB_L1Source_L1CALO_cp2')
#
ttc_disabled           = DFdal.RoIBInputChannel('TTC_disabled')
ttc_disabled.ChannelID = 0
#
ctp                    = DFdal.RoIBInputChannel('CTP')
ctp.ChannelID          = 4
ctp.L1Source           = p1.getObject('Resource','RoIB_L1Source_CTP')
#
muctpi                 = DFdal.RoIBInputChannel('MUCTPI')
muctpi.ChannelID       = 8
muctpi.L1Source        = p1.getObject('Resource','RoIB_L1Source_MUCTPI')
#
objs = [ l1calo_cp3, l1calo_jep0, l1calo_jep1,
         inputchannel_2, inputchannel_6, inputchannel_10,
         l1calo_cp0, l1calo_cp1, l1calo_cp2,
         ttc_disabled, ctp, muctpi ]
#
p1.addObjects(objs)
#
# RoIBOutputChannels
#
hltsv_1 = DFdal.RoIBOutputChannel('HLTSV_1')
hltsv_1.ChannelID      = 0
hltsv_1.L2SVConnection = p1.getObject('HLTSVApplication','HLTSV')
#
#
#outputchannel_6 = DFdal.RoIBOutputChannel('OutputChannel_6')
#outputchannel_6.ChannelID = 5
#
#outputchannel_7 = DFdal.RoIBOutputChannel('OutputChannel_7')
#outputchannel_7.ChannelID = 6
#
p1.addObjects([ hltsv_1 ])
#p1.addObjects([ l2sv_1, l2sv_2, l2sv_3, l2sv_4, l2sv_5, l2sv_6, l2sv_7, l2sv_8 ])
##                outputchannel_6, outputchannel_7 ])
#
# RoIBInputBoards
#
slot12 = DFdal.RoIBInputBoard('Slot_12')
slot12.VMESlot = 12
slot12.Contains = [ l1calo_cp3, l1calo_jep0, l1calo_jep1 ] 
#
slot13 = DFdal.RoIBInputBoard('Slot_13')
slot13.VMESlot = 13
slot13.Contains = [ inputchannel_2, inputchannel_6, inputchannel_10 ]
#
slot14 = DFdal.RoIBInputBoard('Slot_14')
slot14.VMESlot = 14
slot14.Contains = [ l1calo_cp0, l1calo_cp1, l1calo_cp2 ]
#
slot15 = DFdal.RoIBInputBoard('Slot_15')
slot15.VMESlot = 15
slot15.Contains = [ ttc_disabled, ctp, muctpi ]
#
p1.addObjects([ slot12, slot13, slot14, slot15 ])
#
# RoIBOutputBoards
#
slot16 = DFdal.RoIBOutputBoard('Slot_16')
slot16.VMESlot = 16
#slot16.Contains = [ l2sv_1, l2sv_2, l2sv_3, l2sv_4 ]
slot16.Contains = [ hltsv_1 ]
#
slot17 = DFdal.RoIBOutputBoard('Slot_17')
slot17.VMESlot = 17
#slot17.Contains = [ l2sv_5, l2sv_6, l2sv_7, l2sv_8 ]
#
slot18 = DFdal.RoIBOutputBoard('Slot_18')
slot18.VMESlot = 18
#
slot19 = DFdal.RoIBOutputBoard('Slot_19')
slot19.VMESlot = 19
#
p1.addObjects([ slot16, slot17, slot18, slot19 ])
#
# ResourceSets
#
roibInput = dal.ResourceSet('RoIBInput')
roibInput.Contains = [slot15,slot14,slot13,slot12]
#
roibOutput = dal.ResourceSet('RoIBOutput')
#roibOutput.Contains = [slot16,slot17]
roibOutput.Contains = [slot16]
#
p1.addObjects([roibInput,roibOutput])

# ---------------------------------
# done with RoIBInfrastructure_LVL1 
# ---------------------------------
p1.commit()

# --------------------------------- 
# start working on RoIBSegment_LVL1
# ---------------------------------

includes = ['daq/schema/core.schema.xml',
            'daq/schema/df.schema.xml',
            'daq/hw/hosts.data.xml',
            'daq/sw/repository.data.xml',
            #'daq/segments/L2/RoIBInfrastructure_LVL1.data.xml'
            'RoIBInfrastructure_LVL1.data.xml'
            ]

import pm.farm
hosts = {}
for k in includes: hosts.update(pm.farm.load(k, short_names=True))

p2 = pm.project.Project('RoIBSegment_LVL1.data.xml',includes)

#
# XXX may need to convert this part to the new way of monitoring. For now just leave it out.
#
if False:
    dc_isru = DFdal.DC_ISResourceUpdate('ROIB')
    dc_isru.name = 'RoIB'
    dc_isru.delay = 5
    dc_isru.activeOnNodes = ['ROIB-1']
    #
    storage = DFdal.HistogramStorage('ROIB-HistogramStorage')
    storage.ISServerName = 'Histogramming'
    #
    providercfg = DFdal.HistogramProviderConfig('ROIB-SporadicProvider')
    providercfg.PublishProviderName = '.*'
    providercfg.UseHistory = False
    providercfg.UpdateFrequency = 30
    providercfg.SummingMode = 'Add'
    providercfg.ResultContent = 'Sum'
    providercfg.Storage = [ storage ]
    #
    dc_htu = DFdal.DC_HistogramTypeUpdate('ROIBDEBUG')
    dc_htu.name = 'ROIBDEBUG'
    dc_htu.path = '/DEBUG/RoIBHistograms'
    dc_htu.activeOnNodes = ['ALL']
    dc_htu.ref_HistogramProviderConfig = [ providercfg ]
    dc_htu.ref_HistogramOperationConfig = []
    #
    dcappcfg = DFdal.DCApplicationConfig('DCAppConfig-ROIB')
    dcappcfg.ISThreadResourceCheckInterval = 20
    dcappcfg.ISDefaultRsrcUpdateInterval = 0
    dcappcfg.ISDefaultServer = 'DF'
    dcappcfg.ISlongTimeout = 10
    dcappcfg.refDC_ISResourceUpdate = [ dc_isru ]
    dcappcfg.refDC_HistogramTypeUpdate = [ dc_htu ]
    #
    p2.addObjects([dc_isru, storage, providercfg, dc_htu, dcappcfg])
#
# roib config
#
roib_cfg = DFdal.RoIBConfiguration('ROIBConfig')
roib_cfg.roibTtype = 'TRG_ROD'
roib_cfg.liveChannel = 0xff
roib_cfg.eventrate = 0
roib_cfg.EnableStandalone = False
roib_cfg.EnableFlowControl = True
roib_cfg.MaxInputSkew = 100
roib_cfg.UsesTestFiles = 12 * ['']
#
# RoIB app
#
roib = DFdal.RoIBApplication('ROIB-1')
roib.ActionTimeout = 60
#roib.ShortTimeout = 10
roib.ProbeInterval = 5
roib.FullStatisticsInterval = 60
roib.IfError = 'Error'
roib.Logging = True
roib.InputDevice = ''
roib.InitTimeout = 60
roib.ExitTimeout = 5
#roib.StartAt = 'Boot'
#roib.StopAt = 'Shutdown'
roib.RestartableDuringRun = False
roib.IfExitsUnexpectedly = 'Error'
roib.IfFailsToStart = 'Error'
#roib.DFApplicationConfig = dcappcfg
roib.Program = dal.Binary('roib_main')
try:
    roib.RunsOn = hosts[roib_host]
except KeyError:
    print('roib host ',roib_host,' was not found in the hosts database')
    sys.exit(1)
roib.RoIBConfiguration = roib_cfg
#
# RoIB Segment
#
roib_seg = dal.Segment('ROIBSegment')
roib_seg.Resources = [roibInput, roibOutput]
roib_seg.Applications = [ roib ]
roib_seg.IsControlledBy = p2.getObject('RunControlTemplateApplication','DefRC')
#roib_seg.DefaultHost = hosts['pc-tdq-onl-80']
#roib_seg.DefaultHost = hosts['pc-tbed-pub-10']
#
p2.updateObjects( [roib_cfg] )
p2.updateObjects( [roib] )
p2.updateObjects( [roib_seg] )
# --------------------------
# done with RoIBSegment_LVL1 
# --------------------------
p2.commit()
#
# fixup include path for RoIBInfrastructure_LVL1.data.xml
#
s1 = 'HLT-internal.data.xml'
s2 = 'daq/segments/HLT/HLT-internal.data.xml'
#
f  = open('./RoIBInfrastructure_LVL1.data.xml','r')
li = f.readlines()
f.close()
#
lo = []
for l in li:
    l = l.replace(s1,s2)
    lo.append(l)
#
f  = open('./RoIBInfrastructure_LVL1.data.xml','w')
f.writelines(lo)
f.close()
#
# fixup include path for RoIBSegment_LVL1.data.xml
#
s1 = 'RoIBInfrastructure_LVL1.data.xml'
s2 = 'daq/segments/HLT/RoIBInfrastructure_LVL1.data.xml'
#
f  = open('./RoIBSegment_LVL1.data.xml','r')
li = f.readlines()
f.close()
#
lo = []
for l in li:
    l = l.replace(s1,s2)
    lo.append(l)
#
f  = open('./RoIBSegment_LVL1.data.xml','w')
f.writelines(lo)
f.close()
