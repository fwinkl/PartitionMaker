#!/usr/bin/env tdaq_python
""" pm_list_options.py -- list PM's options

"""
from __future__ import print_function
from pm.option.common import option as opt_common
from pm.option.segcommon import option as opt_segcommon
from pm.option.specific import option as opt_specific
import string

d = {}
for c in string.ascii_letters:
    d[c] = []

for (k,v) in list(opt_common.items()):
    s = v['short']
    if s:
        d[s].append((k,'common'))

for (k,v) in list(opt_segcommon.items()):
    s = v['short']
    if s:
        d[s].append((k,'segcommon'))

for (k,v) in list(opt_specific.items()):
    s = v['short']
    if s:
        d[s].append((k,'specific'))

ks = list(d.keys())
ks.sort()

print("\nList the options known to PartitionMaker")
print("Note: the options of the option parser itself are not listed")
print("      (that's essentially the -D and -F options, plus a few special ones)\n")

print("\nList of options sorted by their 'short' name, if they have any\n")
for s in ks:
    if len(d[s]) > 0:
        print('{:<25s}   -{:s}'.format(d[s][0][0],s))

D = {}
D.update(opt_common)
D.update(opt_segcommon)
D.update(opt_specific)

kl = list(D.keys())
kl.sort()

print("\nList of options sorted by their 'long' name\n")
for l in kl:
    if D[l]['short']:
        print('{:<25s}  -{:s}'.format(l,D[l]['short']))
    else:
        print('{:<25s}'.format(l))
