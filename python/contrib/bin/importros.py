#!/usr/bin/env python
from __future__ import print_function
from builtins import map
from builtins import str
from builtins import object
import re
import string
import optparse
import sys

filename = "data.ros.csv"

parser = optparse.OptionParser()
parser.add_option("-f", "--file", type="string", dest="filename", metavar="FILE",
                  default=filename,
                  help="ros csv data file")

parser.add_option("-g", "--list_global_detectors", action="store_true",
                  dest="list_global_detectors", default=False, 
                  help="display global detector name found")

parser.add_option("-l", "--list_detectors", action="store_true",
                  dest="list_detectors", default=False,
                  help="display detector name found")

parser.add_option("-r", "--list_ros_hostnamess", action="store_true",
                  dest="list_ros_hostnames", default=False,
                  help="display application ip name found")

parser.add_option("-a", "--application", type="string", 
                  dest="application_exp", metavar="REGEX",
                  help="select application ip name regex")

parser.add_option("-d", "--detector", type="string",
                  dest="detector",
                  help="select detector, TRT, TDQ, etc")

(options, args) = parser.parse_args()

fp = open(options.filename)
application_exp = options.application_exp
detector = options.detector


class RosData (object):
  '''
  Base Class to handle ROS data
  '''

  __my_global_detector = None
  __my_application_ip_name = None

  def __init__(self, index, detector_id, global_detector_name, detector_name, ros_name, application, application_ip_name, channel_id, crap_module):
    self.index = index
    self.detector_id = detector_id
    self.global_detector_name = global_detector_name
    self.detector_name = detector_name
    self.ros_name = ros_name
    self.application = application
    self.application_ip_name = application_ip_name
    self.channel_id = channel_id
    self.crap_module = crap_module

  def __my_str_repr(self):
    return ", ".join(
      (str(self.index),
      self.detector_id,
      self.global_detector_name,
      self.detector_name,
      self.ros_name,
      self.application,
      self.application_ip_name,
      self.channel_id,
      self.crap_module, )
    )

  def __repr__(self):
      return self.__my_str_repr()
      #print self.__dict__.keys().__repr__()
      #return self.__dict__.values().__repr__()

  __str__ = __repr__

  def setGlobalDetector(self, global_detector):
    self.__my_global_detector = global_detector
    return True

  def setApplicationIpName(self, application_ip_name):
    self.__my_application_ip_name = application_ip_name
    return True

  def checkData(self):
      if self.__my_global_detector is not None:
        if self.__my_global_detector != self.global_detector_name:
          return False
      if self.__my_application_ip_name is not None:
        r = re.search(self.__my_application_ip_name, self.application_ip_name)
        if r is None:
          return False
      return True

  def hasData(self):
    return self.checkData()

def parseRosData(line):
  expression='^[ \t]{0,}#' ### Check for comments
  m = re.search(expression, line)
  if m is not None:
    return False

  expression='^[ \t]{0,}-' ### Check for hyphen
  m = re.search(expression, line)
  if m is not None:
    return False

  expression='^[ \t]{0,}!' ### check if index needs to be incremented
  m = re.search(expression, line)
  if m is not None:
    return 1

  if ',' in line:
    data = line.split(',')
    data = list(map(string.strip, data))
    return data

  return False

index = 0
ros = {}
detectors=[]
global_detectors=[]
hosts=[]
for line in fp:
  data = parseRosData(line)
  if data==1:
    index += 1
    continue

  if not index:
    index += 1
    continue

  if data:
    rd = RosData(index, *data)
    if rd.application_ip_name not in hosts:
      hosts.append(rd.application_ip_name)
    if rd.detector_name not in detectors:
      detectors.append(rd.detector_name)
    if rd.global_detector_name not in global_detectors:
      global_detectors.append(rd.global_detector_name)
    if detector is not None:
      rd.setGlobalDetector(detector)
    if application_exp is not None:
      rd.setApplicationIpName(application_exp)
    if rd.checkData():
      ros[index] = rd
    index += 1

if options.list_ros_hostnames:
  print('\n'.join(hosts))
  sys.exit()

if options.list_detectors:
  print(detectors)
  sys.exit()

if options.list_global_detectors:
  print(global_detectors)
  sys.exit()

### display ROS data result
for v in list(ros.values()):
  print(v)
