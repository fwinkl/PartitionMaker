#!/usr/bin/env tdaq_python
from __future__ import print_function
import pm.utils


import os, sys
import pm.project

from pm.partition.utils import parse_cmdline, post_process
from pm.segment.ros import option, gendb
from pm.dal import dal, DFdal
from config.dal import module as dal_module

# parse the arguments, etc.
from optparse import OptionParser

usage = "Usage: %prog <daq/segments/ROS/some-ros-db-file>\n"\
      "e.g.\n\t %prog daq/segments/ROS/ROS-TDQ-robin-dc.data.xml"
parser = OptionParser(usage)

(options, args) = parser.parse_args()

try:
  ros_dbfile = args[0]
except IndexError:
  print(parser.print_help())
  sys.exit()

# load the new schema and the segment file
Descr_dal = dal_module('Descr_dal', 'daq/schema/ROSDescriptor.schema.xml', [dal, DFdal])

db = pm.project.Project(ros_dbfile)
all_objs = db.getAllObjects()
dbname = ros_dbfile.rstrip('data.xml')

#if len(segment) == 0:
#  print("ERROR: There doesn't seem to be a 'Segment' object in %s" % ros_dbfile)
#  sys.exit(1)
#elif len(segment) > 1:
#  print("ERROR: There are more than one 'Segment' objects in %s. I am confused." % ros_dbfile)
#  sys.exit(1)


# we will only change the ROSs
roslist = db.getObject('ROS')

emulated_flag = False
for ros in roslist:

  # replace the readout modules
  new_modules = []
  for module in ros.Contains:
    new_module_id = module.id + "-newarch"
    if module.isDalType('RobinReadoutModule'):
      new_module = Descr_dal.RobinDescriptorReadoutModule(new_module_id)
    elif module.isDalType('EmulatedReadoutModule'):
      emulated_flag = True
      new_module = Descr_dal.EmulatedDescriptorModule(new_module_id)
    else:
      print("I was expecting a RobinReadoutModule or EmulatedReadoutModule but I got a '%s'. I am confused." % module.className)
      sys.exit(1)

    new_module.copy(module)
    new_modules.append(new_module)

  ros.Contains = new_modules

  # replace the output
  old_output = ros.Output
  new_output_id = old_output.id + "-newarch"
  if old_output.isDalType('DcDataOut'):
    ros.Output = Descr_dal.DCOutput(new_output_id)
  elif old_output.isDalType('FileDataOut'):
    ros.Output = Descr_dal.FileOutput(new_output_id)
  elif old_output.isDalType('TCPDataOut'):
    ros.Output = Descr_dal.TCPOutput(new_output_id)
  else:
      print("I was expecting a DcDataOut, FileDataOut or TCPDataOut but I got a %s. I am confused." % ros.Output.className)
      sys.exit(1)
  ros.Output.copy(old_output)

  # replace the trigger
  old_trigger = ros.Trigger[0]
  new_trigger_id = old_trigger.id + "-newarch"
  if old_trigger.isDalType('DcTriggerIn'):
    # both emulated-dc and robin-dc use DcTriggerIn
    if emulated_flag:
      ros.Trigger = [Descr_dal.EmulatedTrigger(new_trigger_id)]
    else:
      ros.Trigger = [Descr_dal.DCTrigger(new_trigger_id)]
  elif old_trigger.isDalType('RobinDataDrivenTriggerIn'):
    ros.Trigger = [Descr_dal.RobinDataDrivenTrigger(new_trigger_id)]
  else:
      print("I was expecting a DcDataOut, FileDataOut or TCPDataOut but I got a %s. I am confused." % ros.Output.className)
      sys.exit(1)
  ros.Trigger[0].copy(old_trigger)


# see if we need to add the ROSDescriptor include
descr_classes = [
    'RobinDescriptorReadoutModule',
    'DCOutput',
    'DCTrigger',
    ]

newdb = pm.project.Project(dbname+"-newarch.data.xml", db.getIncludes())

newdb_classes = newdb.classes();
if any([c not in newdb_classes for c in descr_classes]):
  newdb.addInclude('daq/schema/ROSDescriptor.schema.xml')

newdb.addObjects(all_objs)
