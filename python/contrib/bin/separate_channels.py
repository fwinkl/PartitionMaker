#!/usr/bin/env tdaq_python
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 21 Aug 2007 12:14:52 PM CEST 
import pm.utils


import os, sys
import pm.project

from pm.partition.utils import parse_cmdline, post_process
from pm.segment.ros import option, gendb

db = pm.project.Project(sys.argv[1])
dbname = sys.argv[1].rstrip('data.xml')

db_objs = db.getAllObjects()
db_includes = db.getIncludes()
channels = db.getObject('InputChannel')
channels_filename = str.join('-', dbname.split('-')[:-1]) + '-channels.data.xml'

channels_db = pm.project.Project(channels_filename, db_includes)
channels_db.addObjects(channels)

# we can't directly remove the objects from the segment db, since it would leave
# dangling references. we also can't directly add the include file because of
# namespace collisions. so, just create it again with the new include file
db_includes.append(channels_filename)
db = pm.project.Project(sys.argv[1], db_includes)
db.addObjects(db_objs)


