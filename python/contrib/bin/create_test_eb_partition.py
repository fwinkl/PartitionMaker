#!/usr/bin/env tdaq_python

from __future__ import print_function
from builtins import filter
from builtins import str
import pm.project

import pm.common
import pm.farm
import pm.multinode
from pm.dal import dal,DCMdal
from pm.partition.utils import parse_cmdline, post_process
import os, sys
from pm.project import Project
from itertools import chain

includes = list(pm.multinode.includes)

###############################################
## PARAMETERS #################################

if os.getenv('ROS_HOSTFILE'):
  hostfile = os.getenv('ROS_HOSTFILE').strip()
else:
  hostfile = "daq/hw/hosts-preseries.data.xml"
includes.append(hostfile)
hosts = pm.farm.load(hostfile)
if os.getenv('ROS_DEFAULT_DATA_NETWORKS'):
  default_data_networks = os.getenv('ROS_DEFAULT_DATA_NETWORKS').strip().split(',')
else:
  default_data_networks = []

if os.getenv('TEST_HLT_HLTSV'):
  hltsv = hosts[os.getenv('TEST_HLT_HLTSV')]
else:
  hltsv = hosts['pc-preseries-dfm-02.cern.ch']

if os.getenv('TEST_HLT_DCMS'):
  dcmlist = os.getenv('TEST_HLT_DCMS').strip().split(',')
else:
  dcmlist = "pc-preseries-sfi-[01-04].cern.ch"
dcm = list(chain(*[list(pm.farm.subselect_pattern(hosts, el).values()) for el in dcmlist]))
assert dcm, "No DCMs defined for HLT segment. Perhaps you are not including the correct hosts db?"

###############################################
###############################################
ROS_IS_STRING = False
if '--xml' in sys.argv:
  ROS_IS_STRING = True
  sys.argv.remove('--xml')

try:
  ros_segment_dbfile = sys.argv.pop(1)
except IndexError:
  print("Usage: %s [--xml] <daq/segments/ROS/ros_segment.data.xml> \n"
  "--xml : \n\t Use this switch to add the ROS segment directly as a string in the XML file."
  "\n\t This will add the given path to the list of includes and the first part of the filename"
  "\n\t (until the first '.') to Partition.Segments"
  "\n\t (Remember to check the partition with OKS later!)\n" % sys.argv[0])
  sys.exit(1)

from pm.partition.hlt import option
kwargs = parse_cmdline("", option)


exec('from %s import hlt_farm' % kwargs['hlt-farm'])
includes = pm.utils.merge_includes(includes, kwargs['hlt-farm'], 'includes')
exec('from %s import ros_farm' % kwargs['ros-farm'])
includes = pm.utils.merge_includes(includes, kwargs['ros-farm'], 'includes')

#add ROS segment (as object)
if ROS_IS_STRING:
  part_name = 'part-' + '-'.join(ros_segment_dbfile.split('-')[1:])

if not ROS_IS_STRING:
  ros_db = Project(ros_segment_dbfile)
  includes = pm.utils.merge_unique(includes, ros_segment_dbfile)
  objcache = pm.common.load(includes)

  # get either the first segment, or any segment containing subsegments
  if ros_db.getObject("Segment"):
    ros_segment = ros_db.getObject("Segment")[0]
  else:
    # generate new segment
    ros_segment = dal.Segment(ros_segment_dbfile.split('/')[-1].split('.')[0])
    ros_segment.Hosts.insert(0,hltsv)

    ros_segment.IsControlledBy = objcache['DefRC']
    ros_segment.Infrastructure = [objcache['DefRDB']]
    roslist = ros_db.getObject("ROS")
    ros_segment.Resources = sorted(roslist, key = lambda ros: ros.RunsOn.id)
  for s in ros_db.getObject("Segment"):
    if len(s.Segments):
      ros_segment = s
      break

  part_name = 'part-' + '-'.join(ros_segment.id.split('-')[1:])

hlt_farm['hltsv'] = [hltsv]
dummyrack = dal.Rack('dummyRack')
dummyrack.Nodes = [hltsv]+dcm
dummyrack.LFS = [hltsv]
hlt_farm['hlt_farm'] = [pm.multinode.hlt_subfarm([dummyrack])]

part = pm.multinode.part_hlt(part_name, 
    hlt_farm, ros_farm,
    setup=Project(kwargs['setup-file']),
    template=Project(kwargs['template-file']),
    datafiles=[], output=[])

# not necessary
part.DefaultHost = ros_segment.Hosts[0]
part.LogRoot = os.getenv('PARTITION_LOG_ROOT_OVERRIDE')
if not part.LogRoot:
  part.LogRoot = "/tmp/logs/"

for s in part.Segments:
  if s.id.startswith('ROS'):
    part.Segments.remove(s)

if not ROS_IS_STRING:
  part.Segments.append(ros_segment)
# remove any previous version
pfile = part.id + '.data.xml'
try:
    os.remove(pfile)
    print('previous version of',pfile,' removed')
except:
    pass

# Fix DCM configuration (no output)
fileout = DCMdal.DcmFileOutput('DcmFileOut')
fileout.storageAcceptance = 0
part.get('DcmApplication')[0].output = fileout

#Remove SFO
hltseg = part.get('Segment','HLT')
hltseg.Resources = [r for r in hltseg.Resources if not 'SFO' in r.id]

# Use only DCM info from "DF.TopMIG-IS:HLT.info"
# fix L1 counters
infsources = part.get("IS_InformationSources")[0]
infsources.LVL1.EventCounter = "DF.TopMIG-IS:HLT.info.L1SourceDoneEvents"
infsources.LVL1.Rate = "DF.TopMIG-IS:HLT.info.L1Rate"
# fix HLT counters
infsources.HLT.EventCounter = "DF.TopMIG-IS:HLT.info.ProxAccEvents"
infsources.HLT.Rate = "DF.TopMIG-IS:HLT.info.OutRate"

p = post_process(kwargs, part)

includes = pm.utils.merge_unique(includes, [kwargs['setup-file'], \
    kwargs['template-file']])
if len(kwargs['extra-includes']) != 0:
  includes = pm.utils.merge_unique(includes, kwargs['extra-includes'])

p = pm.project.Project(pfile, includes)

mt = dal.MasterTrigger("HLTSVMaster")
mt.Controller = part.get("HLTSVApplication")[0]

part.get("DFParameters")[0].DefaultDataNetworks = default_data_networks

part.MasterTrigger = mt

# Define TriggerConfiguration and link to Partition.
opt =  { 'implementation':'pudummy' }
tc = pm.common.create_trigger_config(opt)
part.TriggerConfiguration = tc

part.DefaultTags.append(part.get('Tag','x86_64-centos7-gcc8-opt'))

p.addObjects([part])

if ROS_IS_STRING:
# hacking at the XML level from now on
# NOT a very good idea, don't use this for anything serious

  from xml.etree import ElementTree as ET

  class PIParser(ET.XMLTreeBuilder):

    """Wrapper oover elementtree that preserves xml comments (needed by OKS)
    Created by Fredrik Lundh @effbot.org"""

    def __init__(self):
      ET.XMLTreeBuilder.__init__(self)
      # assumes ElementTree 1.2.X
      self._parser.CommentHandler = self.handle_comment
      self._parser.ProcessingInstructionHandler = self.handle_pi
      self._target.start("document", {})

    def close(self):
      self._target.end("document")
      return ET.XMLTreeBuilder.close(self)

    def handle_comment(self, data):
      self._target.start(ET.Comment, {})
      self._target.data(data)
      self._target.end(ET.Comment)

    def handle_pi(self, target, data):
      self._target.start(ET.PI, {})
      self._target.data(target + " " + data)
      self._target.end(ET.PI)

  # OKS helpers
  def oks_split_header(filename):
    with open(filename, 'r') as f:
      s = f.read()
      oks_data_tag_index = s.find('<oks-data>')
      return s[:oks_data_tag_index], s[oks_data_tag_index:]

  def oks_write(newdoc, filename):
    # add whitespace to empty tags; this will prevent self-closing tags,
    # (e.g., "<rel ... />")which OKS doesn't recognize
    for el in newdoc.getiterator():
      # on the other hand, it expects a few tags to be self-closing
      if not el.text and el.tag not in ('comment', 'info', 'file'):
        el.text = ' '
    # file will exist, it was generated by PM at this points
    header, _ = oks_split_header(filename)
    newdoc.write(filename)
    _, body = oks_split_header(filename)
    closing_tag_index = body.find('</document>')
    with open(filename, 'w') as out:
      out.write(header+body[:closing_tag_index])

  def parse(source):
    return ET.parse(source, PIParser())

  doc = parse(pfile)
  # add include  for ROS segment
  # using the hacked elementtree, find won't work :\
  # inc_element = doc.find('include')
  inc_el = filter(lambda x: x.tag=='include', doc.getiterator())[0]
  from copy import deepcopy
  ros_file_el = deepcopy(inc_el.getchildren()[0])
  ros_file_el.set('path', ros_segment_dbfile)
  inc_el.insert(-1, ros_file_el)

  # add ROS segment to Partition.Segments
  ros_segment_name = ros_segment_dbfile.split('/')[-1].split('.')[0]
  part_el = filter(lambda x: x.get('class')=='Partition', doc.getiterator())[0]
  segment_el = filter(lambda x: x.tag=='rel' and x.get('name')=='Segments', part_el.getiterator())[0]
  segment_el.text = segment_el.text +(' "Segment" "%s"\n' % ros_segment_name)
  segment_el.set('num', str(int(segment_el.get('num'))+1))
  #ipshell()
  oks_write(doc, pfile)
