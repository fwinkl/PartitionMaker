#!/usr/bin/env tdaq_python

from __future__ import print_function
import pm.project

import pm.common
import pm.farm
import pm.multinode
from pm.dal import dal, DFdal, dal_module
from pm.partition.utils import parse_cmdline, post_process
import os, sys
from pm.project import Project
from itertools import chain

includes = list(pm.multinode.includes)
includes = pm.utils.merge_unique(includes, ['daq/segments/setup.data.xml'])

###############################################
## PARAMETERS #################################
if os.getenv('ROS_HOSTFILE'):
  hostfile = os.getenv('ROS_HOSTFILE').strip()
else:
  hostfile = "daq/hw/hosts-preseries.data.xml"
includes.append(hostfile)
hosts = pm.farm.load(hostfile)

assert os.getenv('ROS_DEFAULT_DATA_NETWORKS'), """ROS_DEFAULT_DATA_NETWORKS must be set for TestDC!
e.g., for preseries do:
  export ROS_DEFAULT_DATA_NETWORKS=10.149.96.0/255.255.255.0,10.149.64.0/255.255.255.0"""
default_data_networks = os.getenv('ROS_DEFAULT_DATA_NETWORKS').strip().split(',')

if os.getenv('TESTDC_MASTER_PARAMETERS'):
  master_parameters = os.getenv('TESTDC_MASTER_PARAMETERS').strip()
else:
  master_parameters = ""

if os.getenv('TESTDC_SLAVE_PARAMETERS'):
  slave_parameters = os.getenv('TESTDC_SLAVE_PARAMETERS').strip()
else:
  slave_parameters = ""

if os.getenv('TESTDC_ROI_DISTRIBUTION'):
  roi_distr_strlist = os.getenv('TESTDC_ROI_DISTRIBUTION').strip().split(",")
  roi_distr = [int(s) for s in roi_distr_strlist]
else:
  roi_distr = [0,0,100]

if os.getenv('TESTDC_TESTING_HOSTS'):
  testinglist = os.getenv('TESTDC_TESTING_HOSTS').strip().split(',')
else:
  print("Warning: TESTDC_TESTING_HOSTS looks undefined, I'll try to use default hosts...")
  testinglist = "pc-preseries-ros-1[0-1].cern.ch"

tdchosts = chain(*[list(pm.farm.subselect_pattern(hosts, el).values()) for el in testinglist])
tdchosts = sorted(tdchosts, key = lambda computer: computer.id)
assert tdchosts, """No hosts defined for the TestDC segment. This means:
a) You specified the wrong hosts database in ROS_HOSTFILE
b) You left TESTDC_TESTING_HOSTS undefined or you asked for hosts not in ROS_HOSTFILE
"""

def_controller_hostname = ''
if os.getenv('DEFAULT_CONTROLLER_HOST'):
  def_controller_hostname = os.getenv('DEFAULT_CONTROLLER_HOST')
else:
  print("Warning: I can't find $DEFAULT_CONTROLLER_HOST in the hosts DB, using fallback hosts...")
  if 'pc-tdq-ros-spare-01.cern.ch' in objcache:
    def_controller_hostname = 'pc-tdq-ros-spare-01.cern.ch'
  elif 'pc-preseries-dfm-01.cern.ch' in objcache:
    def_controller_hostname = 'pc-preseries-dfm-01.cern.ch'
  else:
    raise Exception("$DEFAULT_CONTROLLER_HOST is unset and the fallbacks are not defined in the hosts DB!")



###############################################
###############################################

try:
  ros_segment_dbfile = sys.argv.pop(1)
except IndexError:
  print("Usage: %s <daq/segments/ROS/ros_segment.data.xml> \n" % sys.argv[0])
  sys.exit(1)

# Part 1 Get ROS segment

ros_db = Project(ros_segment_dbfile)
includes = pm.utils.merge_unique(includes, ros_segment_dbfile)
objcache = pm.common.load(includes)

ros = ros_db.getObject("ROS")[0]
# generate new segment
ros_segment = dal.Segment(ros_segment_dbfile.split('/')[-1].split('.')[0] + "-GeneratedForTestDC")
# include only the first ROS
ros_segment.Resources = [ros]
ros_segment.IsControlledBy = objcache['DefRC']
ros_segment.Hosts.insert(0,objcache[def_controller_hostname])

# Part 2 Create the TestDC segment
testdc_segment = dal.Segment('ROSTestDC-Segment')
testdc_segment.IsControlledBy = dal.RunControlApplication('ROSTestDCController')
testdc_segment.IsControlledBy.RunsOn = objcache[def_controller_hostname]
testdc_segment.IsControlledBy.Program = objcache['rc_controller']
proc_env = dal.Variable('TDAQ_PARTITION')
proc_env.Name = 'TDAQ_PARTITION'
proc_env.Value = '${TDAQ_PARTITION}'
testdc_segment.ProcessEnvironment = [proc_env]
testdc_segment.Infrastructure = []

for tdcindex,tdchost in enumerate(tdchosts):
  
  ROSTesterdal = dal_module('ROSTesterdal', 'daq/schema/ROSTester.schema.xml', 
                            [DFdal, dal])

  tdc = ROSTesterdal.ROSTester('ROSTester-%d'%tdcindex)

  if tdcindex==0:
    tdc.Parameters = master_parameters + " -T %d" % \
        (len(tdchosts))
  else:
    tdc.Parameters = slave_parameters + " -T %d -I %d" % \
        (len(tdchosts), tdcindex)

  tdc.IfExitsUnexpectedly = tdc.IfFailsToStart = 'Error'
  tdc.ROIDistribution = roi_distr
  tdc.RunsOn = tdchost
  tdc.ConnectsToROS = ros
  tdc.Program = objcache['ROSTester']
  tdc.L2RequestFraction = 0
  tdc.EBRequestFraction = 100
  tdc.SyncDeletesWithEBRequests = 1
  tdc.Outstanding = 100
  tdc.StartDelay = 5000000
  tdc.InterfaceName = "rc/commander" 
  testdc_segment.Applications.append(tdc)

# Part 3 Create Partition

part_name = 'part-' + '-'.join(ros_segment.id.split('-')[1:2]) + '-ROSTester-' + str(len(tdchosts))
pfile = part_name + '.data.xml'

part = dal.Partition(part_name)
part.RepositoryRoot = ""
part.IPCRef = "$(TDAQ_IPC_INIT_REF)"
part.DBPath = "$(TDAQ_DB_PATH)"
part.DBName = "$(TDAQ_DB_DATA)"
part.DBTechnology = "rdbconfig"
part.LogRoot = os.getenv('PARTITION_LOG_ROOT_OVERRIDE')
if not part.LogRoot:
  part.LogRoot = "/tmp/logs/"
part.RunTypes = ['Physics']
part.Segments = [ros_segment, testdc_segment]
part.OnlineInfrastructure = objcache['setup']
part.DefaultTags.append(part.get('Tag','x86_64-centos7-gcc8-opt'))
part.ProcessEnvironment = [objcache['CommonEnvironment'], objcache['External-environment']]

part.DefaultHost = objcache[def_controller_hostname]

part.Parameters = [objcache['CommonParameters']]
part.DataFlowParameters = DFdal.DFParameters('DFParameters-1')
part.DataFlowParameters.Name = "DFParameters-1"
part.DataFlowParameters.DefaultDataNetworks = default_data_networks

part.IS_InformationSource = dal.IS_InformationSources('DF_ISInfo')
part.IS_InformationSource.LVL1 = dal.IS_EventsAndRates('L1Counters')
part.IS_InformationSource.LVL1.EventCounter = "DF.ROS."+ros.id+".DataChannel0.fragsreceived"
part.IS_InformationSource.LVL1.Rate = "DF.ROS."+ros.id+".DataChannel0.level1RateHz"


try:
    os.remove(pfile)
    print('previous version of',pfile,' removed')
except:
    pass

p = pm.project.Project(pfile, includes+['daq/schema/ROSTester.schema.xml'])

p.addObjects([part])
