#!/usr/bin/env tdaq_python
from __future__ import print_function
import sys
import pm.common
from pm.dal import dal, DFdal
from pm.project import Project

import pm.utils

from create_test_reb_partition import gen_reb_partition

if __name__=='__main__':
  from optparse import OptionParser
  usage = "Usage: %prog <daq/segments/ROS/some-ros-db-file> \n"\
        "\ne.g.\n\t %prog daq/segments/ROS/ROS-PIX-robin-datadriven-edo.data.xml"
  parser = OptionParser(usage)

  (options, args) = parser.parse_args()

  try:
    ros_dbfile = args[0]
  except IndexError:
    print(parser.print_help())
    sys.exit()

  includes = [ 'daq/segments/setup.data.xml' ]
  includes += [ros_dbfile]

  partition = gen_reb_partition(ros_dbfile, None, includes)
  partition.id = partition.id.replace('-reb', '-edo')
  partition.DefaultTags.append(partition.get('Tag','x86_64-centos7-gcc8-opt'))

  # save
  import os
  pfile = partition.id + ".data.xml"
  try:
      os.remove(pfile)
      print('previous version of',pfile,'removed')
  except:
      pass
  db = Project(pfile, includes)
  db.addObjects([partition])
