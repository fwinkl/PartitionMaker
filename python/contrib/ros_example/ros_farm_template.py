#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :

from builtins import range
__author__ = "Giorgos Boutsioukis <georgios.boutsioukis@cern.ch>"
import pm.farm
from eformat import helper


# This is a farm template provided as an example of the basic structure, since
# point1 farms are generated from a CSV file. New farms should follow this
# example instead of the CSV way.

# Checklist to create a new farm
#########################################################################

# *1 <---------- Change hosts
# hosts db as returned by pm_farm.py
hosts = pm.farm.load('daq/hw/hosts-preseries.data.xml', short_names=True)

# *2 <---------- Change includes
includes = [\
    'daq/hw/hosts-preseries.data.xml',
    'daq/segments/ROS/ros-common-config.data.xml',
    'daq/segments/ROS/ros-specific-config-PIX.data.xml',]

ros_objects = pm.common.load(includes)

# *3 <---------- Insert the ROS farm definition
# provide your own(see README for structure) or use csv2farm.py
#
#ros_farm = {
#    'name' : 'PRESERIES-PIX',
#         ................
#}

# *4 <---------- Change these objects for your farm
ros_farm['ros_config'] = ros_objects['ROS-Config-PIX']
ros_farm['robin_channel_config'] = ros_objects['RobinDataChannel-Config-PIX'] 
ros_farm['robin_mem_pool'] = ros_objects['ROS-MemoryPool-Data-PIX']
ros_farm['robin_config'] = ros_objects['Robin-Config-PIX']

###############################################################################
# The rest can usually be left unchanged ######################################
###############################################################################

# PM won't pass parameters to the farms;
# cheat and parse the command line directly
# beware of this when modifying the arguments!
import sys

ros_mode = None
for i in range(len(sys.argv)):
  if sys.argv[i] == '--ros-mode': ros_mode = sys.argv[i+1]

assert ros_mode, "No --ros-mode? (needed by the ros_farm file!)"

ros_farm['ros_common_config'] = ['daq/segments/ROS/ros-common-config.data.xml']

ros_farm['ros_override_params'] = { \
    'ActionTimeout':10,
    'ProbeInterval':5,
    'FullStatisticsInterval':60, 
    'Parameters':'-a 3',
    'RestartParameters':'-a 3',
    'InitTimeout':60,
    'RestartableDuringRun':1,
    }
# use spare for testing

ros_farm['reb']['tcp_data_out_config_override_params'] = { \
    'SamplingGap':1,
    'TCPBufferSize':32768,
    'ThrowIfUnavailable':0
    }

# REB segment parameters
ros_farm['reb']['rcapp_override_params'] = { \
    'ActionTimeout':60,
    'ProbeInterval':5,
    'FullStatisticsInterval':60,
    'IfExitsUnexpectedly':'Restart',
    }
ros_farm['reb']['rcd_override_params'] = {
    'ProbeInterval':5,
    'FullStatisticsInterval':60,
    'Id':85, # (?) this was hardcoded in the perl template
    }
ros_farm['reb']['rcd_config_override_params'] = {
    'NumberOfRequestHandlers':1,
    }

ros_farm['reb']['input_fragment_type'] = 'ROSFragment'

if ros_mode.endswith('-reb'):
  # REB mode only
  ros_farm['ros_override_params']['ReceiveMulticast'] = 0


