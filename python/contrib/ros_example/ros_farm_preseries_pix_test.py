#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :

__author__ = "Giorgos Boutsioukis <georgios.boutsioukis@cern.ch>"

# This is a minimal farm provided as an example of the basic structure, since
# point1 farms are generated from a CSV file. New farms should follow this
# example instead of the CSV way.

import pm.farm
from eformat import helper

# hosts db as returned by pm_farm.py

includes = ['daq/hw/hosts-preseries.data.xml',
    'daq/segments/ROS/ros-common-config.data.xml',
   'daq/segments/ROS/ros-specific-config-PIX.data.xml',]
hosts = pm.farm.load('daq/hw/hosts-preseries.data.xml', short_names=True)
ros_objects = pm.common.load(includes)


ros_farm = {
    'name' : 'PRESERIES-PIX',
    helper.SubDetector.PIXEL_BARREL: {
      'ros': [
        (
          hosts['pc-preseries-ros-00'],
          {
            'index': 0x00000001,
            'modules': [
              ( 0x00000001, [ 0x00001708, 0x00001710, 0x00002406, ],),
              ( 0x00000002, [ 0x00001712, 0x00001715, 0x00001810, ],),
              ( 0x00000004, [ 0x00001812, 0x00002515, 0x00002517, ],),
              ( 0x00000005, [ 0x00002519, 0x00002521, ],),
              ],
            },
          ),
        (
          hosts['pc-preseries-ros-01'],
          {
            'index': 0x00000002,
            'modules': [
              ( 0x00000001, [ 0x00001707, 0x00001709, 0x00001711, ],),
              ( 0x00000002, [ 0x00001714, 0x00001809, 0x00001811, ],),
              ( 0x00000004, [ 0x00002405, 0x00002514, 0x00002516, ],),
              ( 0x00000005, [ 0x00002518, 0x00002520, ],),
              ],
            },
          ),
        (
          hosts['pc-preseries-ros-02'],
          {
            'index': 0x00000003,
            'modules': [
              ( 0x00000001, [ 0x00001719, 0x00001721, 0x00001806, ],),
              ( 0x00000002, [ 0x00001808, 0x00001819, 0x00001821, ],),
              ( 0x00000004, [ 0x00002419, 0x00002421, 0x00002512, ],),
              ( 0x00000005, [ 0x00001706, ],),
              ],
            },
          ),
        (
          hosts['pc-preseries-ros-03'],
          {
            'index': 0x00000004,
            'modules': [
              ( 0x00000001, [ 0x00001705, 0x00001805, 0x00001807, ],),
              ( 0x00000002, [ 0x00001818, 0x00001820, 0x00002418, ],),
              ( 0x00000004, [ 0x00001718, 0x00002420, 0x00002511, ],),
              ( 0x00000005, [ 0x00001720, ],),
              ],
            },
          ),
        (
          hosts['pc-preseries-ros-04'],
          {
            'index': 0x00000005,
            'modules': [
              ( 0x00000001, [ 0x00002505, 0x00002507, 0x00002509, ],),
              ( 0x00000002, [ 0x00001717, 0x00002408, 0x00002410, ],),
              ( 0x00000004, [ 0x00002412, 0x00002415, 0x00002417, ],),
              ( 0x00000005, [ 0x00001815, 0x00001817, ],),
              ],
            },
          ),
        (
            hosts['pc-preseries-ros-05'],
            {
              'index': 0x00000006,
              'modules': [
                ( 0x00000001, [ 0x00002506, 0x00002508, 0x00002510, ],),
                ( 0x00000002, [ 0x00001716, 0x00001814, 0x00001816, ],),
                ( 0x00000004, [ 0x00002407, 0x00002409, 0x00002411, ],),
                ( 0x00000005, [ 0x00002414, 0x00002416, ],),
                ],
              },
            ),
        ],
    'name': 'PIX-B',
  },
  helper.SubDetector.PIXEL_DISK_SIDE: {
      'ros': [
        (
          hosts['pc-preseries-ros-06'],
          {
            'index': 0x00000007,
            'modules': [
              ( 0x00000001, [ 0x00000205, 0x00000207, 0x00000209, ],),
              ( 0x00000002, [ 0x00000211, 0x00000214, 0x00000216, ],),
              ( 0x00000004, [ 0x00000218, 0x00000220, 0x00001609, ],),
              ( 0x00000005, [ 0x00001611, 0x00001614, 0x00001616, ],),
              ],
            },
          ),
        (
          hosts['pc-preseries-ros-07'],
          {
            'index': 0x00000008,
            'modules': [
              ( 0x00000001, [ 0x00000206, 0x00000208, 0x00000210, ],),
              ( 0x00000002, [ 0x00000212, 0x00000215, 0x00000217, ],),
              ( 0x00000004, [ 0x00000219, 0x00000221, 0x00001610, ],),
              ( 0x00000005, [ 0x00001612, 0x00001615, 0x00001617, ],),
              ],
            },
          ),
        ],
      'name': 'PIX-DISK',
      },
  helper.SubDetector.PIXEL_B_LAYER: {
      'ros': [
        (
          hosts['pc-preseries-ros-08'],
          {
            'index': 0x00000009,
            'modules': [
              ( 0x00000001, [ 0x00000007, 0x00000009, 0x00000010, ],),
              ( 0x00000002, [ 0x00000011, 0x00000012, 0x00000014, ],),
              ( 0x00000004, [ 0x00000015, 0x00000016, 0x00000017, ],),
              ( 0x00000005, [ 0x00000018, 0x00000019, ],),
              ],
            },
          ),
        (
          hosts['pc-preseries-ros-09'],
          {
            'index': 0x0000000a,
            'modules': [
              ( 0x00000001, [ 0x00000312, 0x00000314, 0x00000315, ],),
              ( 0x00000002, [ 0x00000107, 0x00000316, 0x00000317, ],),
              ( 0x00000004, [ 0x00000108, 0x00000109, 0x00000110, ],),
              ( 0x00000005, [ 0x00000111, 0x00000112, ],),
              ],
            },
          ),
        (
          hosts['pc-preseries-ros-10'],
          {
            'index': 0x0000000b,
            'modules': [
              ( 0x00000001, [ 0x00000005, 0x00000006, 0x00000008, ],),
              ( 0x00000002, [ 0x00000114, 0x00000115, 0x00000116, ],),
              ( 0x00000004, [ 0x00000117, 0x00000118, 0x00000119, ],),
              ( 0x00000005, [ 0x00000120, 0x00000121, ],),
              ],
            },
          ),
        (
          hosts['pc-preseries-ros-11'],
          {
            'index': 0x0000000c,
            'modules': [
              ( 0x00000001, [ 0x00000307, 0x00000308, 0x00000309, ],),
              ( 0x00000002, [ 0x00000020, 0x00000310, 0x00000311, ],),
              ( 0x00000004, [ 0x00000021, 0x00000105, 0x00000106, ],),
              ( 0x00000005, [ 0x00000318, 0x00000319, ],),
              ],
            },
          ),
        ],
      'name': 'PIX-BL',
  },
}

# Needed for robin-* modes
ros_farm['ros_common_config'] = ['daq/segments/ROS/ros-common-config.data.xml']

ros_farm['ros_config'] = ros_objects['ROS-Config-PIX']
ros_farm['robin_channel_config'] = ros_objects['RobinDataChannel-Config-PIX'] 
ros_farm['robin_mem_pool'] = ros_objects['ROS-MemoryPool-Data-PIX']
ros_farm['robin_config'] = ros_objects['Robin-Config-PIX']

ros_farm['ros_override_params'] = { \
    'ActionTimeout':10,
    'ProbeInterval':5,
    'FullStatisticsInterval':60, 
    'Parameters':'-a 3',
    'RestartParameters':'-a 3',
    'InitTimeout':60,
    'RestartableDuringRun':1,
    }


#REB mode

ros_farm['reb'] = {}
# use spare for testing

ros_farm['reb']['tcp_data_out_config_override_params'] = { \
    'SamplingGap':1,
    'TCPBufferSize':32768,
    'ThrowIfUnavailable':0
    }
ros_farm['ros_override_params']['ReceiveMulticast'] = 0

# REB segment parameters
ros_farm['reb']['rcapp_override_params'] = { \
    'ActionTimeout':60,
    'ProbeInterval':5,
    'FullStatisticsInterval':60,
    'IfExitsUnexpectedly':'Restart',
    }
ros_farm['reb']['rcd_override_params'] = {
    'ProbeInterval':5,
    'FullStatisticsInterval':60,
    'Id':85, # (?) this was hardcoded in the perl template
    }
ros_farm['reb']['rcd_config_override_params'] = {
    'NumberOfRequestHandlers':1,
    }

ros_farm['reb']['input_fragment_type'] = 'ROSFragment'

