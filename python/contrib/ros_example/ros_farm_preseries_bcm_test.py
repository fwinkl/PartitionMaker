#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :

__author__ = "Giorgos Boutsioukis <georgios.boutsioukis@cern.ch>"

# This is a minimal farm provided as an example of the basic structure, since
# point1 farms are generated from a CSV file. New farms should follow this
# example instead of the CSV way.

import pm.farm
from eformat import helper

# hosts db as returned by pm_farm.py

includes = ['daq/hw/hosts-preseries.data.xml',
    'daq/segments/ROS/ros-common-config.data.xml',
   'daq/segments/ROS/ros-specific-config-BCM.data.xml',]

hosts = pm.farm.load('daq/hw/hosts-preseries.data.xml', short_names=True)
ros_objects = pm.common.load(includes)


ros_farm = {
    'name' : 'PRESERIES-BCM',
    helper.SubDetector.FORWARD_BCM: {
      'ros': [
        (
          hosts['pc-preseries-ros-00'],
          {
            'index': 0x00000001,
            'modules': [
              ( 0x00000001, [ 0x0000000A, 0x0000000C, 0x0000000F, ],),
              ],
            },
          ),
        ],
    'name': 'FWD-BCM',
  },
}

# Needed for robin-* modes
ros_farm['ros_common_config'] = ['daq/segments/ROS/ros-common-config.data.xml']

ros_farm['ros_config'] = ros_objects['ROS-Config-BCM']
ros_farm['robin_channel_config'] = ros_objects['RobinDataChannel-Config-BCM'] 
ros_farm['robin_mem_pool'] = ros_objects['ROS-MemoryPool-Data-BCM']
ros_farm['robin_config'] = ros_objects['Robin-Config-BCM']


ros_farm['ros_override_params'] = { \
    'ActionTimeout':10,
    'ProbeInterval':5,
    'FullStatisticsInterval':60, 
    'Parameters':'-a 3',
    'RestartParameters':'-a 3',
    'InitTimeout':60,
    'RestartableDuringRun':1,
    }


#REB mode

ros_farm['reb'] = {}

ros_farm['reb']['tcp_data_out_config_override_params'] = { \
    'SamplingGap':1,
    'TCPBufferSize':32768,
    'ThrowIfUnavailable':0
    }
ros_farm['ros_override_params']['ReceiveMulticast'] = 0

# REB segment parameters
ros_farm['reb']['rcapp_override_params'] = { \
    'ActionTimeout':60,
    'ProbeInterval':5,
    'FullStatisticsInterval':60,
    'IfExitsUnexpectedly':'Restart',
    }
ros_farm['reb']['rcd_override_params'] = {
    'ProbeInterval':5,
    'FullStatisticsInterval':60,
    'Id':85, # (?) this was hardcoded in the perl template
    }
ros_farm['reb']['rcd_config_override_params'] = {
    'NumberOfRequestHandlers':1,
    }

ros_farm['reb']['input_fragment_type'] = 'ROSFragment'
