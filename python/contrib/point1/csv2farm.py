#!/usr/bin/env tdaq_python

"""
This is a script to generate PartitionMaker ros farms from the
old CSV format used by the DBGeneration scripts.
It assumes the presence of the following columns:

DETECTOR_ID: subdetector id
APPLICATION_IP_NAME: ROS hostname
CHANNEL_ID: channel #

"""
from __future__ import print_function

from builtins import zip
from builtins import filter
from builtins import str
from builtins import next
from builtins import object
import sys
import csv
from eformat import helper
import pm
from pm.dal import dal

APPLICATION_IP_DOMAIN = ".cern.ch"

class Header(object):
    def __init__(self, tag, level="", hex=False):
        self.tag = tag
        self.level = level
        self.hex = hex
    def __repr__(self):
        return "tag:"+self.tag+" level:"+self.level+" hex:"+str(self.hex)

    def parseValue(self, c):
        if self.tag.upper().endswith('_ID') or \
            self.level=='Module': # covers all numeric
            if self.hex: return int(c, 16)
            else: return int(c)
        else: return c.strip()


def get_subdetector_by_id(id):
    if id in list(helper.SubDetector.values.keys()):
        return helper.SubDetector.values[id]
    else:
        return helper.SubDetector.OTHER


def get_ros_host_by_hostname(hostname, ros_hosts):
    if hostname in list(ros_hosts.keys()):
        return ros_hosts[hostname]
    else:
        print("Warning: Host %s not in hostlist, added bogus properties" % hostname)
        return dal.Computer(hostname)


#farm format:
"""
    farm = {
     'default_host': dal.Computer|*,
     'name': string|*
     subdetector_id: {
                      'default_host': dal.Computer|*,
                      'name': string|*,
                      'ros': [
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                             ]
                     },
     subdetector_id: {
                      'default_host': dal.Computer|*,
                      'ros': [
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                             ]
                     },
      ...
    }
new format:
      'ros': [
              (ros_host, [(module, [rob_list], ...]),
              (ros_host, [rob_list]),
              (ros_host, [rob_list]),
             ]
newer format:
               (ros_host, { 'modules': [(<module_id>, [rob_list]),
                                        (<module_id>, ...])
                                        ...
                                       ],
                            'index': <ros_global_index> }

"""
def parse_row(row):
    row_dict = {}
    for t in zip(row, headers):
        c, h = t
        if h.level:
            #prefer to use levels as keys, as var names might be arbitrary
            row_dict[h.level] = h.parseValue(c)
        else: row_dict[h.tag] = h.parseValue(c)
    return row_dict

# the purpose of keeping a 'global index' for each ROS, is to
# ensure that all ROSs in a datafile will be assigned a different
# index (useful for setting ports etc.)
ROS_COUNTER = 0
ALL_ROS = []

def generate_ros_farm(hosts, datafile_path='./data.ros.csv', still_count=[], pattern="", hosts_as_strings=False):
    """
    Generate a ROS farm from a (historic) CSV file

    hosts - [dal.Computer,...] 
      The list of valid hosts

    datafile_path 
    still_count - [dal.Computer,...] 
      This is a list of hosts that were commented
      out and will not be included in the farm, yet the global ROS index will still
      be increased when they are encountered in the CSV.  """

    print("INFO: Generating ROS farm from CSV file at: %s" %datafile_path)
    if hosts_as_strings:
      hosts = {}
    global ROS_COUNTER
    with open(datafile_path, 'rt') as datafile:
        ros_farm = {}
        rawheaders = datafile.next().split(',')
        global headers
        headers = []

#get column names
        for h in rawheaders:
            hex = False
            h = h.strip() #strip whitespace
            if h.endswith("%hex"):
                hex = True
                h = h.rstrip("%hex")
            if ':' in h:
                tag, level = h.split(':')
            else:
                tag, level = h, ""
            headers.append(Header(tag, level, hex))
        fieldnames = [h.tag for h in headers]

        rows = csv.reader(datafile)

        for row in rows:
            ROW_STARTED_WITH_EXCLMARK = False
            if pattern not in str.join(",", row): continue # skip if it doesn't contain the pattern
            if not row or not row[0].strip(): continue # skip empty lines
            if row[0].startswith('#'): continue # comment
            if row[0].startswith('!'):
              ROW_STARTED_WITH_EXCLMARK = True
              row[0] = row[0].lstrip('!')
            if row[0].startswith('-'): continue # option

            row_cols = parse_row(row)
            subdetector_id, hostname, rosname, channel, module = (None,)*5
            #decompose row
            for col in row_cols:
                if col == 'DETECTOR_ID': subdetector_id = row_cols[col]
                elif col == 'APPLICATION_IP_NAME':
                  hostname = row_cols[col]
                  if hosts_as_strings:
                    # wrap this in a list (because it's mutable)
                    hosts[hostname] = [hostname]
                elif col == 'ROS_NAME':
                  rosname = row_cols[col]
                elif col == 'Channel': channel = row_cols[col]
                elif col == 'Module': module = row_cols[col]
            assert(None not in (subdetector_id, hostname, channel))

            # sometimes Module isn't defined
            if module is None:
              module = 1
            subdetector = get_subdetector_by_id(subdetector_id)
            # this just for accounting
            if rosname not in ALL_ROS:
              ALL_ROS.append(rosname)
              ROS_COUNTER += 1

            if hostname not in list(hosts.keys()):
              print("WARNING: No host object found for host '%s' (mentioned in the CSV file):\n\ta)You filtered it out on purpose, ignore this warning.\n\tb)The host is not defined in the host database file (check your TDAQ_DB_PATH)." % hostname)
              continue
            if ROW_STARTED_WITH_EXCLMARK:
              continue # host was excluded; skip csv line

            ros_host = get_ros_host_by_hostname(hostname, hosts)

            #add row to farm; create any new objects when necessary
            if subdetector not in list(ros_farm.keys()):
              ros_farm[subdetector] = {'ros':[], 'name': perl_sd_names.get(subdetector, 'UNKNOWN')}

            if ros_host not in [ tpl[0] for tpl in ros_farm[subdetector]['ros'] ]:
              ros_farm[subdetector]['ros'].append((ros_host,{'modules':[], 'index':ROS_COUNTER, 'name':rosname}))

            roshost_tuple = filter(lambda t: t[0]==ros_host, ros_farm[subdetector]['ros'])[0]
            module_list = roshost_tuple[1]['modules']
            if module not in [ tpl[0] for tpl in module_list]:
              module_list.append( (module, []) )

            module_tuple = filter(lambda t: t[0]==module, module_list)[0]
            channel_list = module_tuple[1]
            if channel not in channel_list:
              channel_list.append(channel)
            else:
              # this means a) there is a duplicate channel id in the module
              # b) we did something horribly wrong
              raise RuntimeError("Duplicate channel %d in module %d, host %s"%(channel, module, hostname))

        return ros_farm


perl_sd_names = {
    helper.SubDetector.FORWARD_BCM:'FWD-BCM',
    helper.SubDetector.FORWARD_BCM:'FWD-BCM',
    helper.SubDetector.FORWARD_ALPHA:'FWD-ALFA',
    helper.SubDetector.MUON_CSC_ENDCAP_A_SIDE:'CSC-ECA',
    helper.SubDetector.MUON_CSC_ENDCAP_C_SIDE:'CSC-ECC',
    helper.SubDetector.TDAQ_MUON_CTP_INTERFACE:'TDQ-MUCTPI',
    helper.SubDetector.TDAQ_CTP:'TDQ-CTP',
    helper.SubDetector.TDAQ_FTK:'TDQ-FTK',
    helper.SubDetector.TDAQ_CALO_PREPROC:'TDQ-CALPP',
    helper.SubDetector.TDAQ_CALO_CLUSTER_PROC_DAQ:'TDQ-CALCP',
    helper.SubDetector.TDAQ_CALO_CLUSTER_PROC_ROI:'TDQ-CALCPROI',
    helper.SubDetector.TDAQ_CALO_JET_PROC_DAQ:'TDQ-CALJE',
    helper.SubDetector.TDAQ_CALO_JET_PROC_ROI:'TDQ-CALJEROI',
    helper.SubDetector.LAR_EM_BARREL_A_SIDE:'LAR-EMBA',
    helper.SubDetector.LAR_EM_BARREL_C_SIDE:'LAR-EMBC',
    helper.SubDetector.LAR_EM_ENDCAP_A_SIDE:'LAR-EMECA',
    helper.SubDetector.LAR_EM_ENDCAP_C_SIDE:'LAR-EMECC',
    helper.SubDetector.LAR_FCAL_A_SIDE:'LAR-FCALA',
    helper.SubDetector.LAR_FCAL_C_SIDE:'LAR-FCALC',
    helper.SubDetector.LAR_HAD_ENDCAP_A_SIDE:'LAR-HECA',
    helper.SubDetector.LAR_HAD_ENDCAP_C_SIDE:'LAR-HECC',
    helper.SubDetector.FORWARD_LUCID:'FWD-LUC',
    helper.SubDetector.MUON_MDT_BARREL_A_SIDE:'MDT-BA',
    helper.SubDetector.MUON_MDT_BARREL_C_SIDE:'MDT-BC',
    helper.SubDetector.MUON_MDT_ENDCAP_A_SIDE:'MDT-ECA',
    helper.SubDetector.MUON_MDT_ENDCAP_C_SIDE:'MDT-ECC',
    helper.SubDetector.PIXEL_BARREL:'PIX-B',
    helper.SubDetector.PIXEL_DISK_SIDE:'PIX-DISK',
    helper.SubDetector.PIXEL_B_LAYER:'PIX-BL',
    helper.SubDetector.MUON_RPC_BARREL_A_SIDE:'RPC-BA',
    helper.SubDetector.MUON_RPC_BARREL_C_SIDE:'RPC-BC',
    helper.SubDetector.SCT_BARREL_A_SIDE:'SCT-BA',
    helper.SubDetector.SCT_BARREL_C_SIDE:'SCT-BC',
    helper.SubDetector.SCT_ENDCAP_A_SIDE:'SCT-ECA',
    helper.SubDetector.SCT_ENDCAP_C_SIDE:'SCT-ECC',
    helper.SubDetector.MUON_TGC_ENDCAP_A_SIDE:'TGC-ECA',
    helper.SubDetector.MUON_TGC_ENDCAP_C_SIDE:'TGC-ECC',
    helper.SubDetector.TILECAL_BARREL_A_SIDE:'TIL-LBA',
    helper.SubDetector.TILECAL_BARREL_C_SIDE:'TIL-LBC',
    helper.SubDetector.TILECAL_EXT_A_SIDE:'TIL-EBA',
    helper.SubDetector.TILECAL_EXT_C_SIDE:'TIL-EBC',
    helper.SubDetector.TRT_ENDCAP_A_SIDE:'TRT-ECA',
    helper.SubDetector.TRT_ENDCAP_C_SIDE:'TRT-ECC',
    helper.SubDetector.TRT_BARREL_A_SIDE:'TRT-BA',
    helper.SubDetector.TRT_BARREL_C_SIDE:'TRT-BC',
    helper.SubDetector.FORWARD_ZDC:'FWD-ZDC',
    }
#'ros': [(<pc-fwd-ros-zdc-00.cern.ch@Computer>, {'index': 1, 'modules': [(1, [0, 1, 2]), (2, [3])]})]}}


def printfarm(farm):
  i = ' '
  indent = ""
  print("ros_farm = {")
  print(indent+i+"'name' : 'CHANGE_THIS_TO_FARM_NAME',")
  for sd in farm:
    indent += i
    if not isinstance(sd, helper.SubDetector): continue
    sdstring = repr(sd)
    sdstring = 'helper.' + str.join('.', sdstring.split('.')[1:])

    print(indent, sdstring, ':', '{')

    indent += i
    print(indent, repr('name'), ':', repr(farm[sd]['name']),',')
    print(indent, repr('ros') + ' : [')

    indent += i
    for tuple in farm[sd]['ros']:
      host = tuple[0]
      if isinstance(host, list): hostname = host[0]
      else: hostname = host.id.split('.')[0]
      rosdict = tuple[1]
      print(indent+"(hosts["+repr(hostname)+"],\n"\
          +indent+i+" {'index':", rosdict['index'], " 'name':", rosdict['name'], ",'modules':[", end=' ')
      for mtuple in rosdict['modules']:
        module_index = mtuple[0]
        module_channels = mtuple[1]
        print('('+str(module_index)+',[', end=' ')
        for channel in module_channels:
          print("0x%02x"%channel+',', end=' ')
        print(']),', end=' ')
      print(']}),')
    indent = indent[:-len(i)]
    print(indent, ']')
    indent = indent[:-len(i)]
    print(indent, '},')
    indent = indent[:-len(i)]
  print(indent, '}')

if __name__ == '__main__':
  from optparse import OptionParser
  usage = "Usage: %prog [options] <path/to/datafile.csv>"
  parser = OptionParser(usage)
  parser.add_option("-g", "--grep", dest="pattern", default="",
      help="Use only lines from the CSV file that contain this pattern (constant pattern, no regex support) ", metavar="PATTERN")
  parser.add_option("-s", "--substitute-hosts", dest="subst_hosts",
      help="Substitute the hosts in the farm with the hostnames in the file (will recycle if not enough)", metavar="<pc.list>")

  (options, args) = parser.parse_args()
  try:
    csvfile = args[0]
  except:
    print(parser.print_help())
    sys.exit()
  ros_farm = generate_ros_farm(None, csvfile, pattern=options.pattern, hosts_as_strings=True)

  # substitute all host references with the ones from the file
  reused_ros = False
  if options.subst_hosts:
    with open(options.subst_hosts, 'r') as pclist:
      for sd in ros_farm:
        for ros in ros_farm[sd]['ros']:
          assert isinstance(ros[0], list), "Probably some bug in the script!"
          nextline = '#'
          # skip commented
          while nextline.startswith('#'):
            try:
              nextline = next(pclist)
            except StopIteration:
              # EOF, recycle hosts
              reused_ros = True
              pclist.seek(0)
          nextline = nextline.strip()
          ros[0][0] = nextline
  printfarm(ros_farm)
  if reused_ros: print("# WARNING: pclist had too few hosts, had to recycle!")



