#!/usr/bin/env tdaq_python

# BCM post processing script
# fixes only 2 minor naming issues

from __future__ import print_function
def modify(segment):
    seg = segment[0]
    det = seg.get("Detector")[0]
    ros = seg.get("ROS")

    if det.id!='FWD-BCM':
      print("BCM post processing script says: I was going"\
          " to rename FWD-BCM to BCM, but apparently no detector object"\
          " exists with this name. I won't rename the detector.")
    else:
      det.id = "BCM"
      for r in ros:
        r.Detector = det

    return segment
