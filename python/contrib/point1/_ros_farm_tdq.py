#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Wed 19 Sep 2007 11:54:23 AM CEST

"""
"""
from __future__ import absolute_import

##################
# Initialization #
##################

from builtins import range
import sys
import pm.multinode
import pm.farm
import pm.common
from eformat import helper
from .standard import *


# PM won't pass parameters to the farms;
# cheat and parse the command line directly
# beware of this when modifying the arguments!
ros_mode = None
for i in range(len(sys.argv)):
  if sys.argv[i] == '--ros-mode': ros_mode = sys.argv[i+1]

assert ros_mode, "No --ros-mode? (needed by the ros_farm file!)"

ros_farm = {}

#############
# Filtering #
#############

hosts = get_standard_hosts()
# filter by host list
host_list = [ \
        'pc-preseries-ros-08',
    ]
hosts = filter_hosts_by_list(hosts, host_list)

# list could be in a file
#hosts = filter_hosts_by_file(hosts, 'data/rosPc.tgc.list')

# or use a pattern
#host_pattern = 'pc-csc-ros-ec[a,c]-00'
#hosts = filter_hosts_by_pattern(hosts, host_pattern)

ros_farm = get_standard_farm(hosts)

# filter by TDQ (FULL_SD_EVENT==0, use that)
subdetectors = [ helper.SubDetector.FULL_SD_EVENT ]
ros_farm = filter_by_subdetectors(ros_farm, subdetectors)

##############
# Parameters #
##############

from .standard import get_common_params
ros_farm.update(get_common_params(ros_mode))

##########################################################################################
# NOTE: These are the subdetector specific parameters, see standard.py as well #
##########################################################################################

#list here the include files
includes = [ \
    'daq/segments/ROS/ros-common-config.data.xml',
    'daq/segments/ROS/ros-specific-config-TDQ.data.xml',
    ]

ros_objs = pm.common.load(includes)

# NOTE: these are the names as used by the perl script, useful for comparison
# the full descriptions in helper.SubDetector should probably be used
ros_farm['name'] = 'TDQ'


ros_farm['ros_config'] = ros_objs['ROS-Config-TDQ']
ros_farm['robin_channel_config'] = ros_objs['RobinDataChannel-Config-TDQ'] 
ros_farm['robin_mem_pool'] = ros_objs['ROS-MemoryPool-Data-TDQ']
ros_farm['robin_config'] = ros_objs['Robin-Config-TDQ']

# mode-specific parameters (most are in standard)
if ros_mode == 'robin-datadriven-reb':
  pass

elif ros_mode == 'emulated-dc' \
    or ros_mode == 'robin-dc' \
    or ros_mode == 'preloaded-dc':
   pass

#add other options to ros_farm from this point on
