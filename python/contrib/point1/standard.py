from __future__ import absolute_import
__author__ = "Giorgos Boutsioukis <georgios.boutsioukis@cern.ch>"

import pm.common

import os
hostfile = os.getenv('ROS_HOSTFILE') or "daq/hw/hosts-ros.data.xml"

def get_common_params(ros_mode, robinnp = False):
  ros_farm = {}
  #list here the include files
  ros_farm['includes'] = [ \
      'daq/segments/ROS/ros-common-config.data.xml',
      ]
  if hostfile != "daq/hw/hosts-ros.data.xml":
    # the standard file is includes in ros-common-config.data.xml, no reason to
    # duplicate the include in this case
    ros_farm['includes'].append(hostfile)

  ros_farm['ros_common_config'] = ['daq/segments/ROS/ros-common-config.data.xml']
  ros_objs = pm.common.load(ros_farm['includes'])

  ros_farm['default_host']= None

#use this to override the default DB parameters for the ROS
  if robinnp:
    ros_farm['ros_override_params'] = { \
      'ActionTimeout':10,
      'ProbeInterval':5,
      'FullStatisticsInterval':60, 
      'Parameters':'-a -1',
      'RestartParameters':'-a -1',
      'InitTimeout':60,
      'RestartableDuringRun':1,
      }
  else:
    ros_farm['ros_override_params'] = { \
      'ActionTimeout':10,
      'ProbeInterval':5,
      'FullStatisticsInterval':60, 
      'Parameters':'-a 3',
      'RestartParameters':'-a 3',
      'InitTimeout':60,
      'RestartableDuringRun':1,
      }

# use this to override the default DB parameters for the RunControlApplication
# comment the key out if you want to use RunControlTemplateApplication
  #ros_farm['rcapp_override_params'] = { \
      #    'ActionTimeout':60,
      #'ProbeInterval':5,
      #'FullStatisticsInterval':60,
      #'IfExitsUnexpectedly':'Restart',
      #}

# mode-specific parameters
  if ros_mode == 'robin-datadriven-reb':
    ros_farm['reb'] = {}
    ros_farm['reb']['tcp_data_out_config_override_params'] = { \
        'SamplingGap':1,
        'TCPBufferSize':32768,
        'ThrowIfUnavailable':0
        }

    # REB segment parameters
    ros_farm['reb']['rcapp_override_params'] = { \
        'ActionTimeout':60,
        'ProbeInterval':5,
        'FullStatisticsInterval':60,
        'IfExitsUnexpectedly':'Restart',
        }
    ros_farm['reb']['rcd_override_params'] = {
        'ProbeInterval':5,
        'FullStatisticsInterval':60,
        'Id':85, # (?) this was hardcoded in the perl template
        }
    ros_farm['reb']['rcd_config_override_params'] = {
        'NumberOfRequestHandlers':1,
        }

    ros_farm['reb']['input_fragment_type'] = 'ROSFragment'

  elif ros_mode == 'emulated-dc' \
      or ros_mode == 'robin-dc' \
      or ros_mode == 'preloaded-dc':
      pass

  return ros_farm

def get_standard_farm(hosts, robinnp=False):
  import os
  if robinnp:
    from .csv2farm3 import generate_ros_farm
    csvfile = os.getenv('ROS_CSVFILE') or 'data/data.ros3.csv'
  else:
    from .csv2farm import generate_ros_farm
    csvfile = os.getenv('ROS_CSVFILE') or 'data/data.ros.csv'

  base = os.path.dirname(os.path.abspath(__file__))
  return generate_ros_farm(hosts, datafile_path=base+'/'+csvfile)

def get_standard_hosts():

  hosts = pm.farm.load(hostfile, short_names=True)
  return dict([(k,hosts[k]) for k in hosts if hosts[k].isDalType('Computer')])
