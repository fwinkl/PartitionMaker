#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :

from __future__ import print_function
from __future__ import absolute_import
from builtins import range
__author__ = "Giorgos Boutsioukis <georgios.boutsioukis@cern.ch>"

##################
# Initialization #
##################

import sys
import pm.multinode
import pm.farm
import pm.common
from eformat import helper
from .standard import *


# PM won't pass parameters to the farms;
# cheat and parse the command line directly
# beware of this when modifying the arguments!
ros_mode = None
for i in range(len(sys.argv)):
  if sys.argv[i] == '--ros-mode': ros_mode = sys.argv[i+1]

assert ros_mode, "No --ros-mode? (needed by the ros_farm file!)"

robinnp = ('--robinnp' in sys.argv)

ros_farm = {}

#############
# Filtering #
#############

import os
base = os.path.dirname(os.path.abspath(__file__))
hosts = get_standard_hosts()

# filter by host list
#host_list = [ \
    #    'pc-fwd-ros-bcm-00'
    #]
#hosts = pm.farm.filter_hosts_by_list(hosts, host_list)

# list could be in a file
#hosts = pm.farm.filter_hosts_by_file(hosts, base+'/data/rosPc.tgc.list')

# or use a pattern
#host_pattern = 'pc-csc-ros-ec[a,c]-00'
#hosts = pm.farm.filter_hosts_by_pattern(hosts, host_pattern)

ros_farm = get_standard_farm(hosts, robinnp)

# filter by LAR subdetectors
if robinnp:
  ros_farm[0]['ros'] = [ros for ros in ros_farm[0]['ros'] if 'LAR' in ros[1]['name']]
else:
  subdetectors = [ \
    helper.SubDetector.LAR_EM_BARREL_A_SIDE,
    helper.SubDetector.LAR_EM_BARREL_C_SIDE,
    helper.SubDetector.LAR_EM_ENDCAP_A_SIDE,
    helper.SubDetector.LAR_EM_ENDCAP_C_SIDE,
    helper.SubDetector.LAR_FCAL_A_SIDE,
    helper.SubDetector.LAR_FCAL_C_SIDE,
    helper.SubDetector.LAR_HAD_ENDCAP_A_SIDE,
    helper.SubDetector.LAR_HAD_ENDCAP_C_SIDE,
    ]
  ros_farm = pm.farm.filter_by_subdetectors(ros_farm, subdetectors)
##############
# Parameters #
##############

ros_farm.update(get_common_params(ros_mode, robinnp))

##########################################################################################
# NOTE: These are the subdetector specific parameters, see standard.py as well #
##########################################################################################

#list here the include files
includes = [ \
    'daq/segments/ROS/ros-common-config.data.xml',
    'daq/segments/ROS/ros-specific-config-LAR.data.xml',
    # extra includes are added through the command line
    ]

includes = pm.utils.merge_unique(ros_farm.get('includes', []), includes)

ros_objs = pm.common.load(includes)

ros_farm['name'] = 'LAR'

# this part overrides the generation of the detector objects and use
# the predefined ones (provided that the relevant includes were added)
# WARNING: these relies on subdetectors having standard names (LAR-EMBA, LAR-EMBC, ...)
if not robinnp:
  sds_in_farm = [sd for sd in subdetectors if sd in ros_farm] # fix for filtering from CSV
  for sd in sds_in_farm:
    if ros_farm[sd]['name'] in ros_objs:
      ros_farm[sd]['detector'] = ros_objs.get(ros_farm[sd]['name'], None)

ros_farm['ros_config'] = ros_objs['ROS-Config-LAR']
ros_farm['robin_channel_config'] = ros_objs['RobinDataChannel-Config-LAR'] 
ros_farm['robin_mem_pool'] = ros_objs['ROS-MemoryPool-Data-LAR']
ros_farm['robin_config'] = ros_objs['Robin-Config-LAR']
ros_farm['debug_output'] = ros_objs.get('ROS-LAR-DebugOutput', None)
if ros_farm['debug_output'] is None:
  print("Warning: ROS-LAR-DebugOutput not found in includes, setting DebugOutput to null")

# mode-specific parameters (most are in standard)
if ros_mode == 'robin-datadriven-reb':
  pass

elif ros_mode == 'emulated-dc' \
    or ros_mode == 'robin-dc' \
    or ros_mode == 'preloaded-dc':
   pass

#add other options to ros_farm from this point on
