from __future__ import division
from builtins import range
from past.utils import old_div
__author__ = "Giorgos Boutsioukis <georgios.boutsioukis@cern.ch>"

####################
# Helper functions #
####################
from eformat import helper

def name_from_hostname(hostname):
  "This is primarily for hostnames of the form: pc-fwd-ros-bcm-00"
  hostname = hostname.split('.')[0]
  hostname = hostname.upper()
  bits = hostname.split('-')
  try:
    return bits[1]+'-'+bits[3]+'-'+bits[4]
  except IndexError:
    #well, I tried
    return hostname

def generate_preloaded_farm(hosts, roblist_path, subdetector_filter=[], groups=[]):
  roslist = []
  roblist = {} #per subdetector
  numrobs = 0
  numros = 0

  import os.path
  base = os.path.dirname(os.path.abspath(__file__))
  roblist_path = base + '/' + roblist_path

  if len(subdetector_filter) and isinstance(subdetector_filter[0], int):
    subdetector_filter = [helper.SubDetector.values[sid] for sid in subdetector_filter]
  for group in groups:
    if len(group) and isinstance(group[0], int):
      group = [helper.SubDetector.values[sid] for sid in group]
  merged_sds = []
  # combine grouped subdets in one (the first)
  for group in groups:
    for sd in group[1:]: merged_sds.append(sd)

  if isinstance(hosts, str):
    with open(roslist_path, 'rb') as roslistfile:
        for line in roslistfile:
          if line.startswith('#') or line.startswith('!'): continue
          roslist.append(line.strip())
  else: roslist = list(hosts.keys())

  with open(roblist_path, 'rb') as roblistfile:
    for line in roblistfile:
      if line.startswith('#') or line.startswith('!'): continue
      robid = int(line.strip(), 16)
      rob_sd = helper.SubDetector.values[helper.SourceIdentifier(robid).subdetector_id()]
      if len(subdetector_filter) \
          and helper.SubDetector.values[rob_sd] \
          not in subdetector_filter:
        continue
      if rob_sd in merged_sds:
          merged_group_first = [g[0] for g in groups if rob_sd in g][0]
          rob_sd = merged_group_first
      if rob_sd not in roblist:
          roblist[rob_sd] = []
      roblist[rob_sd].append(robid)
      numrobs += 1

  # ros to subdetector assignment

  sd_ros_count = dict.fromkeys(list(roblist.keys()), 1)
  unallocated_ros = len(roslist) - len(sd_ros_count)

  while unallocated_ros>0:
    # on each round, allocate a ROS to the subdetector with the highest ROB/ROS ratio
    lucky_sd = max(list(roblist.keys()), key=lambda sd: old_div(float(len(roblist[sd])),sd_ros_count[sd]))
    # group: only assign 
    sd_ros_count[lucky_sd] += 1
    unallocated_ros -= 1

  #for sd in sd_ros_count:
  #  print hex(sd.real), " : ", sd_ros_count[sd], float(len(roblist[sd]))/sd_ros_count[sd]
  #print sum([sd_ros_count[sd] for sd in sd_ros_count])

  #ros_farm = dict([ (sd,{'ros':[]}) for sd in roblist ])
  ros_farm = {helper.SubDetector.FULL_SD_EVENT:{'ros':[]}}

  # assign the channels to each ros and create the farm
  last_ros_index = 0
  assigned_robs = 0
  for sd in roblist:
    sd_channel_list = roblist[sd]

    last_channel_index = 0
    for ros_index in range(last_ros_index, last_ros_index+sd_ros_count[sd]):
      ros = hosts[roslist[ros_index]]
      ros_in_sd_index = ros_index - last_ros_index

      numchannels = old_div(len(sd_channel_list),sd_ros_count[sd])
      # if the channels can't be divided exactly by the ros count, assign an extra channel
      # to the first (numchannels % numROSs) ROSs
      if ros_in_sd_index < len(sd_channel_list)%sd_ros_count[sd]:
        numchannels +=  1

      # add all channels to a single module with index 0
      ros_channels = sd_channel_list[last_channel_index:last_channel_index+numchannels]
      # merge all subdetector ROSs to FULL_SD_EVENT
      ros_farm[helper.SubDetector.FULL_SD_EVENT]['ros'].append(\
          (ros, {'modules':[(0,ros_channels)], 'index':ros_index, 'name':name_from_hostname(ros.id)}) )
      last_channel_index += numchannels

    assert last_channel_index == len(sd_channel_list), "Not all channels were assigned! (failed in subdetector) %d != %d " % (last_channel_index, len(sd_channel_list))
    assigned_robs += len(sd_channel_list)
    last_ros_index += sd_ros_count[sd]

  assert numrobs == assigned_robs, "Not all channels were assigned! %d != %d " % (numrobs, assigned_robs)
  return ros_farm


