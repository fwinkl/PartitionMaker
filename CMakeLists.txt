tdaq_package()

tdaq_add_python_package(pm)

#Copying the ROS generation kit
install(DIRECTORY python/contrib  COMPONENT ${TDAQ_COMPONENT_NOARCH} OPTIONAL DESTINATION share/data/PartitionMaker FILES_MATCHING PATTERN Makefile PATTERN README PATTERN *.py PATTERN *.csv PATTERN *.list PATTERN *.xml PATTERN .svn EXCLUDE)

tdaq_add_scripts(python/scripts/*.py)

# FIXME: install examples
# apply_pattern install_examples name="pm_examples" src_dir="../python/examples" files=" *.py Makefile *.xml "

tdaq_add_test(NAME test-pm-set COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/python/tests/test_pm_set ${CMAKE_CURRENT_SOURCE_DIR}/python/tests/pm-set-reference.data.xml
              WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR} POST_INSTALL)
